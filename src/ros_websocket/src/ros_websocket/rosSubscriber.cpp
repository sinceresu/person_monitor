#include "rosSubscriber.hpp"
#include "rosTopic.hpp"
#include "rosNode.hpp"

/**
  * 函数名称:       CRosSubscriber
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosSubscriber::CRosSubscriber()
{
}

/**
  * 函数名称:       CRosSubscriber
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosSubscriber::CRosSubscriber(const std::string &_ip, const std::string &_name, const std::string &_type, const topic_callback_fun &_callback, const unsigned int &_queue_length,
                               const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint)
    : ip(_ip),
      id(to_string(boost::uuids::random_generator()())),
      name(_name),
      type(_type),
      throttle_rate(0),
      queue_length(_queue_length),
      fragment_size(0),
      compression("none"),
      callback(_callback),
      state("subscribe"),
      node_id(_nodeId),
      node_name(_nodeName),
      endpoint(_endpoint)
{
}

/**
  * 函数名称:       ~CRosSubscriber
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosSubscriber::~CRosSubscriber()
{
}

void CRosSubscriber::unsubscriber()
{
  if (state == "" || state == "unsubscribe" || endpoint == nullptr || node_id == 0)
  {
    return;
  }

  std::string unsubscribeData;
  Json::Value root;
  root["ip"] = ip;
  root["op"] = "unsubscribe";
  root["id"] = id;
  root["topic"] = name;
  unsubscribeData = root.toStyledString();

  std::string message = unsubscribeData;

  int ret = endpoint->send(node_id, message);
  if (ret != 0)
  {
    return;
  }

  CRosTopic::get_topic()->remove_subscriber(*this);

  ip = "";
  id = "";
  name = "";
  type = "";
  throttle_rate = 0;
  queue_length = 0;
  fragment_size = 0;
  compression = "";
  callback = 0;
  state = "";
  node_id = 0;
  node_name = "";
  endpoint = nullptr;
}

bool CRosSubscriber::operator==(const CRosSubscriber m) const
{
  if (ip == m.ip && id == m.id && name == m.name && type == m.type && throttle_rate == m.throttle_rate && queue_length == m.queue_length &&
      fragment_size == m.fragment_size && compression == m.compression && state == m.state &&
      node_id == m.node_id && node_name == m.node_name && endpoint == m.endpoint)
  {
    return true;
  }

  return false;
}

bool CRosSubscriber::operator!=(const CRosSubscriber m) const
{
  if (ip != m.ip || id != m.id || name != m.name || type != m.type || throttle_rate != m.throttle_rate || queue_length != m.queue_length ||
      fragment_size != m.fragment_size || compression != m.compression || state != m.state ||
      node_id != m.node_id || node_name != m.node_name || endpoint != m.endpoint)
  {
    return true;
  }

  return false;
}