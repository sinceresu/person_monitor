#include "rosNode.hpp"

#include "../common/Common.hpp"
/**
  * 函数名称:       CRosNode
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosNode::CRosNode()
    : id(0),
      name(""),
      endpoint(nullptr)
{
  endpoint = new websocket_endpoint();

  p = this;
}

/**
  * 函数名称:       CRosNode
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosNode::CRosNode(const std::string &_url, const std::string &_name)
    : url(_url),
      id(0),
      name(_name),
      endpoint(nullptr)
{
  endpoint = new websocket_endpoint();

  init_connect(_url, _name);

  p = this;
}

/**
  * 函数名称:       ~CRosNode
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosNode::~CRosNode()
{
  id = 0;
  name = "";

  if (endpoint != nullptr)
  {
    delete endpoint;
    endpoint = nullptr;
  }
}

CRosNode *CRosNode::p = nullptr;

void CRosNode::on_message(std::string _msg)
{
  // printf("on_message msg : %s \r\n", _msg.c_str());

  Json::Reader reader;
  Json::Value root;
  bool b = reader.parse(_msg, root);
  if (b == false)
  {
    return;
  }

  if (root.isMember("code"))
  {
    if (root["code"].asString().compare("501") == 0 ||
        root["code"].asString().compare("502") == 0 ||
        root["code"].asString().compare("503") == 0)
    {
      printf("on_message msg : %s \r\n", _msg.c_str());
      std::list<std::string>::iterator ip = std::find(register_fail_ips.begin(), register_fail_ips.end(), root["ip"].asString());
      if (ip == register_fail_ips.end())
      {
        register_fail_ips.push_back(root["ip"].asString());
      }
      return;
    }
  }

  if (root["op"].asString() == "publish")
  {
    std::vector<CRosSubscriber> subs = topics.get_subs();
    for (std::vector<CRosSubscriber>::const_iterator it = subs.begin(); it != subs.end(); ++it)
    {
      if (it->name == root["topic"].asString())
      {
        if (root["msg"].isObject())
        {
          std::string msg_ip = "";
          std::string msg_data = "";
          msg_data = root["msg"].toStyledString();
          msg_ip = root["ip"].asString();

          msg_data = ReplaceString(msg_data, "\n", "");
          // msg_data = ReplaceString(msg_data, " ", "");

          it->callback(msg_ip, msg_data);
        }
      }
    }
  }
  if (root["op"].asString() == "call_service")
  {
    std::vector<CRosServer> servers = services.get_servers();
    for (std::vector<CRosServer>::iterator it = servers.begin(); it != servers.end(); ++it)
    {
      if (it->name == root["service"].asString())
      {
        if (root["args"].isObject())
        {
          std::string msg_ip = "";
          std::string msg_data = "";
          msg_data = root["args"].toStyledString();
          msg_ip = root["ip"].asString();

          msg_data = ReplaceString(msg_data, "\n", "");
          // msg_data = ReplaceString(msg_data, " ", "");

          std::string res;
          bool bRet = it->callback(msg_ip, msg_data, res);
          // response
          string call_id = root["id"].asString();
          it->response(msg_ip, bRet, call_id, res);
        }
      }
    }
  }
  if (root["op"].asString() == "service_response")
  {
    std::vector<CRosClient> clients = services.get_clients();
    for (std::vector<CRosClient>::iterator it = clients.begin(); it != clients.end(); ++it)
    {
      if (it->name == root["service"].asString() && it->id == root["id"].asString())
      {
        map<std::string, SresMsg>::iterator res;
        res = CRosClient::res_msg.find(it->id + "/" + it->name);
        if (res == CRosClient::res_msg.end())
        {
          return;
        }
        SresMsg &m_res = res->second;

        bool bRet = root["result"].asBool();
        if (root["values"].isObject())
        {
          std::string msg_ip = "";
          std::string msg_data = "";
          msg_data = root["values"].toStyledString();
          msg_ip = root["ip"].asString();

          msg_data = ReplaceString(msg_data, "\n", "");
          // msg_data = ReplaceString(msg_data, " ", "");

          m_res.res_msg = msg_data;
          m_res.res_ip = msg_ip;
        }

        m_res.exit_flag = 1;
      }
    }
  }
}

void CRosNode::connect_check_fun(double msec)
{
  std::time_t start_time = GetTimeStamp();
  while (1)
  {
    std::time_t now_time = GetTimeStamp();
    if ((now_time - start_time) >= msec)
    {
      start_time = GetTimeStamp();

      try
      {
        std::string status = get_status();
        if (status != "Open")
        {
          //
        }
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }
    }
    usleep(100000);
  }
}

void CRosNode::init_connect(const std::string &_url, const std::string &_name)
{
  url = _url;
  name = _name;

  id = endpoint->connect(_url);
  if (id != -1)
  {
    std::cout << "> Created connection with id " << id << std::endl;
  }

  endpoint->register_msg_callback(id, boost::bind(&CRosNode::on_message, this, _1));

  // thread connect_status(boost::bind(&CRosNode::connect_check_fun, this, 10000.0));
  // connect_status.detach();
}

void CRosNode::reconnection()
{
  id = endpoint->connect(id, url);
  if (id != -1)
  {
    std::cout << "> Created connection with id " << id << std::endl;
  }

  services.clear_server();
  services.clear_client();
  topics.clear_publisher();
  topics.clear_subscriber();

  endpoint->register_msg_callback(id, boost::bind(&CRosNode::on_message, this, _1));
}

std::string CRosNode::get_status()
{
  return endpoint->get_status(id);
}

CRosNode *CRosNode::get_node()
{
  return p;
}

void CRosNode::printf_info()
{
  if (endpoint == nullptr)
  {
    return;
  }

  std::cout << "\n";
  connection_metadata::ptr metadata = endpoint->get_metadata(id);
  if (metadata)
  {
    std::cout << *metadata << std::endl;
  }
  else
  {
    std::cout << "> Unknown connection id " << id << std::endl;
  }
}

void CRosNode::printf_metadata()
{
  if (endpoint == nullptr)
  {
    return;
  }

  std::cout << "\n";
  endpoint->printf_metadata(id);
}

void CRosNode::printf_message()
{
  if (endpoint == nullptr)
  {
    return;
  }

  std::cout << "\n";
  endpoint->printf_message(id);
}

void CRosNode::printf_server()
{
  std::cout << "\n";
  std::cout << "> Server: (" << services.get_servers().size() << ") \n";
  std::cout << std::endl;

  std::vector<CRosServer> servers = services.get_servers();
  std::vector<CRosServer>::const_iterator it;
  for (it = servers.begin(); it != servers.end(); ++it)
  {
    std::cout << "> Server: " << it->name << "\n";
    std::cout << "> Type: " << it->type << "\n";
    std::cout << std::endl;
  }
}

void CRosNode::printf_client()
{
  std::cout << "\n";
  std::cout << "> Client: (" << services.get_clients().size() << ") \n";
  std::cout << std::endl;

  std::vector<CRosClient> clients = services.get_clients();
  std::vector<CRosClient>::const_iterator it;
  for (it = clients.begin(); it != clients.end(); ++it)
  {
    std::cout << "> Client: " << it->name << "\n";
    std::cout << "> Type: " << it->type << "\n";
    std::cout << std::endl;
  }
}

void CRosNode::printf_publisher()
{
  std::cout << "\n";
  std::cout << "> Publisher: (" << topics.get_pubs().size() << ") \n";
  std::cout << std::endl;

  std::vector<CRosPublisher> pubs = topics.get_pubs();
  std::vector<CRosPublisher>::const_iterator it;
  for (it = pubs.begin(); it != pubs.end(); ++it)
  {
    std::cout << "> Topic: " << it->name << "\n";
    std::cout << "> Type: " << it->type << "\n";
    std::cout << std::endl;
  }
}

void CRosNode::printf_subscriber()
{
  std::cout << "\n";
  std::cout << "> Subscriber: (" << topics.get_subs().size() << ") \n";
  std::cout << std::endl;

  std::vector<CRosSubscriber> subs = topics.get_subs();
  std::vector<CRosSubscriber>::const_iterator it;
  for (it = subs.begin(); it != subs.end(); ++it)
  {
    std::cout << "> Topic: " << it->name << "\n";
    std::cout << "> Type: " << it->type << "\n";
    std::cout << std::endl;
  }
}

CRosServer CRosNode::advertiseService(std::string _ip, std::string _name, std::string _type, service_callback_fun _callback)
{
  CRosServer server(_ip, _name, _type, _callback, id, name, endpoint);

  if (endpoint == nullptr)
  {
    return server;
  }

  std::string advertiseServiceData;
  Json::Value root;
  root["ip"] = server.ip;
  root["op"] = "advertise_service";
  root["service"] = server.name;
  root["type"] = server.type;
  advertiseServiceData = root.toStyledString();

  std::string message = advertiseServiceData;

  int ret = endpoint->send(id, message);
  if (ret != 0)
  {
    return server;
  }

  services.add_server(server);

  return server;
}

void CRosNode::unadvertiseService(CRosServer &_server)
{
  if (endpoint == nullptr)
  {
    return;
  }

  std::string unadvertiseServiceData;
  Json::Value root;
  root["ip"] = _server.ip;
  root["op"] = "unadvertise_service";
  root["service"] = _server.name;
  unadvertiseServiceData = root.toStyledString();

  std::string message = unadvertiseServiceData;

  int ret = endpoint->send(id, message);
  if (ret != 0)
  {
    return;
  }

  services.remove_server(_server);

  _server = CRosServer();
}

CRosClient CRosNode::serviceClient(std::string _ip, std::string _name, std::string _type)
{
  CRosClient client(_ip, _name, _type, id, name, endpoint);

  if (endpoint == nullptr)
  {
    return client;
  }

  map<std::string, SresMsg>::iterator it;
  it = CRosClient::res_msg.find(client.id + "/" + client.name);
  if (it == CRosClient::res_msg.end())
  {
    SresMsg temp;
    temp.exit_flag = 0;
    temp.res_ip = "";
    temp.res_msg = "";
    CRosClient::res_msg.insert(pair<std::string, SresMsg>(client.id + "/" + client.name, temp));
  }
  else
  {
    it->second.exit_flag = 0;
    it->second.res_ip = "";
    it->second.res_msg = "";
  }

  services.add_client(client);

  return client;
}

void CRosNode::unserviceClient(CRosClient &_client)
{
  if (endpoint == nullptr)
  {
    return;
  }

  map<std::string, SresMsg>::iterator it;
  it = CRosClient::res_msg.find(_client.id + "/" + _client.name);
  if (it == CRosClient::res_msg.end())
  {
    //
  }
  else
  {
    CRosClient::res_msg.erase(_client.id + "/" + _client.name);
  }

  services.remove_client(_client);

  _client = CRosClient();
}

CRosPublisher CRosNode::advertise(std::string _ip, std::string _name, std::string _type)
{
  CRosPublisher pub(_ip, _name, _type, id, name, endpoint);

  if (endpoint == nullptr)
  {
    return pub;
  }

  std::string advertiseData;
  Json::Value root;
  root["ip"] = pub.ip;
  root["op"] = "advertise";
  root["id"] = pub.id;
  root["topic"] = pub.name;
  root["type"] = pub.type;
  advertiseData = root.toStyledString();

  std::string message = advertiseData;

  int ret = endpoint->send(id, message);
  if (ret != 0)
  {
    return pub;
  }

  topics.add_publisher(pub);

  return pub;
}

void CRosNode::unadvertise(CRosPublisher &_pub)
{
  if (endpoint == nullptr)
  {
    return;
  }

  std::string unadvertiseData;
  Json::Value root;
  root["ip"] = _pub.ip;
  root["op"] = "unadvertise";
  root["id"] = _pub.id;
  root["topic"] = _pub.name;
  unadvertiseData = root.toStyledString();

  std::string message = unadvertiseData;

  int ret = endpoint->send(id, message);
  if (ret != 0)
  {
    return;
  }

  topics.remove_publisher(_pub);

  _pub = CRosPublisher();
}

CRosSubscriber CRosNode::subscribe(std::string _ip, std::string _name, std::string _type, topic_callback_fun _callback, unsigned int _queue_length)
{
  CRosSubscriber sub(_ip, _name, _type, _callback, _queue_length, id, name, endpoint);

  if (endpoint == nullptr)
  {
    return sub;
  }

  std::string subscribeData;
  Json::Value root;
  root["ip"] = sub.ip;
  root["op"] = "subscribe";
  root["id"] = sub.id;
  root["topic"] = sub.name;
  root["type"] = sub.type;
  // root["throttle_rate"] = sub.throttle_rate;
  root["queue_length"] = sub.queue_length;
  // root["fragment_size"] = sub.fragment_size;
  // root["compression"] = sub.compression;
  subscribeData = root.toStyledString();

  std::string message = subscribeData;

  int ret = endpoint->send(id, message);
  if (ret != 0)
  {
    return sub;
  }

  topics.add_subscriber(sub);

  return sub;
}

void CRosNode::unsubscribe(CRosSubscriber &_sub)
{
  if (endpoint == nullptr)
  {
    return;
  }

  std::string unsubscribeData;
  Json::Value root;
  root["ip"] = _sub.ip;
  root["op"] = "unsubscribe";
  root["id"] = _sub.id;
  root["topic"] = _sub.name;
  unsubscribeData = root.toStyledString();

  std::string message = unsubscribeData;

  int ret = endpoint->send(id, message);
  if (ret != 0)
  {
    return;
  }

  topics.remove_subscriber(_sub);

  _sub = CRosSubscriber();
}

void CRosNode::retry_register()
{
  std::vector<CRosServer> servers = services.get_servers();
  std::vector<CRosClient> clients = services.get_clients();
  std::vector<CRosPublisher> pubs = topics.get_pubs();
  std::vector<CRosSubscriber> subs = topics.get_subs();

  std::list<std::string> ips = register_fail_ips;
  register_fail_ips.clear();

  while (ips.size() > 0)
  {
    std::string ip = ips.front();
    ips.pop_front();
    //
    for (std::vector<CRosServer>::const_iterator server = servers.begin(); server != servers.end(); ++server)
    {
      std::string advertiseServiceData;
      Json::Value root;
      root["ip"] = ip;
      root["op"] = "advertise_service";
      root["service"] = server->name;
      root["type"] = server->type;
      advertiseServiceData = root.toStyledString();
      std::string message = advertiseServiceData;
      int ret = endpoint->send(id, message);
    }
    //
    for (std::vector<CRosClient>::const_iterator client = clients.begin(); client != clients.end(); ++client)
    {
      /* code */
    }
    //
    for (std::vector<CRosPublisher>::const_iterator pub = pubs.begin(); pub != pubs.end(); ++pub)
    {
      std::string advertiseData;
      Json::Value root;
      root["ip"] = ip;
      root["op"] = "advertise";
      root["id"] = pub->id;
      root["topic"] = pub->name;
      root["type"] = pub->type;
      advertiseData = root.toStyledString();
      std::string message = advertiseData;
      int ret = endpoint->send(id, message);
    }
    //
    for (std::vector<CRosSubscriber>::const_iterator sub = subs.begin(); sub != subs.end(); ++sub)
    {
      std::string subscribeData;
      Json::Value root;
      root["ip"] = ip;
      root["op"] = "subscribe";
      root["id"] = sub->id;
      root["topic"] = sub->name;
      root["type"] = sub->type;
      // root["throttle_rate"] = sub->throttle_rate;
      root["queue_length"] = sub->queue_length;
      // root["fragment_size"] = sub->fragment_size;
      // root["compression"] = sub->compression;
      subscribeData = root.toStyledString();
      std::string message = subscribeData;
      int ret = endpoint->send(id, message);
    }
  }
}