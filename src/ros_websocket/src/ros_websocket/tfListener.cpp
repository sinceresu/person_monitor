#include "tfListener.hpp"

/**
  * 函数名称:       CTfListener
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CTfListener::CTfListener()
{
}

/**
  * 函数名称:       ~CTfListener
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CTfListener::~CTfListener()
{
}

void CTfListener::tf_Callback(std::string &ip, std::string &msg)
{
  // printf("> tf_Callback << %s \r\n - \r\n", msg.c_str());

  mtx.lock();

  SRosTFMessage tfMsg;
  x2struct::X::loadjson(msg, tfMsg, false);

  // tfMsgs.push_back(tfMsg);
  // while (tfMsgs.size() > 100)
  //   tfMsgs.pop_front();

  tfMsgs_map[ip].push_back(tfMsg);
  while (tfMsgs_map[ip].size() > 50)
    tfMsgs_map[ip].pop_front();

  mtx.unlock();
}

SRosTransformStamped CTfListener::lookupTransform(std::string ip, std::string target_frame, std::string source_frame,
                                                  std::time_t time, std::time_t timeout)
{
  SRosTransformStamped center_pose;

  mtx.lock();

  // if (target_frame.substr(0, 1) != "/")
  //   target_frame = "/" + target_frame;
  // if (source_frame.substr(0, 1) != "/")
  //   source_frame = "/" + source_frame;

  // if (tfMsgs.size() > 0)
  // {
  //   for (std::list<SRosTFMessage>::reverse_iterator tf_it = tfMsgs.rbegin(); tf_it != tfMsgs.rend(); ++tf_it)

  //     for (std::vector<SRosTransformStamped>::iterator tf_s_it = tf_it->transforms.begin(); tf_s_it != tf_it->transforms.end(); ++tf_s_it)
  //     {
  //       // if (tf_s_it->header.frame_id == target_frame && tf_s_it->child_frame_id == source_frame)
  //       if (tf_s_it->header.frame_id.find(target_frame) != string::npos && tf_s_it->child_frame_id.find(source_frame) != string::npos)
  //       {
  //         center_pose = *tf_s_it;
  //       }
  //     }
  // }

  std::map<std::string, std::list<SRosTFMessage>>::iterator msg = tfMsgs_map.find(ip);
  if (msg != tfMsgs_map.end())
  {
    if (msg->second.size() > 0)
    {
      for (std::list<SRosTFMessage>::reverse_iterator tf_it = msg->second.rbegin(); tf_it != msg->second.rend(); ++tf_it)

        for (std::vector<SRosTransformStamped>::iterator tf_s_it = tf_it->transforms.begin(); tf_s_it != tf_it->transforms.end(); ++tf_s_it)
        {
          // if (tf_s_it->header.frame_id == target_frame && tf_s_it->child_frame_id == source_frame)
          if (tf_s_it->header.frame_id.find(target_frame) != string::npos && tf_s_it->child_frame_id.find(source_frame) != string::npos)
          {
            center_pose = *tf_s_it;
          }
        }
    }
  }

  mtx.unlock();

  return center_pose;
}