#include "rosService.hpp"
#include "rosNode.hpp"

/**
  * 函数名称:       CRosService
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosService::CRosService()
{
  p = this;
}

/**
  * 函数名称:       ~CRosService
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosService::~CRosService()
{
}

CRosService *CRosService::p = nullptr;

CRosService *CRosService::get_service()
{
  return p;
}

std::vector<CRosServer> CRosService::get_servers()
{
  return servers;
}

void CRosService::add_server(CRosServer _server)
{
  servers.push_back(_server);
}

void CRosService::remove_server(CRosServer _server)
{
  std::vector<CRosServer>::const_iterator it;
  for (it = servers.begin(); it != servers.end(); ++it)
  {
    if (*it == _server)
    {
      servers.erase(it);
    }
  }
}

void CRosService::clear_server()
{
  servers.clear();
}

std::vector<CRosClient> CRosService::get_clients()
{
  return clients;
}

void CRosService::add_client(CRosClient _client)
{
  clients.push_back(_client);
}

void CRosService::remove_client(CRosClient _client)
{
  std::vector<CRosClient>::const_iterator it;
  for (it = clients.begin(); it != clients.end(); ++it)
  {
    if (*it == _client)
    {
      clients.erase(it);
    }
  }
}

void CRosService::clear_client()
{
  clients.clear();
}