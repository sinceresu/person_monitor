#include "rosWebsocket.hpp"

#include "glog/logging.h"

#include "../common/Common.hpp"

bool cmp(const pair<std::string, Msg_t> &a, const pair<std::string, Msg_t> &b)
{
    time_t a_time = StrToTime_t(a.second.time, "%d-%d-%d %d:%d:%d");
    time_t b_time = StrToTime_t(b.second.time, "%d-%d-%d %d:%d:%d");

    return a_time < b_time;
}

connection_metadata::connection_metadata(int id, websocketpp::connection_hdl hdl, std::string uri)
    : m_id(id), m_hdl(hdl), m_status("Connecting"), m_uri(uri), m_server("N/A"),
      /* msg_mtx(), */ msg_pt_mutex_t(PTHREAD_MUTEX_INITIALIZER)
{
    // 获取配置
}

void connection_metadata::on_open(client *c, websocketpp::connection_hdl hdl)
{
    m_status = "Open";

    client::connection_ptr con = c->get_con_from_hdl(hdl);
    m_server = con->get_response_header("Server");
}

void connection_metadata::on_fail(client *c, websocketpp::connection_hdl hdl)
{
    m_status = "Failed";

    client::connection_ptr con = c->get_con_from_hdl(hdl);
    m_server = con->get_response_header("Server");
    m_error_reason = con->get_ec().message();
}

void connection_metadata::on_close(client *c, websocketpp::connection_hdl hdl)
{
    m_status = "Closed";

    client::connection_ptr con = c->get_con_from_hdl(hdl);
    std::stringstream s;
    s << "close code: " << con->get_remote_close_code() << " ("
      << websocketpp::close::status::get_string(con->get_remote_close_code())
      << "), close reason: " << con->get_remote_close_reason();
    m_error_reason = s.str();
}

void connection_metadata::on_message(websocketpp::connection_hdl, client::message_ptr msg)
{
    // if (msg->get_opcode() == websocketpp::frame::opcode::text)
    // {
    //     m_messages.push_back("<< " + msg->get_payload());
    // }
    // else
    // {
    //     m_messages.push_back("<< " + websocketpp::utility::to_hex(msg->get_payload()));
    // }

    std::string msg_data = "";
    if (msg->get_opcode() == websocketpp::frame::opcode::text)
    {
        msg_data = msg->get_payload();
    }
    else
    {
        msg_data = websocketpp::utility::to_hex(msg->get_payload());
    }

    msg_data = ReplaceString(msg_data, "\n", "");
    // msg_data = ReplaceString(msg_data, " ", "");

    record_message(msg_data, "Receive");

    // std::cout << "> on_message << " << msg_data << std::endl;

    for (std::vector<msg_callback_fun>::const_iterator it = m_callbacks.begin(); it != m_callbacks.end(); ++it)
    {
        (*it)(msg_data);
    }
}

websocketpp::connection_hdl connection_metadata::get_hdl() const
{
    return m_hdl;
}

int connection_metadata::get_id() const
{
    return m_id;
}

std::string connection_metadata::get_status() const
{
    return m_status;
}

void connection_metadata::record_sent_message(std::string message)
{
    m_messages.push_back(">> " + message);
}

void connection_metadata::record_message(std::string message, std::string type)
{
    msg_mtx.lock();
    // pthread_mutex_lock(&msg_pt_mutex_t);

    Json::Reader reader;
    Json::Value root;
    bool bRet = reader.parse(message, root);
    if (bRet == false)
    {
        LOG(ERROR) << "Json序列化错误 ... " << type <<  " Message : "  << message;
        return;
    }

    std::string key = "";
    std::string time = "";
    std::string op = "";
    std::string topic = "";
    std::string service = "";
    op = root["op"].asString();
    topic = root["topic"].asString();
    service = root["service"].asString();
    key = topic == "" ? service : topic;
    key = service == "" ? topic : service;
    time = GetCurrentDate();

    Msg_t msg;
    msg.time = time;
    msg.type = type;
    msg.op = op;
    msg.topic = topic;
    msg.service = service;
    msg.msg = message;

    if (type == "Send")
    {
        std::map<std::string, Msg_t>::const_iterator msg_it = send_msgs.find(key);
        if (msg_it == send_msgs.end())
        {
            send_msgs.insert(pair<std::string, Msg_t>(key, msg));
        }
        else
        {
            send_msgs[key] = msg;
        }
    }
    if (type == "Receive")
    {
        std::map<std::string, Msg_t>::const_iterator msg_it = receive_msgs.find(key);
        if (msg_it == receive_msgs.end())
        {
            receive_msgs.insert(pair<std::string, Msg_t>(key, msg));
        }
        else
        {
            receive_msgs[key] = msg;
        }
    }

    msg_mtx.unlock();
    // pthread_mutex_unlock(&msg_pt_mutex_t);

    if (std::find(topic_arr.begin(), topic_arr.end(), topic) != topic_arr.end() ||
        std::find(service_arr.begin(), service_arr.end(), service) != service_arr.end())
    {
        LOG(INFO) << type << " Message " << message;
    }
}

void connection_metadata::record_sent_callback(msg_callback_fun _callback)
{
    m_callbacks.push_back(_callback);
}

std::ostream &operator<<(std::ostream &out, connection_metadata const &data)
{
    out << "> ID: " << data.m_id << "\n"
        << "> URI: " << data.m_uri << "\n"
        << "> Status: " << data.m_status << "\n"
        << "> Remote Server: " << (data.m_server.empty() ? "None Specified" : data.m_server) << "\n"
        << "> Error/close reason: " << (data.m_error_reason.empty() ? "N/A" : data.m_error_reason) << "\n";
    out << "> Messages Processed: (" << data.m_messages.size() << ") \n";
    out << "\n";

    std::vector<std::string>::const_iterator it;
    for (it = data.m_messages.begin(); it != data.m_messages.end(); ++it)
    {
        out << *it << "\n";
    }

    return out;
}

void connection_metadata::printf_metadata()
{
    std::cout << "> ID: " << m_id << "\n"
              << "> URI: " << m_uri << "\n"
              << "> Status: " << m_status << "\n"
              << "> Remote Server: " << (m_server.empty() ? "None Specified" : m_server) << "\n"
              << "> Error/close reason: " << (m_error_reason.empty() ? "N/A" : m_error_reason) << "\n";
    std::cout << "> Messages Processed: (" << send_msgs.size() + receive_msgs.size() << ") \n";
    std::cout << "\n";
}

void connection_metadata::printf_message()
{
    msg_mtx.lock();
    // pthread_mutex_lock(&msg_pt_mutex_t);

    std::cout << "> Messages: (" << send_msgs.size() + receive_msgs.size() << ") \n";
    std::cout << std::endl;

    std::vector<pair<std::string, Msg_t>> vec(send_msgs.begin(), send_msgs.end());
    vec.insert(vec.begin(), receive_msgs.begin(), receive_msgs.end());
    std::sort(vec.begin(), vec.end(), cmp);
    for (std::vector<pair<std::string, Msg_t>>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        std::string symbol = "--";
        if (it->second.type == "Send")
        {
            symbol = ">>";
        }
        if (it->second.type == "Receive")
        {
            symbol = "<<";
        }

        std::cout << " " << symbol << " "
                  << "[" << it->second.time << "]"
                  << "\"" << it->second.msg << "\""
                  << "\n";
    }

    msg_mtx.unlock();
    // pthread_mutex_unlock(&msg_pt_mutex_t);
}

/**
  * websocket_endpoint
*/
websocket_endpoint::websocket_endpoint() : m_next_id(1)
{
    m_endpoint.clear_access_channels(websocketpp::log::alevel::all);
    m_endpoint.clear_error_channels(websocketpp::log::elevel::all);

    m_endpoint.init_asio();
    m_endpoint.start_perpetual();

    m_thread = websocketpp::lib::make_shared<websocketpp::lib::thread>(&client::run, &m_endpoint);
}

websocket_endpoint::~websocket_endpoint()
{
    m_endpoint.stop_perpetual();

    for (con_list::const_iterator it = m_connection_list.begin(); it != m_connection_list.end(); ++it)
    {
        if (it->second->get_status() != "Open")
        {
            // Only close open connections
            continue;
        }

        std::cout << "> Closing connection " << it->second->get_id() << std::endl;

        websocketpp::lib::error_code ec;
        m_endpoint.close(it->second->get_hdl(), websocketpp::close::status::going_away, "", ec);
        if (ec)
        {
            std::cout << "> Error closing connection " << it->second->get_id() << ": "
                      << ec.message() << std::endl;
        }
    }

    m_thread->join();
}

int websocket_endpoint::connect(std::string const &uri)
{
    websocketpp::lib::error_code ec;

    client::connection_ptr con = m_endpoint.get_connection(uri, ec);

    if (ec)
    {
        std::cout << "> Connect initialization error: " << ec.message() << std::endl;
        return -1;
    }

    int new_id = m_next_id++;
    connection_metadata::ptr metadata_ptr = websocketpp::lib::make_shared<connection_metadata>(new_id, con->get_handle(), uri);
    m_connection_list[new_id] = metadata_ptr;

    con->set_open_handler(websocketpp::lib::bind(
        &connection_metadata::on_open,
        metadata_ptr,
        &m_endpoint,
        websocketpp::lib::placeholders::_1));
    con->set_fail_handler(websocketpp::lib::bind(
        &connection_metadata::on_fail,
        metadata_ptr,
        &m_endpoint,
        websocketpp::lib::placeholders::_1));
    con->set_close_handler(websocketpp::lib::bind(
        &connection_metadata::on_close,
        metadata_ptr,
        &m_endpoint,
        websocketpp::lib::placeholders::_1));
    con->set_message_handler(websocketpp::lib::bind(
        &connection_metadata::on_message,
        metadata_ptr,
        websocketpp::lib::placeholders::_1,
        websocketpp::lib::placeholders::_2));

    m_endpoint.connect(con);

    while (m_connection_list[new_id]->get_status() == "Connecting")
    {
        usleep(100);
    }

    return new_id;
}

int websocket_endpoint::connect(int const &id, std::string const &uri)
{
    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it != m_connection_list.end())
    {
        try
        {
            websocketpp::lib::error_code ec;
            m_endpoint.close(metadata_it->second->get_hdl(), websocketpp::close::status::going_away, "", ec);
            if (ec)
            {
                std::cout << "> Error initiating close: " << ec.message() << std::endl;
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
        }
        m_connection_list.erase(id);
    }

    return connect(uri);
}

void websocket_endpoint::close(int id, websocketpp::close::status::value code, std::string reason)
{
    websocketpp::lib::error_code ec;

    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        std::cout << "> No connection found with id " << id << std::endl;
        return;
    }

    m_endpoint.close(metadata_it->second->get_hdl(), code, reason, ec);
    if (ec)
    {
        std::cout << "> Error initiating close: " << ec.message() << std::endl;
    }
}

int websocket_endpoint::send(int id, std::string message)
{
    websocketpp::lib::error_code ec;

    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        std::cout << "> No connection found with id " << id << std::endl;
        return -1;
    }

    m_endpoint.send(metadata_it->second->get_hdl(), message, websocketpp::frame::opcode::text, ec);
    if (ec)
    {
        std::cout << "> Error sending message: " << ec.message() << std::endl;
        return -1;
    }

    message = ReplaceString(message, "\n", "");
    // message = ReplaceString(message, " ", "");

    // metadata_it->second->record_sent_message(message);
    metadata_it->second->record_message(message, "Send");

    return 0;
}

int websocket_endpoint::register_msg_callback(int id, msg_callback_fun _callback)
{
    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        std::cout << "> No connection found with id " << id << std::endl;
        return -1;
    }

    metadata_it->second->record_sent_callback(_callback);

    return 0;
}

std::string websocket_endpoint::get_status(int id)
{
    con_list::iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        std::cout << "> No connection found with id " << id << std::endl;
        return "";
    }

    std::string status = metadata_it->second->get_status();

    return status;
}

connection_metadata::ptr websocket_endpoint::get_metadata(int id) const
{
    con_list::const_iterator metadata_it = m_connection_list.find(id);
    if (metadata_it == m_connection_list.end())
    {
        return connection_metadata::ptr();
    }
    else
    {
        return metadata_it->second;
    }
}

void websocket_endpoint::printf_metadata(int id)
{
    con_list::const_iterator metadata_it = m_connection_list.find(id);
    if (metadata_it != m_connection_list.end())
    {
        metadata_it->second->printf_metadata();
    }
}

void websocket_endpoint::printf_message(int id)
{
    con_list::const_iterator metadata_it = m_connection_list.find(id);
    if (metadata_it != m_connection_list.end())
    {
        metadata_it->second->printf_message();
    }
}
