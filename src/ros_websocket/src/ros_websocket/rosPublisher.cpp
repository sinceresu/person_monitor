#include "rosPublisher.hpp"
#include "rosTopic.hpp"

/**
  * 函数名称:       CRosPublisher
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosPublisher::CRosPublisher()
{
}

/**
  * 函数名称:       CRosPublisher
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosPublisher::CRosPublisher(const std::string &_ip, const std::string &_name, const std::string &_type,
                             const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint)
    : ip(_ip),
      id(to_string(boost::uuids::random_generator()())),
      name(_name),
      type(_type),
      state("advertise"),
      node_id(_nodeId),
      node_name(_nodeName),
      endpoint(_endpoint)
{
}

/**
  * 函数名称:       ~CRosPublisher
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosPublisher::~CRosPublisher()
{
}

void CRosPublisher::unadvertise()
{
  if (state == "" || state == "unadvertise" || endpoint == nullptr || node_id == 0)
  {
    return;
  }

  std::string unadvertiseData;
  Json::Value root;
  root["ip"] = ip;
  root["op"] = "unadvertise";
  root["id"] = id;
  root["topic"] = name;
  unadvertiseData = root.toStyledString();

  std::string message = unadvertiseData;

  int ret = endpoint->send(node_id, message);
  if (ret != 0)
  {
    return;
  }

  CRosTopic::get_topic()->remove_publisher(*this);

  ip = "",
  id = "",
  name = "",
  type = "",
  state = "",
  node_id = 0,
  node_name = "",
  endpoint = nullptr;
}

void CRosPublisher::publish(std::string _ip, std::string _msg)
{
  if (state == "unadvertise" || endpoint == nullptr || node_id == 0)
  {
    return;
  }

  Json::Reader reader;
  Json::Value Value;
  bool b = reader.parse(_msg, Value);
  if (b == false)
  {
    return;
  }

  std::string publishData;
  Json::Value root;
  root["ip"] = _ip;
  root["op"] = "publish";
  root["id"] = id;
  root["topic"] = name;
  root["msg"] = Value;
  publishData = root.toStyledString();

  std::string message = publishData;

  int ret = endpoint->send(node_id, message);
  if (ret != 0)
  {
  }
}

bool CRosPublisher::operator==(const CRosPublisher m) const
{
  if (ip == m.ip && id == m.id && name == m.name && type == m.type && state == m.state &&
      node_id == m.node_id && node_name == m.node_name && endpoint == m.endpoint)
  {
    return true;
  }

  return false;
}

bool CRosPublisher::operator!=(const CRosPublisher m) const
{
  if (ip != m.ip || id != m.id || name != m.name || type != m.type || state != m.state ||
      node_id != m.node_id || node_name != m.node_name || endpoint != m.endpoint)
  {
    return true;
  }

  return false;
}