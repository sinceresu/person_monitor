<!-- init -->
### 连接
# CRosNode ros_node;
# ros_node.init_connect("url", "name");

<!-- ros server -->
#### 服务
# bool class::callback_fun(string &req, string &res)
# CRosServer server = ros_node.advertiseService("/server", "type", boost::bind(&class::callback_fun, this, _1, _2));

<!-- ros client -->
### 客户端
# CRosClient client = ros_node.serviceClient("/server", "type");
# string data = "{\"request\":{\msg\":0},\"response\":{}}";
# if (client.call(data))` 
# {
#     printf("%s", data.c_str());
# }
# else  
# {
#     <!--  -->
# }

<!-- ros talker -->
### 发布Topic
# CRosPublisher publisher = ros_node.advertise("/topic", "type"); 
# string data = "{\msg\":0}";
# publisher.publish(data);

<!-- ros listener -->
### 订阅Topic
# void class::callback_fun(string msg);
# CRosSubscriber subscriber = ros_node.subscribe("/topic", "type", boost::bind(&class::callback_fun, this, _1), 10);