#include "Global.hpp"

/**
 * Description:      创建目录
 * Function:            CreateDirs
 * Parameters:       dirPath:世界格式
 * Return:                  bool:true成功,false失败
 * Author:
 * Date:
*/
bool CreateDirs(const std::string &dirPath)
{
    uint32_t beginCmpPath = 0;
    uint32_t endCmpPath = 0;
    std::string fullPath = "";

    if ('/' != dirPath[0])
    {
        // 当前工作目录的绝对路径
        fullPath = getcwd(nullptr, 0);
        beginCmpPath = fullPath.size();
        fullPath = fullPath + "/" + dirPath;
    }
    else
    {
        fullPath = dirPath;
        beginCmpPath = 1;
    }

    if (fullPath[fullPath.size() - 1] != '/')
    {
        fullPath += "/";
    }

    endCmpPath = fullPath.size();
    for (uint32_t i = beginCmpPath; i < endCmpPath; i++)
    {
        if ('/' == fullPath[i])
        {
            std::string curPath = fullPath.substr(0, i);
            // 判断目标文件夹是否存在
            if (access(curPath.c_str(), F_OK) != 0)
            {
                if (mkdir(curPath.c_str(), S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH) == -1)
                {
                    return false;
                }
            }
        }
    }

    return true;
}