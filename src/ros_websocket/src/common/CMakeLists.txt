cmake_minimum_required(VERSION 3.0.2)
project(common)


aux_source_directory(./ common_LIB_SRCS)

# 生成链接库
add_library(${PROJECT_NAME} STATIC ${common_LIB_SRCS} )

target_link_libraries(${PROJECT_NAME} crypto ssl)
