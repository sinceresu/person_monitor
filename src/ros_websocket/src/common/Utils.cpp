#include "Utils.hpp"

int get_workdir(std::string &work_path)
{
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        work_path = ".";
        return -1;
    }
    else
    {
        work_path = buffer;
        free(buffer);
    }

    return 0;
}

int md5_encrypt(const char *input, char *output)
{
    int nRet = 0;
    //
    unsigned char md[MD5_DIGEST_LENGTH];
    memset(md, 0, MD5_DIGEST_LENGTH);
    char buf[MD5_DIGEST_LENGTH * 2 + 1];
    memset(buf, 0, MD5_DIGEST_LENGTH * 2 + 1);
    //
    MD5_CTX ctx;
    // 1. 初始化
    if (MD5_Init(&ctx) != 1)
    {
        nRet = -1;
        printf("MD5_Init failed ... \r\n");
    }
    // 2. 添加数据
    if (MD5_Update(&ctx, (const void *)input, strlen((char *)input)) != 1)
    {
        nRet = -1;
        printf("MD5_Update failed ... \r\n");
    }
    // 3. 计算结果
    if (MD5_Final(md, &ctx) != 1)
    {
        nRet = -1;
        printf("MD5_Final failed ... \r\n");
    }
    // 4. 输出结果
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&buf[i * 2], "%02x", md[i]);
        // sprintf(&buf[i * 2], "%02X", md[i]);
    }
    strcpy(output, buf);
    //
    // MD5((unsigned char *)input, strlen((char *)input), md);
    // for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    // {
    //   sprintf(&buf[i * 2], "%02x", md[i]);
    //   // sprintf(&buf[i * 2], "%02X", md[i]);
    // }
    // strcpy(output, buf);

    return nRet;
}

char *md5_hex2c(unsigned char *in_md5_c, char *out_md5_c)
{
    for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
    {
        // 小写
        sprintf(out_md5_c + i * 2, "%02x", in_md5_c[i]);
        // 大写
        // sprintf(out_md5_c + i * 2, "%02X", in_md5_c[i]);
    }
    out_md5_c[MD5_DIGEST_LENGTH * 2] = '\0';

    return out_md5_c;
}
