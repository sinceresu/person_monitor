#ifndef utils_HPP
#define utils_HPP

#pragma once
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <openssl/md5.h>

int get_workdir(std::string &work_path);
int md5_encrypt(const char *input, char *output);
char *md5_hex2c(unsigned char *in_md5_c, char *out_md5_c);

#endif