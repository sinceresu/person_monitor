#ifndef rosPublisher_HPP
#define rosPublisher_HPP

#pragma once
#include "rosType.hpp"

using namespace std;

class CRosPublisher
{
public:
    CRosPublisher();
    CRosPublisher(const std::string &_ip, const std::string &_name, const std::string &_type,
                  const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint);
    ~CRosPublisher();

private:
    websocket_endpoint *endpoint;
    int node_id;
    std::string node_name;

public:
    void unadvertise();
    void publish(std::string _ip, std::string _msg);

public:
    std::string ip;
    std::string id;
    std::string name;
    std::string type;
    //
    std::string state;

public:
    bool operator==(const CRosPublisher m) const;
    bool operator!=(const CRosPublisher m) const;
};

#endif