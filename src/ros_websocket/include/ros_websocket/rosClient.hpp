#ifndef rosClient_HPP
#define rosClient_HPP

#pragma once
#include "rosType.hpp"

using namespace std;

typedef struct resMsg
{
    int exit_flag;
    std::string res_ip;
    std::string res_msg;
} SresMsg, *PresMsg;

class CRosClient
{
public:
    CRosClient();
    CRosClient(const std::string &_ip, const std::string &_name, const std::string &_type,
               const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint);
    ~CRosClient();

public:
    std::string ip;
    std::string id;
    std::string name;
    std::string type;
    unsigned int fragment_size;
    std::string compression;
    //
    std::string state;

public:
    bool call(std::string &_ip, std::string &_msg);
    void unserviceClient();

    static std::map<std::string, SresMsg> res_msg;

private:
    websocket_endpoint *endpoint;
    int node_id;
    std::string node_name;

public:
    bool operator==(const CRosClient m) const;
    bool operator!=(const CRosClient m) const;
};

#endif