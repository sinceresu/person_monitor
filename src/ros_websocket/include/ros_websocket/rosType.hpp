#ifndef rosType_HPP
#define rosType_HPP

#pragma once
#include <string>
#include <vector>
#include <map>
#include <boost/random.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "jsoncpp/json/json.h"
#include "jsoncpp/json/value.h"
#include "jsoncpp/json/writer.h"
#include "rosWebsocket.hpp"
#include "x2struct/x2struct.hpp"

// ros struct
typedef struct RosInt
{
    int data;
    XTOSTRUCT(O(data));
} SRosInt, *PRosInt;

typedef struct RosString
{
    std::string data;
    XTOSTRUCT(O(data));
} SRosString, *PRosString;

typedef struct RosBool
{
    bool data;
    XTOSTRUCT(O(data));
} SRosBool, *PRosBool;

typedef struct RosTime
{
    unsigned int secs;
    unsigned int nsecs;
    XTOSTRUCT(O(secs, nsecs));
} SRosTime, *PRosTime;

typedef struct RosHeader
{
    unsigned int seq;
    SRosTime stamp;
    std::string frame_id;
    XTOSTRUCT(O(seq, stamp, frame_id));
} SRosHeader, *PRosHeader;

typedef struct RosPoint
{
    double x;
    double y;
    double z;
    RosPoint() : x(0), y(0), z(0) {}
    XTOSTRUCT(O(x, y, z));
} SRosPoint, *PRosPoint;

typedef struct RosQuaternion
{
    double x;
    double y;
    double z;
    double w;
    RosQuaternion() : x(0), y(0), z(0), w(0) {}
    XTOSTRUCT(O(x, y, z, w));
} SRosQuaternion, *PRosQuaternion;

typedef struct RosPose
{   
    SRosPoint position;
    SRosQuaternion orientation;
    XTOSTRUCT(O(position, orientation));
} SRosPose, *PRosPose;

typedef struct RosPoseWithCovariance
{
    SRosPose pose;
    // double covariance[36];
    std::vector<double> covariance;
    XTOSTRUCT(O(pose, covariance));
} SRosPoseWithCovariance, *PRosPoseWithCovariance;

typedef struct RosVector3
{
    double x;
    double y;
    double z;
    RosVector3() : x(0), y(0), z(0) {}
    XTOSTRUCT(O(x, y, z));
} SRosVector3, *PRosVector3;

typedef struct RosTwist
{
    SRosVector3 linear;
    SRosVector3 angular;
    XTOSTRUCT(O(linear, angular));
} SRosTwist, *PRosTwist;

typedef struct RosTwistWithCovariance
{
    SRosTwist twist;
    // double covariance[36];
    std::vector<double> covariance;
    XTOSTRUCT(O(twist, covariance));
} SRosTwistWithCovariance, *PRosTwistWithCovariance;

typedef struct RosOdometry
{
    SRosHeader header;
    std::string child_frame_id;
    SRosPoseWithCovariance pose;
    SRosTwistWithCovariance twist;
    XTOSTRUCT(O(header, child_frame_id, pose, twist));
} SRosOdometry, *PRosOdometry;

typedef struct RosPoseWithCovarianceStamped
{
    SRosHeader header;
    SRosPoseWithCovariance pose;
    XTOSTRUCT(O(header, pose));
} SRosPoseWithCovarianceStamped, *PRosPoseWithCovarianceStamped;

typedef struct RosKeyValue
{
    std::string key;
    std::string value;
    XTOSTRUCT(O(key, value));
} SRosKeyValue, *PRosKeyValue;

typedef struct RosDiagnosticStatus
{
    unsigned char level;
    std::string name;
    std::string message;
    std::string hardware_id;
    std::vector<SRosKeyValue> values;
    XTOSTRUCT(O(level, name, message, hardware_id, values));
} SRosDiagnosticStatus, *PRosDiagnosticStatus;

typedef struct RosDiagnosticArray
{
    SRosHeader header;
    std::vector<SRosDiagnosticStatus> status;
    XTOSTRUCT(O(header, status));
} SRosDiagnosticArray, *PRosDiagnosticArray;

typedef struct RosTransform
{
    SRosVector3 translation;
    SRosQuaternion rotation;
    XTOSTRUCT(O(translation, rotation));
} SRosTransform, *PRosTransform;

typedef struct RosTransformStamped
{
    SRosHeader header;
    std::string child_frame_id;
    SRosTransform transform;
    XTOSTRUCT(O(header, child_frame_id, transform));
} SRosTransformStamped, *PRosTransformStamped;

typedef struct RosTFMessage
{
    std::vector<SRosTransformStamped> transforms;
    XTOSTRUCT(O(transforms));
} SRosTFMessage, *PRosTFMessage;

typedef struct RosPoseStamped
{
    SRosHeader header;
    SRosPose pose;
    XTOSTRUCT(O(header, pose));
} SRosPoseStamped, *PRosPoseStamped;

typedef struct RosPoint32
{
    float x;
    float y;
    float z;
    RosPoint32() : x(0), y(0), z(0) {}
    XTOSTRUCT(O(x, y, z));
} SRosPoint32, *PRosPoint32;

typedef struct RosPolygon
{
    std::vector<SRosPoint32> points;
    XTOSTRUCT(O(points));
} SRosPolygon, *PRosPolygon;

typedef struct RosImage
{
    SRosHeader header;
    unsigned int width;
    unsigned int height;
    std::string encoding;
    unsigned char is_bigendian;
    unsigned int step;
    // std::vector<unsigned char> data;
    std::string data;
    XTOSTRUCT(O(header, width, height, encoding, is_bigendian, step, data));
} SRosImage, *PRosImage;

// path planning
typedef struct RosPathPlanningReq
{
    int type;
    SRosPoint start;
    SRosPoint current;
    SRosPoint end;
    XTOSTRUCT(O(type, start, current, end));
} SRosPathPlanningReq, *PRosPathPlanningReq;

typedef struct RosPathPlanningRes
{
    int nResult;
    SRosPoint target;
    XTOSTRUCT(O(nResult, target));
} SRosPathPlanningRes, *PRosPathPlanningRes;

typedef struct RosPathPlanningServer
{
    SRosPathPlanningReq request;
    SRosPathPlanningRes response;
    XTOSTRUCT(O(request, response));
} SRosPathPlanningServer, *PRosPathPlanningServer;

typedef struct RosTaskListReq
{
    std::string plan;
    XTOSTRUCT(O(plan));
} SRosTaskListReq, *PRosTaskListReq;

typedef struct RosTaskListRes
{
    bool status;
    XTOSTRUCT(O(status));
} SRosTaskListRes, *PRosTaskListRes;

typedef struct RosTaskListServer
{
    SRosTaskListReq request;
    SRosTaskListRes response;
    XTOSTRUCT(O(request, response));
} SRosTaskListServer, *PRosTaskListServer;

typedef struct RosTaskPlanStatus
{
    int robotId;
    int status;
    XTOSTRUCT(O(robotId, status));
} SRosTaskPlanStatus, *PRosTaskPlanStatus;

typedef struct RosTaskDeliver
{
    int task_history_id;
    int robot_id;
    XTOSTRUCT(O(task_history_id, robot_id));
} SRosTaskDeliver, *PRosTaskDeliver;

typedef struct RosOneButton
{
    int robot_id;
    XTOSTRUCT(O(robot_id));
} SRosOneButton, *PRosOneButton;

// data center

typedef struct RosDataCenterReq
{
    int type;
    std::string table_name;
    XTOSTRUCT(O(type, table_name));
} SRosDataCenterReq, *PRosDataCenterReq;

typedef struct RosDataCenterRes
{
    int nResult;
    XTOSTRUCT(O(nResult));
} SRosDataCenterRes, *PRosDataCenterRes;

typedef struct RosDataCenterServer
{
    SRosDataCenterReq request;
    SRosDataCenterRes response;
    XTOSTRUCT(O(request, response));
} SRosDataCenterServer, *PRosDataCenterServer;

typedef struct RosTaskRealStatusWeb
{
    int task_history_id;
    int robot_id;
    int task_id;
    unsigned char task_status;
    XTOSTRUCT(O(task_history_id, robot_id, task_id, task_status));
} SRosTaskRealStatusWeb, *PRosTaskRealStatusWeb;

typedef struct RosInspectedResult
{
    int camid;
    int picid;
    float x;
    float y;
    float z;
    // std::vector<unsigned char> equipimage;
    std::string equipimage;
    // std::vector<unsigned char> nameplates;
    std::string nameplates;
    std::string equipid;
    std::string result;
    bool success;
    XTOSTRUCT(O(camid, picid, x, y, z, equipimage, nameplates, equipid, result, success));
} SRosInspectedResult, *PRosInspectedResult;

typedef struct RosTaskJsonStatus
{
    int robot_id;
    int task_history_id;
    int index;
    int status;
    XTOSTRUCT(O(robot_id, task_history_id, index, status));
} SRosTaskJsonStatus, *PRosTaskJsonStatus;

typedef struct RosWeather
{
    int robot_id;
    int switch1;
    int switch2;
    float wind_speed;
    float wind_direction;
    float humidity;
    float temperature;
    float airpressure;
    float minute_rainfall;
    float hour_rainfall;
    float day_rainfall;
    float all_rainfall;
    float battery_info;
    XTOSTRUCT(O(robot_id, switch1, switch2, wind_speed, wind_direction, humidity, temperature,
                airpressure, minute_rainfall, hour_rainfall, day_rainfall, all_rainfall, battery_info));
} SRosWeather, *PRosWeather;

typedef struct RosInspectedResultWeb
{
    int task_history_id;
    int robot_id;
    int object_id;
    int point_id;
    std::string task_name;
    float value;
    std::string name;
    std::string object_name;
    unsigned char alarm_type_id;
    unsigned char alarm_level;
    std::string recon_time;
    std::string recon_type_name;
    int recon_type_id;
    int save_type_id;
    std::string pic_address;
    XTOSTRUCT(O(task_history_id, robot_id, object_id, point_id, task_name, value, name, object_name,
                alarm_type_id, alarm_level, recon_time, recon_type_name, recon_type_id, save_type_id, pic_address));
} SRosInspectedResultWeb, *PRosInspectedResultWeb;

typedef struct RosAlarmMessage
{
    int robotId;
    std::string errorCode;
    std::string errorValue;
    std::string alarmTime;
    int alarmType;
    int alarmLevel;
    std::string alarmDesc;
    XTOSTRUCT(O(robotId, errorCode, errorValue, alarmTime, alarmType, alarmLevel, alarmDesc));
} SRosAlarmMessage, *PRosAlarmMessage;

// data server
typedef struct RosDeviceParamReq
{
    std::string param;
    XTOSTRUCT(O( param));
} SRosDeviceParamReq, *PRosDeviceParamReq;

typedef struct RosDeviceParamRes
{
    int result;
    std::string data;
    XTOSTRUCT(O(result, data));
} SRosDeviceParamRes, *PRosDeviceParamRes;

typedef struct RosDeviceParamServer
{
    SRosDeviceParamReq request;
    SRosDeviceParamRes response;
    XTOSTRUCT(O(request, response));
} SRosDeviceParamServer, *PRosDeviceParamServer;

typedef struct RosPoseKeypoints
{
    SRosHeader header;
    std::string cam_id;
    uint32_t person_id;
    SRosPolygon polygon;
    XTOSTRUCT(O(header, cam_id, person_id, polygon));
} SRosPoseKeypoints, *PRosPoseKeypoints;

typedef struct RosPersonTrack
{
    SRosHeader header;
    std::string cam_id;
    std::vector<uint32_t> person_id;
    std::vector<SRosPolygon> person_rect;
    uint32_t width;
    uint32_t height;
    std::string encoding;
    // std::vector<unsigned char> image;
    std::string image;
    XTOSTRUCT(O(header, cam_id, person_id, person_rect, width, height, encoding, image));
} SRosPersonTrack, *PRosPersonTrack;

typedef struct RosPersonList
{
    SRosHeader header;
    std::string cam_id;
    std::vector<uint32_t> person_id;
    XTOSTRUCT(O(header, cam_id, person_id));
} SRosPersonList, *PRosPersonList;

typedef struct RosPersonPose
{
    SRosHeader header;
    std::string cam_id;
    uint32_t person_id;
    SRosPose pose;
    XTOSTRUCT(O(header, cam_id, person_id, pose));
} SRosPersonPose, *PRosPersonPose;

#endif
