#include "cam_param_reader.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include<pcl/io/pcd_io.h>


using namespace std;

namespace person_monitor{

namespace person_location{


CamParamReader::CamParamReader() 
{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

CamParamReader::~CamParamReader() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}
 int CamParamReader::LoadIntrinsic(const std::string& calibration_string) {
	cv::FileStorage fs(calibration_string, cv::FileStorage::READ + cv::FileStorage::MEMORY);
  fs["CameraMat"] >> intrinsic_mat_;
  fs["DistCoeff"] >> distortion_coeffs_;
  fs["ImageSize"] >> image_size_;
  return 0;
 }

 int CamParamReader::LoadExtrinsic(const std::string& calibration_string) {
	cv::FileStorage fs(calibration_string, cv::FileStorage::READ + cv::FileStorage::MEMORY);
  cv::Mat cam_extrinsic_mat;
  fs["CameraExtrinsicMat"] >> cam_extrinsic_mat;
  cv::cv2eigen(cam_extrinsic_mat, extrinsic_mat_);

  return 0;
 }


 int CamParamReader::LoadCalibrationString(const std::string& calibration_string) {
  int result =    LoadIntrinsic(calibration_string);
  result = LoadExtrinsic(calibration_string);
  return result;
}

int CamParamReader::SetRotations(const vector<double> &rads)
{
    return 0;
}

} // namespace person_location
}// namespace person_monitor