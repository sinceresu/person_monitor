#pragma once

#include <map>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "ros/ros.h"
#include "person_monitor_msgs/PoseKeypoints.h"
#include "person_localizer.h"
#include "monitor_common/device_list_parser.h"
#include "common/Common.hpp"
#include "ros_websocket/rosNode.hpp"
#include "http_com/http_client.hpp"

namespace person_monitor {
namespace  person_location{
class PersonLocalizer;
class LocationNode {
 public:
   struct LocationNodeOptions {
    std::string http_url;
    std::string master_ip;
    std::string client_ips;
    std::string calibration_filepath;    
    std::string cloud_map_filepath;    
    std::string openpose_topic;
    std::string ptz_pose_topic;

    std::string location_topic;
    std::string frame_id;
    float horizontal_fov;
    float vertical_fov;
    float near_plane_distance;
    float far_plane_distance;
  };

  LocationNode(const LocationNodeOptions& options);
  ~LocationNode();

  LocationNode(const LocationNode&) = delete;
  LocationNode& operator=(const LocationNode&) = delete;

  bool ConnectToServer();
  bool RegisterMsg();
  void ConnectCheck(double msec);
  bool Initialize();
  void StartLocation();

  private:
    void response_callback(int code, const char *response, int lenght, void *ud);
    bool GetCameraList();
    bool InitializeLocalizers();

    void OpenPoseCallback(string &ip, string &msg);
    void PtzPoseCallback(string &ip, string &msg);

    LocationNodeOptions node_options_;
    ::ros::NodeHandle node_handle_;

    std::map<std::string, std::shared_ptr<PersonLocalizer>> pose_localizers_;
    std::map<std::string, PersonLocalizer::Calibration_t> camera_calib_datas_;    //cam_id, Calibration_t

    CRosNode ros_node_;
    CRosClient device_param_client_;
     CRosSubscriber person_pose2d_subscriber_;
     CRosSubscriber ptz_pose_subscriber_;
     CRosPublisher person_pose3d_publisher_;

     monitor_common::DeviceListParser::DeviceList device_list;

  };
  
}
}
