#pragma once
#include <string>
#include <deque>
#include <set>
#include <mutex>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/voxel_grid_occlusion_estimation.h>

namespace person_monitor{

namespace person_location{
class CamParamReader;

class PersonLocalizer 
{
public:
struct Calibration_t {
  int rotate;
  std::string calibration;
};

public:
  explicit PersonLocalizer() ;

  virtual ~PersonLocalizer();

  PersonLocalizer(const PersonLocalizer&) = delete;
  PersonLocalizer& operator=(const PersonLocalizer&) = delete;
  int SetCloudMap(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud_map);
  int LoadCalibrationString(const std::string& calibration_string, bool rotate = false) ;
  int SetRotations(const std::vector<double> rotations) ;

  int SetCullingParamters( float horizontal_fov, 
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance) ;
                          
int Localize( std::vector<cv::Point2f> poses, cv::Point3f& person_position);
private:
 pcl::PointCloud<pcl::PointXYZRGB>::Ptr CullingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud, const Eigen::Matrix4f& parent_link_pose) ;

  int GenerateDistanceImage();
  void UpdateExtrinsic();

  // void InitializeOcclusionFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  Eigen::Matrix4f camara_to_ref_pose_;
  Eigen::Matrix4f camara_to_ref_for_culling_;
  Eigen::Matrix4f ref_to_camera_pose_;

  cv::Mat rotation_vec_;
  cv::Mat transition_vec_;

  float horizontal_fov_;
  float vertical_fov_;
  float near_plane_distance_; 
  float far_plane_distance_;

  bool extrinsic_need_update_ = true;
  
  std::shared_ptr<CamParamReader> cam_param_reader_;
  std::mutex extrinsic_mutex_;
  
  cv::Mat distance_image_;
  cv::Mat point3d_image_;

  cv::Mat rgb_image_;

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_map_;
 };
} // namespace person_location
}// namespace person_monitor
