/*
 * Copyright 2016 The Cartographer Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ros/ros.h"

#include "glog/logging.h"
#include "gflags/gflags.h"
#include "ros_log_sink.h"


#include "location_node.h"



DEFINE_string(http_url, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(master_ip, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(client_ips, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(calibration_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(cloud_map_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

namespace person_monitor {
namespace  person_location{
void Run() {

  std::string openpose_topic;
  std::string ptz_pose_topic;
  std::string location_topic;
  std::string frame_id;

  float horizontal_fov;
  float vertical_fov;
  float near_plane_distance;
  float far_plane_distance;


  ros::param::get("~openpose_topic", openpose_topic);
  ros::param::get("~ptz_pose_topic", ptz_pose_topic);
  ros::param::get("~location_topic", location_topic);
  ros::param::get("~frame_id", frame_id);

  ros::param::get("~horizontal_fov", horizontal_fov);
  ros::param::get("~vertical_fov", vertical_fov);
  ros::param::get("~near_plane_distance", near_plane_distance);
  ros::param::get("~far_plane_distance", far_plane_distance);

  LOG(INFO) << "Pose position node started.";

  LocationNode::LocationNodeOptions node_options;
  node_options.http_url = FLAGS_http_url;
  node_options.master_ip = FLAGS_master_ip;
  node_options.client_ips = FLAGS_client_ips;
  node_options.calibration_filepath = FLAGS_calibration_filepath;
  node_options.cloud_map_filepath = FLAGS_cloud_map_filepath;
  node_options.openpose_topic = openpose_topic;
  node_options.ptz_pose_topic = ptz_pose_topic;
  node_options.location_topic = location_topic;
  node_options.frame_id = frame_id;

  node_options.horizontal_fov = horizontal_fov;
  node_options.vertical_fov = vertical_fov;
  node_options.near_plane_distance = near_plane_distance;
  node_options.far_plane_distance = far_plane_distance;

  LocationNode location_node(node_options);

  location_node.Initialize();
  location_node.ConnectToServer();
  location_node.RegisterMsg();
  // location_node.Initialize();

  ::ros::spin();

}

}
}

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  // CHECK(!FLAGS_configuration_directory.empty())
  //     << "-configuration_directory is missing.";
  // CHECK(!FLAGS_configuration_basename.empty())
  //     << "-configuration_basename is missing.";

  ::ros::init(argc, argv, "cartographer_node");
  ::ros::start();
  person_monitor::person_location::ScopedRosLogSink ros_log_sink;

person_monitor::person_location::Run();

  ::ros::shutdown();
}
