#pragma once
#include <string>
#include <vector>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/voxel_grid_occlusion_estimation.h>
#include"cam_param_reader.h"
using namespace std;
namespace person_monitor{

namespace person_location{
  
class RotateCamParamReader  : public CamParamReader
{ 


public:
  explicit RotateCamParamReader() ;

  virtual ~RotateCamParamReader();

  RotateCamParamReader(const RotateCamParamReader&) = delete;
  RotateCamParamReader& operator=(const RotateCamParamReader&) = delete;

virtual int SetRotations(const std::vector<double> &rads) override;

private:
  virtual int LoadExtrinsic(const std::string& calibration_string) override;

  std::vector<Eigen::Matrix4d> axis_mats_;
  Eigen::Matrix4d tool_mat_;


 };
} // namespace person_location
}// namespace person_monitor
