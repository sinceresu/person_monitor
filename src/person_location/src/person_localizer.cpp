#include "person_localizer.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include<pcl/io/pcd_io.h>

#include "cam_param_reader.h"
#include "rotate_cam_param_reader.h"

using namespace std;

namespace person_monitor{

namespace person_location{
namespace {
constexpr float kHorizontalFOV = 60.0f;
constexpr float kVerticalFOV = 60.0f;
constexpr float kNearPlaneDistance = 0.7f;
constexpr float kFarPlaneDistance = 15.0f;
constexpr int kMaxImagesInDistanceMap = 50;
constexpr float kOccludeDistanceTolerence = 0.02f;

}

PersonLocalizer::PersonLocalizer() :
        horizontal_fov_(kHorizontalFOV),
        vertical_fov_(kVerticalFOV),
        near_plane_distance_(kNearPlaneDistance),
        far_plane_distance_(kFarPlaneDistance)
{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

PersonLocalizer::~PersonLocalizer() {
}

int PersonLocalizer::SetCullingParamters( float horizontal_fov, 
                          float vertical_fov, 
                          float near_plane_distance, 
                          float far_plane_distance) {

    horizontal_fov_ = horizontal_fov;
    vertical_fov_ = vertical_fov;
    near_plane_distance_ = near_plane_distance;
    far_plane_distance_ = far_plane_distance;

    return 0;
}

int PersonLocalizer::SetRotations(const std::vector<double> rotations) {
  if (cam_param_reader_) {
    cam_param_reader_->SetRotations(rotations);

    std::lock_guard<std::mutex> lock(extrinsic_mutex_);
    camara_to_ref_pose_ =  cam_param_reader_->GetExtrinsic().cast<float>();
    extrinsic_need_update_ = true;
  }

  return 0;
}

void PersonLocalizer::UpdateExtrinsic() {
  Eigen::Matrix4f cam2robot;
  cam2robot << 0, 0, 1, 0,
            0,-1, 0, 0,
            1, 0, 0, 0,
            0, 0, 0, 1;
  std::lock_guard<std::mutex> lock(extrinsic_mutex_);
  camara_to_ref_for_culling_ = camara_to_ref_pose_ * cam2robot;
  ref_to_camera_pose_ = camara_to_ref_pose_.inverse();
  extrinsic_need_update_ = false;
}

int PersonLocalizer::LoadCalibrationString(const std::string& calibration_string, bool rotate) {
  cam_param_reader_ = rotate ? std::make_shared<RotateCamParamReader>() : std::make_shared<CamParamReader>();
   
	cam_param_reader_->LoadCalibrationString(calibration_string);

  intrinsic_mat_ = cam_param_reader_->GetIntrinsic();
  distortion_coeffs_ = cam_param_reader_->GetDistortion();
  image_size_ = cam_param_reader_->GetImageSize();

  camara_to_ref_pose_ =  cam_param_reader_->GetExtrinsic().cast<float>();

// Note: This assumes a coordinate system where X is forward, Y is up, and Z is right. To convert from the traditional camera coordinate system (X right, Y down, Z forward), one can use:
  UpdateExtrinsic();
  return 0;
 }
 

pcl::PointCloud<pcl::PointXYZRGB>::Ptr PersonLocalizer::CullingPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr point_cloud, const Eigen::Matrix4f& camara_pose) {

  pcl::FrustumCulling<pcl::PointXYZRGB> fc_;
  fc_.setInputCloud (point_cloud);
  fc_.setVerticalFOV (vertical_fov_);
  fc_.setHorizontalFOV (horizontal_fov_);
  fc_.setNearPlaneDistance (near_plane_distance_);
  fc_.setFarPlaneDistance (far_plane_distance_);
  fc_.setCameraPose (camara_pose);

  std::vector<int> filter_indices;
  fc_.filter (filter_indices); 


  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  inliers->indices = move(filter_indices);
  pcl::ExtractIndices<pcl::PointXYZRGB> extract;
  extract.setInputCloud (point_cloud);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*cloud_filtered);
  return cloud_filtered;
}

// static bool upsampling = false;

int PersonLocalizer::GenerateDistanceImage() {
  if (cloud_map_->empty()) {
    return - 1;
  }
  distance_image_ = cv::Mat(image_size_, CV_32F, numeric_limits<float>::max());
  point3d_image_ = cv::Mat(image_size_, CV_32FC3, cv::Vec3f{numeric_limits<float>::min(),  numeric_limits<float>::min(), numeric_limits<float>::min()});

  rgb_image_ = cv::Mat(image_size_, CV_8UC3, cv::Scalar(0, 0, 0));
  Eigen::Matrix4f world_to_camera_transform = ref_to_camera_pose_;

  cv::Mat world_to_camera;
  // rotatiion matrix convert world coordinate of a point  to camera coordinate 
  cv::eigen2cv(world_to_camera_transform, world_to_camera);

  Eigen::Matrix4f camera_to_world_for_culling = camara_to_ref_for_culling_;
  auto culled_pcl = CullingPointCloud(cloud_map_, camera_to_world_for_culling);
  if (culled_pcl->empty()) {
    return - 1;
  }
  // pcl::io::savePCDFile("cull.pcd", *culled_pcl);

  // if (upsampling) {
  //   auto upsamping_pcl = UpsamplingPointCloud(culled_pcl);
  //   culled_pcl = upsamping_pcl;
  // }
  std::vector<cv::Point3f> points_to_colorize;

  points_to_colorize.reserve(culled_pcl->size());
  for (size_t i = 0; i < culled_pcl->size(); ++i) {
      const auto& point = culled_pcl->at(i);
      points_to_colorize.push_back(cv::Point3f(point.x, point.y, point.z));
  }

  cv::Mat rotation_vec;
  cv::Rodrigues(world_to_camera(cv::Rect(0,0,3,3)), rotation_vec);
  rotation_vec_ = rotation_vec.t();
  transition_vec_ = world_to_camera(cv::Rect(3, 0, 1, 3)).t();
  std::vector<cv::Point2f> image_points;
  world_to_camera.convertTo(world_to_camera, CV_64F);

  cv::projectPoints(points_to_colorize, rotation_vec_, transition_vec_, intrinsic_mat_, distortion_coeffs_, image_points);
  std::vector<cv::Point3f> points_to_camera;
  cv::perspectiveTransform(points_to_colorize, points_to_camera, world_to_camera);
  for (size_t i = 0; i < image_points.size(); ++i) {
      float y = image_points[i].y, x = image_points[i].x;
        int col = round(x), row = round(y);
      if (row >= 0 && row < image_size_.height && col >= 0 && col < image_size_.width) {
          float distance = cv::norm(points_to_camera[i]);
          float current_distance = distance_image_.at<float>(row, col);
          if (distance< current_distance)
            distance_image_.at<float>(row, col) = distance;
            point3d_image_.at<cv::Point3f>(row, col) = points_to_colorize[i];
            
            // auto cloud_point =  culled_pcl->at(i);
            // rgb_image_.at<cv::Vec3b>(row, col)[0] = cloud_point.b;
            // rgb_image_.at<cv::Vec3b>(row, col)[1] = cloud_point.g;
            // rgb_image_.at<cv::Vec3b>(row, col)[2] = cloud_point.r;

      }
  }

  // cv::imwrite("output.png", rgb_image_);
  return 0;

}

int PersonLocalizer::SetCloudMap(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud_map) {
  cloud_map_ = pcl::PointCloud<pcl::PointXYZRGB>::Ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
  *cloud_map_ =  *cloud_map;
  return 0;
}

int PersonLocalizer::Localize( std::vector<cv::Point2f> poses, cv::Point3f& person_position) {
  assert(poses.size() == 2);
  int  result =0; 
  if (extrinsic_need_update_) {
    UpdateExtrinsic();
  }
  if (distance_image_.empty()) {
    result = GenerateDistanceImage();
    if (0 != result)
      return result;
  }
  int col = round((poses[0].x + poses[1].x)/ 2) , row = round((poses[0].y + poses[1].y) / 2) ;
  bool found = false;
  for (int y = row; y < image_size_.height; y++) {
    person_position = point3d_image_.at<cv::Point3f>(y, col) ;
    if (person_position.x >  -10000.f) {
      found = true;
      break;
    }
  }
  
  return found ? 0 : -1;
}


} // namespace person_location
}// namespace person_monitor
