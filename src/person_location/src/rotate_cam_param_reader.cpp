#include "rotate_cam_param_reader.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include<opencv2/core/eigen.hpp>

#include "glog/logging.h"
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/extract_indices.h>
#include<pcl/io/pcd_io.h>


using namespace std;

namespace person_monitor{

namespace person_location{


RotateCamParamReader::RotateCamParamReader() 
{
    // std::vector<uint32_t >image_ids = {1,2,3,4,5,6,7,8,9,10};
    // image_id_set = std::set<uint32_t>(image_ids.begin(), image_ids.end());
}

RotateCamParamReader::~RotateCamParamReader() {
    // std::cout << "image_ids" <<  std::endl; 
    // std::ofstream image_id_f("image_id.txt");
    // for (const auto image_id : image_id_set) {
    //     image_id_f << image_id << "," ; 
    // }

}


 int RotateCamParamReader::LoadExtrinsic(const std::string& calibration_string) {
	cv::FileStorage fs(calibration_string, cv::FileStorage::READ + cv::FileStorage::MEMORY);
  axis_mats_.clear();
  int num = fs["num"];
  for (int i = 0; i < num; ++i)
  {
    cv::Mat cv_RT;
    string index("Matrix_");
    index = index + to_string(i);
    fs[index] >> cv_RT;
    Eigen::Matrix4d eigen_RT;
    cv::cv2eigen(cv_RT, eigen_RT);
    // cout << eigen_RT << endl;
    axis_mats_.push_back(eigen_RT);
  }
  cv::Mat cv_RT;
  fs["Tool"] >> cv_RT;
  cv::cv2eigen(cv_RT, tool_mat_);
  return 0;

  std::vector<double> rads(axis_mats_.size(), 0.0);
  SetRotations(rads);
 }

 int RotateCamParamReader::SetRotations(const vector<double> &rads)
{
    assert(axis_mats_.size() == rads.size());
    int num = axis_mats_.size();
    extrinsic_mat_.setIdentity();
    Eigen::Matrix4d motionframes;
    for (int i = 0; i < num; i++)
    {
        motionframes = Eigen::Matrix4d::Identity();
        // motionframes.block<3, 3>(0, 0) = (Eigen::AngleAxisd(rads[i]* M_PI / 180.0, Eigen::Vector3d::UnitZ())).matrix();
        double rad = rads[i] / 100;
        rad = rad > 180 ? rad - 360 : rad;
        motionframes.block<3, 3>(0, 0) = (Eigen::AngleAxisd(rad * M_PI / 180.0, Eigen::Vector3d::UnitZ())).matrix();
        extrinsic_mat_ = extrinsic_mat_ * axis_mats_[i] * motionframes;
    }
    extrinsic_mat_ = extrinsic_mat_ * tool_mat_;

    return 0;
}

} // namespace person_location
}// namespace person_monitor