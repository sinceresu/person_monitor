#include "location_node.h"
#include <limits>

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PolygonStamped.h"

#include<pcl/io/pcd_io.h>
#include "glog/logging.h"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Matrix3x3.h"
#include "person_localizer.h"
#include "person_monitor_msgs/PersonPose.h"


namespace person_monitor {
namespace  person_location{

constexpr int kInfiniteSubscriberQueueSize = 0;
constexpr int kLatestOnlyPublisherQueueSize = 1;

namespace {
bool isPoseValid(std::vector<cv::Point2f> heel_points, std::vector<cv::Point2f> toe_points) {
  auto float_epsilon =  std::numeric_limits<float>::epsilon();
  for (const auto& point : heel_points) {
    if (point.x < float_epsilon || point.y < float_epsilon) {
      return false;
    }
  }
  for (const auto & point : toe_points) {
    if (point.x < float_epsilon || point.y < float_epsilon) {
      return false;
    }
  }
  return true;
}
}

LocationNode::LocationNode(const LocationNodeOptions& node_options) : node_options_(node_options)
 {

}

LocationNode::~LocationNode() { 

  ros_node_.unsubscribe(person_pose2d_subscriber_);
}

bool LocationNode::ConnectToServer() { 

  auto delay_msec = std::chrono::milliseconds(5000);
  auto start_time = std::chrono::system_clock::now();

  ros_node_.init_connect(node_options_.master_ip, "tracking_node");
  std::string status = ros_node_.get_status();
  while (status != "Open")
  {
    auto now_time = std::chrono::system_clock::now();
    if ((now_time - start_time) >= delay_msec)
    {
      start_time = std::chrono::system_clock::now();
      try
      {
        ros_node_.reconnection();
        status = ros_node_.get_status();
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }

      continue;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }

  thread connect_status(boost::bind(&LocationNode::ConnectCheck, this, 3000.0));
  connect_status.detach();

  return true;
}

bool LocationNode::RegisterMsg() {
  device_param_client_ = ros_node_.serviceClient(node_options_.client_ips, "/data_server/device_param", "person_monitor_msgs/DeviceParam");
  person_pose3d_publisher_ = ros_node_.advertise(node_options_.client_ips, node_options_.location_topic,"person_monitor_msgs/PersonPose");
  person_pose2d_subscriber_ = ros_node_.subscribe(node_options_.client_ips, node_options_.openpose_topic, "person_monitor_msgs/PoseKeypoints", std::bind(&LocationNode::OpenPoseCallback, this, 
                                          std::placeholders::_1, std::placeholders::_2), 1);
  ptz_pose_subscriber_ = ros_node_.subscribe(node_options_.client_ips, node_options_.ptz_pose_topic, "nav_msgs/Odometry", std::bind(&LocationNode::PtzPoseCallback, this, 
                                          std::placeholders::_1, std::placeholders::_2), 1);

  return true;
}

void LocationNode::ConnectCheck(double msec) {
  usleep(3000000);
  // std::time_t start_time = 0;
  std::time_t start_time = GetTimeStamp();
  while (1)
  {
    std::time_t now_time = GetTimeStamp();
    if ((now_time - start_time) >= msec)
    {
      start_time = GetTimeStamp();

      try
      {
        std::string status = ros_node_.get_status();
        if (status != "Open")
        {
          ros_node_.reconnection();
          RegisterMsg();
        }
        else
        {
          ros_node_.retry_register();
        }
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }
    }
    usleep(100000);
  }
}

bool LocationNode::Initialize() { 
  if (!GetCameraList())
    return false;

   if (!InitializeLocalizers())
    return false;
  
  return true;

}

bool LocationNode::InitializeLocalizers() {
  pose_localizers_.clear();
  // note: need initializaton of camera list from service server.
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_map= pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>() );
    
  pcl::io::loadPCDFile(node_options_.cloud_map_filepath, *cloud_map);

  // for (const auto & camera : camera_calib_params_ ) {
  //   auto pose_localizer = std::make_shared<PersonLocalizer>();
  //   if (camera.second.empty()) {
  //     return false;
  //   }
  //   if (pose_localizer->LoadCalibrationString(camera.second) != 0)
  //     return false;
    
  //   pose_localizer->SetCullingParamters(node_options_.horizontal_fov, 
  //     node_options_.vertical_fov, 
  //     node_options_.near_plane_distance, 
  //     node_options_.far_plane_distance
  //   );
  //   pose_localizer->SetCloudMap(cloud_map);

  //   pose_localizers_[camera.first] = pose_localizer;
  // }
  // 
  for (const auto & camera : camera_calib_datas_ ) {
    if (camera.second.calibration.empty()) {
      return false;
    }

    auto pose_localizer = std::make_shared<PersonLocalizer>();
    if (pose_localizer->LoadCalibrationString(camera.second.calibration, camera.second.rotate != 0) != 0)
      return false;
  
    pose_localizer->SetCullingParamters(node_options_.horizontal_fov, 
      node_options_.vertical_fov, 
      node_options_.near_plane_distance, 
      node_options_.far_plane_distance
    );
    pose_localizer->SetCloudMap(cloud_map);

    pose_localizers_[camera.first] = pose_localizer;
  

  }

  return true;

}

void LocationNode::StartLocation() {

}

void LocationNode::response_callback(int code, const char *response, int lenght, void *ud)
{
  if (code == 0)
  {
    string responseData = string(response, strlen(response));
    //
    Json::Reader reader;
    Json::Value root;
    bool bRet = reader.parse(responseData, root);
    if (bRet == false)
    {
      return;
    }
    int code = root["code"].asInt();
    if (code == 1)
    {
      string devices_str = root["data"].toStyledString();
      monitor_common::DeviceListParser device_parser(devices_str);
      if (!device_parser.ReadDeviceList(device_list)) {
        return;
      }

    }
  }
}

bool  LocationNode::GetCameraList() {
  // SRosDeviceParamServer device_param;
  // device_param.request.param = "device";
  // string msg = x2struct::X::tojson(device_param);
  // // string msg = x2struct::X::tojson(task, "", 4, ' ');
  // if (!device_param_client_.call(node_options_.client_ips, msg)) 
  //   return false;

  // x2struct::X::loadjson(msg, device_param, false);
  // if (device_param.response.result != 0)
  //   return false;

  // monitor_common::DeviceListParser::DeviceList device_list;
  // monitor_common::DeviceListParser device_parser(device_param.response.data);
  // if (!device_parser.ReadDeviceList(device_list)) {
  //   return false;
  // }

  // if (device_list.devices.empty())
  //   return false;
  // camera_calib_datas_.clear();

  // for (const auto device: device_list.devices) {
  //   if (device.type != 2)
  //   {
  //     continue;
  //   }
    
  //   PersonLocalizer::Calibration_t calib_;
  //   calib_.rotate = device.rotate;
  //   calib_.calibration = device.calibrationData;
  //   // camera_calib_datas_[device.cameras[0].id] = calib_;
  //   camera_calib_datas_[to_string(device.cameras[0].id)] = calib_;
  // }

  // http
  string stamp_str = to_string(GetTimeStamp());
  string temp_sign = "YDROBOT&&" + stamp_str;
  string sign_str = "";
  char md_c[MD5_DIGEST_LENGTH * 2 + 1];
  if (md5_encrypt(temp_sign.c_str(), md_c) == 0)
  {
    sign_str = md_c;
  }
  string url = node_options_.http_url + "/ui/robots?page=1&size=0&type=2&status=1";
  string header_str = "timestamp:" + stamp_str + "\r\n" + "sign:" + sign_str + "\r\n";
  CHttpClient http_client;
  http_client.get(url.c_str(), header_str.c_str(), "", std::bind(&LocationNode::response_callback, this,
                            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4), this);

  if (device_list.devices.empty())
    return false;
  camera_calib_datas_.clear();
  node_options_.client_ips = "";

  for (const auto device: device_list.devices) {
    if (device.type == 2 && device.status == 1) {
      PersonLocalizer::Calibration_t calib_;
      calib_.rotate = device.rotate;
      calib_.calibration = device.calibrationData;
      camera_calib_datas_[to_string(device.id)] = calib_;

      node_options_.client_ips += device.ipCore + ",";
    }
  }

  return true;
}

void LocationNode::PtzPoseCallback(string &ip, string &msg)
{
  RosOdometry ros_msg;
  x2struct::X::loadjson(msg, ros_msg, false);
  if (pose_localizers_.count(ros_msg.header.frame_id) == 0) {
    return;
  }
  auto& pose_localizer = pose_localizers_[ros_msg.header.frame_id];
  std::vector<double> rotations = {ros_msg.pose.pose.position.x, ros_msg.pose.pose.position.z};
  pose_localizer->SetRotations(rotations);
}

void LocationNode::OpenPoseCallback(string &ip, string &msg)
{
  RosPoseKeypoints ros_msg;
  x2struct::X::loadjson(msg, ros_msg, false);
  if (pose_localizers_.count(ros_msg.header.frame_id) == 0) {
    return;
  }
  auto& pose_localizer = pose_localizers_[ros_msg.header.frame_id];

  const auto& polygon =  ros_msg.polygon;
  if (polygon.points.size() != 6){
    LOG(INFO) << "polygon point size error , size is " << polygon.points.size() ;
    return;
  }
  // 消息内容：        points[0]　左大脚趾
  //                 points[1]　左小脚趾
  //                 points[2]　左脚后跟
  //                 points[3]　右大脚趾
  //                 points[4]　右小脚趾
  //                 points[5]　右脚后跟
  std::vector<cv::Point2f> heel_points(2);
  std::vector<cv::Point2f> toe_points(2);
  heel_points[0] = cv::Point2f{polygon.points[2].x, polygon.points[2].y};
  heel_points[1] = cv::Point2f{polygon.points[5].x, polygon.points[5].y};
  toe_points[0] = cv::Point2f{polygon.points[0].x, polygon.points[0].y};
  toe_points[1] = cv::Point2f{polygon.points[3].x, polygon.points[3].y};

  if (!isPoseValid(heel_points, toe_points))
    return;

  cv::Point3f heels_middle_position;
  if (0 != pose_localizer->Localize(heel_points, heels_middle_position) ) {
    return;
  }

  cv::Point3f toes_middle_position;
  if (0 != pose_localizer->Localize(toe_points, toes_middle_position) ) {
    return;
  }
  Eigen::Vector3f direction(toes_middle_position.x- heels_middle_position.x,
  toes_middle_position.y- heels_middle_position.y,
  toes_middle_position.z- heels_middle_position.z);
  double yaw = atan2(direction[1], direction[0]);

  tf2::Quaternion rotation;
  rotation.setRPY(0, 0, yaw);

  RosPersonPose person_pose;
  // person_pose.header.frame_id = node_options_.frame_id;
  person_pose.header.frame_id = ros_msg.header.frame_id;
  person_pose.header.stamp =   ros_msg.header.stamp;
  person_pose.cam_id =   ros_msg.cam_id;
  person_pose.person_id =   ros_msg.person_id;

  SRosPoint position;
  position.x = heels_middle_position.x;
  position.y = heels_middle_position.y;
  position.z = heels_middle_position.z;

  SRosQuaternion orientation;
  orientation.x = rotation.x();
  orientation.y = rotation.y();
  orientation.z = rotation.z();
  orientation.w = rotation.w();

  person_pose.pose.position = position;
  person_pose.pose.orientation = orientation;

  person_pose3d_publisher_.publish(ip,  x2struct::X::tojson(person_pose));


}


}

}
