#pragma once
#include <string>
#include <deque>
#include <set>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/voxel_grid_occlusion_estimation.h>

namespace person_monitor{

namespace person_location{
  
class CamParamReader 
{
public:
  CamParamReader() ;

  virtual ~CamParamReader();

  CamParamReader(const CamParamReader&) = delete;
  CamParamReader& operator=(const CamParamReader&) = delete;
  virtual int LoadCalibrationString(const std::string& calibration_string)  ;
  virtual int SetRotations(const std::vector<double> &rads);
  cv::Mat  GetIntrinsic( ) {
    return intrinsic_mat_;
  };
  cv::Mat  GetDistortion(){
    return distortion_coeffs_;
  };
  cv::Size  GetImageSize(){
    return image_size_;
  };

  Eigen::Matrix4d  GetExtrinsic(){
    return extrinsic_mat_;
  };
protected:
  // void InitializeOcclusionFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr occlude_pcl, const Eigen::Matrix4f& ref_to_world, int64_t image_id);
  cv::Mat intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  Eigen::Matrix4d extrinsic_mat_;

private:

  virtual int LoadIntrinsic(const std::string& calibration_string) ;
  virtual int LoadExtrinsic(const std::string& calibration_string);


};
} // namespace person_location
}// namespace person_monitor
