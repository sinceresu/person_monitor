#ifndef httpClient_HPP
#define httpClient_HPP

#pragma once
#include <string>
#include <functional>

#ifndef MG_ENABLE_CALLBACK_USERDATA
#define MG_ENABLE_CALLBACK_USERDATA 0
#endif

#if MG_ENABLE_CALLBACK_USERDATA
#define HC_UD_ARG(ud) , ud
#define HC_CB(cb, ud) cb, ud
#else
#define HC_UD_ARG(ud)
#define HC_CB(cb, ud) cb
#endif

// typedef void (*DataCallback)(int code, const char *response, int lenght, void *ud);
typedef std::function<void(int code, const char *response, int lenght, void *ud)> DataCallback;

class CHttpClient
{
public:
    CHttpClient();
    ~CHttpClient();

private:
    static int exit_flag;
    static void *u_data;
    static DataCallback resultCallback;

    // #if MG_ENABLE_CALLBACK_USERDATA
    //     static void ev_handler(struct mg_connection *c, int ev, void *ev_data, void *user_data);
    // #else
    //     static void ev_handler(struct mg_connection *c, int ev, void *ev_data);
    // #endif
    static void ev_handler(struct mg_connection *c, int ev, void *ev_data HC_UD_ARG(void *user_data));
    void request(const char *type, const char *url, const char *headers, const char *data, DataCallback callback, void *ud = nullptr);
    // #if MG_ENABLE_CALLBACK_USERDATA
    //     static void response(int code, const char *response, int lenght, void *user_data);
    // #else
    //     static void response(int code, const char *response, int lenght);
    // #endif
    static void response(int code, const char *response, int lenght HC_UD_ARG(void *user_data));

public:
    void get(const char *url, const char *headers, const char *getData, DataCallback callback, void *ud = nullptr);
    void post(const char *url, const char *headers, const char *postData, DataCallback callback, void *ud = nullptr);
    void put(const char *url, const char *headers, const char *putData, DataCallback callback, void *ud = nullptr);
};

#endif