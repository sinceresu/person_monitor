#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include<pcl/io/pcd_io.h>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "src/person_localizer.h"


DEFINE_string(calibration_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");

DEFINE_string(cloud_map_filepath, "",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;

namespace person_monitor {
namespace  person_location{
namespace {
// 字符串分割函数
vector<string> split(string str, string pattern) {
    string::size_type pos;
    vector<string> result;

    // 拓展字符串，方便操作
    str += pattern;
    int size = str.size();

    for (int i = 0; i < size; i++) {
        pos = str.find(pattern, i);
        if (pos < size) {
            string s = str.substr(i, pos - i);
            result.push_back(s);
            i = pos + pattern.size() - 1;
        }
    }

    return result;
}
std::vector<std::tuple<std::string, cv::Point2f>> readPointsFromFile(const std::string data_file) {
  std::ifstream fs(data_file);
  std::string img_file_name;
  std::string coordinate;
  std::vector<std::tuple<std::string, cv::Point2f>> output;
  while (fs >> img_file_name >> coordinate) {
    auto coordinate_value = split(coordinate, ",");
    assert(coordinate_value.size() == 2);
    float x = std::stof(coordinate_value[0]);
    float y = std::stof(coordinate_value[1]);
    output.push_back(std::make_tuple(img_file_name, cv::Point2f(x, y)));
  }
  return output;
}
std::shared_ptr<PersonLocalizer> BuildPoseLocalizer( 
                    const std::string& calibration_filepath) {


auto  localizer = std::make_shared<PersonLocalizer>();
// localizer->LoadCalibrationFile(calibration_filepath);
  return localizer;
}

void Run( ) {
  auto points = readPointsFromFile("/home/sujin/slam_dataset/2021-02-26/POS.txt");
  auto localizer = BuildPoseLocalizer(FLAGS_calibration_filepath);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_point_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>() );

    pcl::io::loadPCDFile(FLAGS_cloud_map_filepath, *input_point_cloud);

    localizer->SetCloudMap(input_point_cloud);

    cv::Point2f ankel_pos2d_1;
    ankel_pos2d_1.x = 1175;
    ankel_pos2d_1.y = 792;

    cv::Point2f ankel_pos2d_2;
    ankel_pos2d_2.x = 1175;
    ankel_pos2d_2.y = 792;
    std::vector<cv::Point2f> poses {ankel_pos2d_1, ankel_pos2d_2};


    cv::Point3f person_position;

    localizer->Localize(poses,  person_position);

    LOG(INFO) << person_position.x << ","   << person_position.y << "," <<  person_position.z;
    std::ofstream output_fs("position_3d.txt");
    for (const auto &point : points) {
      std::vector<cv::Point2f> poses {std::get<1>(point), std::get<1>(point)};
      cv::Point3f person_position;

      localizer->Localize(poses,  person_position);
      
      output_fs << std::get<0>(point) << " " <<  person_position.x << ","   << person_position.y << "," <<  person_position.z << std::endl;

    }

}
void Test(int argc, char** argv) {

    Run();

}

}
}
}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  CHECK(!FLAGS_calibration_filepath.empty())
      << "-configuration_directory is missing.";

  CHECK(!FLAGS_cloud_map_filepath.empty())
      << "-map_configuration_basename is missing.";


  person_monitor::person_location::Test(argc, argv);

}
