cmake_minimum_required(VERSION 2.8.3)
project(data_server)

# Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)
add_compile_options(-std=c++11)

# ##############################################################################
# Build ##
# ##############################################################################

set(CMAKE_BUILD_TYPE DEBUG)

# Specify additional locations of header files Your package locations should be
# listed before other locations
include_directories(# include
                    src/include)

# Declare a C++ library add_library(${PROJECT_NAME}
# src/${PROJECT_NAME}/path_planning.cpp )

# 添加子目录
add_subdirectory(src/logger)
add_subdirectory(src/common)
add_subdirectory(src/httpComm)
add_subdirectory(src/fileParser)
add_subdirectory(src/databaseOperation)
add_subdirectory(src/rosWebsocket)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

set(CMAKE_DEBUG_POSTFIX "_d")
set(CMAKE_RELEASE_POSTFIX "_r")

add_executable(
  ${PROJECT_NAME}
  src/data_server.cpp
  src/util_datas.hpp
  src/util_datas.cpp
  src/rosWebBasics.hpp
  src/rosWebBasics.cpp
  src/data_param.hpp
  src/data_param.cpp)
target_link_libraries(
  ${PROJECT_NAME}
  pthread
  boost_system
  logger
  common
  httpComm
  fileParser
  databaseOperation
  rosWebsocket)
set_target_properties(${PROJECT_NAME} PROPERTIES DEBUG_POSTFIX "_d")
set_target_properties(${PROJECT_NAME} PROPERTIES RELEASE_POSTFIX "_r")

file(COPY ${PROJECT_SOURCE_DIR}/config DESTINATION ${PROJECT_SOURCE_DIR}/bin)
