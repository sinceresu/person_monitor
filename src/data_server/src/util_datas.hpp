#ifndef util_datas_HPP
#define util_datas_HPP

#pragma once
#include <string>

class CUtilDatas
{
public:
    CUtilDatas();
    ~CUtilDatas();

public:
    static std::string pack_path;
};

#endif