#include "INIParser.hpp"

/**
 * Example
 * 
 * CINIParser *p = new CINIParser();
 * p->ReadINI("/home/dingjia/catkin_ws/src/path_planning/config/config.ini");
 * std::cout << p->GetValue("coordinate", "home") << std::endl;
 * 
 * p->SetValue("aaa", "bbb", "111");
 * p->WriteINI("/home/dingjia/catkin_ws/src/path_planning/config/config.ini");
 * p->Travel();
*/

CINIParser::CINIParser()
    : ini_file("")
{
}

CINIParser::~CINIParser()
{
    map_ini.clear();
}

/**
 * Description:      去除空格
 * Function:         TrimString
 * Parameters:       str:输入的字符串
 * Return:           std::string &:结果字符串
 * Author:
 * Date:
*/
std::string &CINIParser::TrimString(std::string &str)
{
    std::string::size_type pos = 0;
    while (str.npos != (pos = str.find(" ")))
        str = str.replace(pos, pos + 1, "");

    return str;
}

/**
 * Description:      读取INI文件，并将其保存到map结构中
 * Function:         ReadINI
 * Parameters:       path:INI文件的路径
 * Return:           int
 * Author:
 * Date:
*/
int CINIParser::ReadINI(std::string path)
{
    int nRet = 0;

    std::ifstream in_conf_file(path.c_str());
    if (!in_conf_file)
    {
        nRet = -1;
        return -1;
    }

    std::string str_line = "";
    std::string str_root = "";
    std::vector<ININode> vec_ini;
    while (getline(in_conf_file, str_line))
    {
        std::string::size_type left_pos = 0;
        std::string::size_type right_pos = 0;
        std::string::size_type equal_div_pos = 0;
        std::string str_key = "";
        std::string str_value = "";
        if ((str_line.npos != (left_pos = str_line.find("["))) && (str_line.npos != (right_pos = str_line.find("]"))))
        {
            str_root = str_line.substr(left_pos + 1, right_pos - 1);
#ifdef INIDEBUG
            std::cout << str_root << std::endl;
#endif
        }

        if (str_line.npos != (equal_div_pos = str_line.find("=")))
        {
            str_key = str_line.substr(0, equal_div_pos);
            str_value = str_line.substr(equal_div_pos + 1, str_line.size() - 1);
            str_key = TrimString(str_key);
            str_value = TrimString(str_value);
#ifdef INIDEBUG
            std::cout << str_key << "=" << str_value << std::endl;
#endif
        }

        if ((!str_root.empty()) && (!str_key.empty()) && (!str_value.empty()))
        {
            ININode ini_node(str_root, str_key, str_value);
            vec_ini.push_back(ini_node);
#ifdef INIDEBUG
            std::cout << vec_ini.size() << std::endl;
#endif
        }
    }
    in_conf_file.close();
    in_conf_file.clear();

    // vector convert to map
    std::map<std::string, std::string> map_tmp;
    for (std::vector<ININode>::iterator itr = vec_ini.begin(); itr != vec_ini.end(); ++itr)
    {
        map_tmp.insert(std::pair<std::string, std::string>(itr->root, ""));
    }

    // 提取出根节点
    for (std::map<std::string, std::string>::iterator itr = map_tmp.begin(); itr != map_tmp.end(); ++itr)
    {
#ifdef INIDEBUG
        std::cout << "根节点：" << itr->first << std::endl;
#endif
        SubNode sn;
        for (std::vector<ININode>::iterator sub_itr = vec_ini.begin(); sub_itr != vec_ini.end(); ++sub_itr)
        {
            if (sub_itr->root == itr->first)
            {
#ifdef INIDEBUG
                std::cout << "键值对：" << sub_itr->key << "=" << sub_itr->value << std::endl;
#endif
                sn.InsertElement(sub_itr->key, sub_itr->value);
            }
        }
        map_ini.insert(std::pair<std::string, SubNode>(itr->first, sn));
    }

    return nRet;
}

/**
 * Description:      根据给出的根结点和键值查找配置项的值
 * Function:         GetValue
 * Parameters:       root:配置项的根结点
 * Parameters:       key:配置项的键,
 * Parameters:       defaultValue:默认值
 * Return:           std::string:配置项的值
 * Author:
 * Date:
*/
std::string CINIParser::GetValue(std::string root, std::string key, std::string defaultValue)
{
    std::map<std::string, SubNode>::iterator itr = map_ini.find(root);
    if (itr == map_ini.end())
    {
        return defaultValue;
    }

    std::map<std::string, std::string>::iterator sub_itr = itr->second.sub_node.find(key);
    if (sub_itr == itr->second.sub_node.end())
    {
        return defaultValue;
    }

    if (!(sub_itr->second).empty())
        return sub_itr->second;

    return "";
}

/**
 * Description:      设置配置项的值
 * Function:         SetValue
 * Parameters:       root:配置项的根节点
 * Parameters:       key:配置项的键,
 * Parameters:       value:配置项的值
 * Return:           std::vector<ININode>::size_type 数据长度
 * Author:
 * Date:
*/
std::vector<ININode>::size_type CINIParser::SetValue(std::string root, std::string key, std::string value)
{
    std::map<std::string, SubNode>::iterator itr = map_ini.find(root); // 查找
    if (map_ini.end() != itr)                                          // 根节点已经存在了，更新值
    {
        // itr->second.sub_node.insert(pair<string, string>(key, value));
        itr->second.sub_node[key] = value;
    }
    else // 根节点不存在，添加值
    {
        SubNode sn;
        sn.InsertElement(key, value);
        map_ini.insert(std::pair<std::string, SubNode>(root, sn));
    }

    return map_ini.size();
}

/**
 * Description:      遍历打印INI文件
 * Function:         Travel
 * Parameters:
 * Return:           void
 * Author:
 * Date:
*/
void CINIParser::Travel()
{
    for (std::map<std::string, SubNode>::iterator itr = this->map_ini.begin(); itr != this->map_ini.end(); ++itr)
    {
        //　root
        std::cout << "[" << itr->first << "]" << std::endl;
        for (std::map<std::string, std::string>::iterator itr1 = itr->second.sub_node.begin(); itr1 != itr->second.sub_node.end();
             ++itr1)
        {
            std::cout << itr1->first << " = " << itr1->second << std::endl;
        }
    }
}

/**
 * Description:      保存XML的信息到文件中
 * Function:         WriteINI
 * Parameters:       path:INI文件的保存路径
 * Return:           int
 * Author:
 * Date:
*/
int CINIParser::WriteINI(std::string path)
{
    std::ofstream out_conf_file(path.c_str());
    if (!out_conf_file)
        return -1;

#ifdef INIDEBUG
    std::cout << map_ini.size() << std::endl;
#endif
    for (std::map<std::string, SubNode>::iterator itr = map_ini.begin(); itr != map_ini.end(); ++itr)
    {
#ifdef INIDEBUG
        std::cout << itr->first << std::endl;
#endif
        out_conf_file << "[" << itr->first << "]" << std::endl;
        for (std::map<std::string, std::string>::iterator sub_itr = itr->second.sub_node.begin(); sub_itr != itr->second.sub_node.end(); ++sub_itr)
        {
            out_conf_file << sub_itr->first << "=" << sub_itr->second << std::endl;
#ifdef INIDEBUG
            std::cout << sub_itr->first << "=" << sub_itr->second << std::endl;
#endif
        }
    }
    out_conf_file.close();
    out_conf_file.clear();

    return 1;
}

/**
 * Description:      加载文件
 * Function:         LoadFIle
 * Parameters:       file:配置项的根节点
 * Return:           void
 * Author:
 * Date:
*/
void CINIParser::LoadFIle(std::string file)
{
    ini_file = file;

    Clear();

    ReadINI(ini_file);
}

/**
 * Description:      获取
 * Function:         GetData
 * Parameters:       root:配置项的根节点
 * Parameters:       key:配置项的键
 * Parameters:       defaultValue:默认值
 * Return:           std::string:返回值
 * Author:
 * Date:
*/
std::string CINIParser::GetData(std::string root, std::string key, std::string defaultValue)
{
    std::string value = GetValue(root, key, defaultValue);

    return value;
}

/**
 * Description:      设置
 * Function:         SetData
 * Parameters:       root:配置项的根节点
 * Parameters:       key:配置项的键
 * Parameters:       value:配置项的值
 * Return:           std::string:返回值
 * Author:
 * Date:
*/
void CINIParser::SetData(std::string root, std::string key, std::string value)
{
    SetValue(root, key, value);
    WriteINI(ini_file);
}