#ifndef Global_HPP
#define Global_HPP

#pragma once
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

bool CreateDirs(const std::string &dirPath);

template <typename _ForwardIterator, typename _Tp>
std::vector<_Tp> vector_remove_ptr(std::vector<_Tp> &__vector, _ForwardIterator __first, _ForwardIterator __last, const _Tp &__value)
{
    __first = __vector.begin();
    while (__first != __vector.end() && __vector.size() > 0)
    {
        if ((*(*__first) == *__value))
        {
            __first = __vector.erase(__first);
        }
        else
        {
            ++__first;
        }
    }

    return __vector;
}

#endif