#include "Characters.hpp"

/**
 * Description:      去除空格
 * Function:         TrimString
 * Parameters:       src:输入的字符串
 * Return:           std::string &:结果字符串
 * Author:
 * Date:
*/
std::string &TrimString(std::string &src)
{
    std::string::size_type pos = 0;
    while (src.npos != (pos = src.find(" ")))
        src = src.replace(pos, pos + 1, "");

    return src;
}

/**
 * Description:      替换字符
 * Function:         ReplaceString
 * Parameters:       strSource:原数据
 * Parameters:       strOld:被替换数据
 * Parameters:       strNew:替换数据
 * Return:           std::string &:结果字符串
 * Author:
 * Date:
*/
std::string &ReplaceString(std::string &strSource, const std::string &strOld, const std::string &strNew)
{
    std::string::size_type nPos = 0;
    while ((nPos = strSource.find(strOld, nPos)) != strSource.npos)
    {
        strSource.replace(nPos, strOld.length(), strNew);
        nPos += strNew.length();
    }

    return strSource;
}

/**
 * Description:      按分隔符分割字符串
 * Function:         SplitString
 * Parameters:       src:要分割的字符串
 * Parameters:       separator:分隔符的字符
 * Parameters:       dest:存放分割后的字符串的vector向量
 * Return:           std::vector<std::string> &:结果
 * Author:
 * Date:
*/
std::vector<std::string> &SplitString(const std::string &src, const std::string &separator, std::vector<std::string> &dest)
{
    std::string str = src;
    std::string substring;
    std::string::size_type start = 0, index;
    dest.clear();
    index = str.find_first_of(separator, start);
    do
    {
        if (index != std::string::npos)
        {
            substring = str.substr(start, index - start);
            dest.push_back(substring);
            start = index + separator.size();
            index = str.find(separator, start);
            if (start == std::string::npos)
                break;
        }
    } while (index != std::string::npos);

    substring = str.substr(start);
    dest.push_back(substring);

    return dest;
}