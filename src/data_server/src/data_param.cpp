#include "data_param.hpp"
#include "common/Common.hpp"
#include "util_datas.hpp"

CDataParma::CDataParma()
{
    CINIParser m_ini;
    m_ini.LoadFIle(CUtilDatas::pack_path + "/config/config.ini");
    http_url = m_ini.GetData("http", "url", "http://192.168.1.110:8080/ydrobot-station-gds-api");
}

CDataParma::~CDataParma()
{
}

int CDataParma::req_header(std::string &str_header)
{
    str_header = "";
    //
    time_t stamp = GetTimeStamp();
    string strStamp = to_string(stamp);
    //
    string temp_sign = "YDROBOT&&" + strStamp;
    //
    char md_c[MD5_DIGEST_LENGTH * 2 + 1];
    if (0 != CHttpClient::md5_encrypt(temp_sign.c_str(), md_c))
    {
        str_header = "";
        return -1;
    }
    string strSign = md_c;
    //
    str_header = "timestamp:" + strStamp + "\r\n" + "sign:" + strSign + "\r\n";

    return 0;
}

int CDataParma::loadRobotIps(vector<string> &ips, string &ips_str)
{
    int nRet = 0;

    ips_str = "";

    CDatabaseBasics::mtx.lock();
    // pthread_mutex_lock(&CDatabaseBasics::pt_mutex_t);

    transaction m_t(CDatabaseBasics::db->begin());
    try
    {
        robot_result robot_Ret = CDatabaseBasics::db->query<robot>();
        for (robot_result::iterator r_it = robot_Ret.begin(); r_it != robot_Ret.end(); r_it++)
        {
            ips_str += r_it->ip_core + ",";
            ips.push_back(r_it->ip_core);
        }
    }
    catch (const std::exception &e)
    {
        nRet = -1;
        std::cerr << e.what() << '\n';
    }
    m_t.commit();

    CDatabaseBasics::mtx.unlock();
    // pthread_mutex_unlock(&CDatabaseBasics::pt_mutex_t);

    return nRet;
}

device_datas_t CDataParma::get_device_parma()
{
    device_datas_t deviceDatas;

    CDatabaseBasics::mtx.lock();
    // pthread_mutex_lock(&CDatabaseBasics::pt_mutex_t);

    transaction m_t(CDatabaseBasics::db->begin());
    try
    {
        robot_result robot_Ret = CDatabaseBasics::db->query<robot>();
        if (!robot_Ret.empty() && robot_Ret.size() > 0)
        {
            for (robot_result::iterator r_it = robot_Ret.begin(); r_it != robot_Ret.end(); r_it++)
            {
                device_t device;
                device.device_id = to_string(r_it->id());
                device.type = r_it->type;
                device.rotate = r_it->rotate;
                device.master_ip = r_it->ip_core;
                device.calibration = r_it->calibration_data;
                camera_result camera_Ret = CDatabaseBasics::db->query<camera>(camera_query::robot_id == r_it->id());
                if (!camera_Ret.empty() && camera_Ret.size() > 0)
                {
                    for (camera_result::iterator c_it = camera_Ret.begin(); c_it != camera_Ret.end(); c_it++)
                    {
                        camera_t camera;
                        camera.camera_id = c_it->id();
                        camera.type = c_it->type;
                        camera.stream_url = c_it->stream_url;
                        camera.sampling_interval = c_it->sampling_interval;
                        camera.sampling_num = c_it->sampling_num;
                        camera.default_angle = c_it->default_angle;
                        camera.status = c_it->status;
                        //
                        // if (c_it->type == 1)
                        // {
                        //     device.infrared = camera;
                        // }
                        // if (c_it->type == 2)
                        // {
                        //     device.visible = camera;
                        // }
                        // if (c_it->type == 3)
                        // {
                        //     device.lidar = camera;
                        // }
                        device.cameras.push_back(camera);
                    }
                }
                deviceDatas.devices.push_back(device);
            }
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
    m_t.commit();

    CDatabaseBasics::mtx.unlock();
    // pthread_mutex_unlock(&CDatabaseBasics::pt_mutex_t);

    return deviceDatas;
}

area_datas_t CDataParma::get_fence_area()
{
    area_datas_t areaData;

    CDatabaseBasics::mtx.lock();
    // pthread_mutex_lock(&CDatabaseBasics::pt_mutex_t);

    transaction m_t(CDatabaseBasics::db->begin());
    try
    {
        area_result area_Ret = CDatabaseBasics::db->query<area>();
        if (!area_Ret.empty() && area_Ret.size() > 0)
        {
            for (area_result::iterator a_it = area_Ret.begin(); a_it != area_Ret.end(); a_it++)
            {
                area_t tarea;
                tarea.id = a_it->id();
                tarea.name = a_it->name;
                tarea.type = a_it->type;
                tarea.range = a_it->area_range;
                areaData.areas.push_back(tarea);
            }
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
    m_t.commit();

    CDatabaseBasics::mtx.unlock();
    // pthread_mutex_unlock(&CDatabaseBasics::pt_mutex_t);

    return areaData;
}
