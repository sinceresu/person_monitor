#ifndef rosWebBasics_HPP
#define rosWebBasics_HPP

#pragma once
#include <thread>
#include <string>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include "common/Common.hpp"
#include "rosWebsocket/rosNode.hpp"
#include "rosWebsocket/tfListener.hpp"
#include "data_param.hpp"

using namespace std;

class CRosWebBasics
{
public:
    CRosWebBasics();
    CRosWebBasics(string _url, void *p);
    ~CRosWebBasics();

private:
    string url;

    CDataParma dataParma;

public:
    CRosNode ros_node;
    // CRosNode ros_node("ws://127.0.0.1:9090", "data_server");

public:
    // 初始化
    void ros_init(bool bFirst = true);

    void timing_monitoring();

    void connect_check_fun(double msec);

    static void *test_fun(void *arg);

public:
    // server
    CRosServer device_param_server;
    bool device_param_Callback(string &ip, string &req, string &res);
    // // client
    // CRosClient device_param_client;
    // bool call_server(string ip, string data);
    // // pub
    // CRosPublisher publisher;
    // void publisher_function(string ip, string data);
    // // sub
    // CRosSubscriber subscriber;
    // void subscriber_Callback(string &ip, string &msg);
};

#endif