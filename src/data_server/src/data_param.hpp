#ifndef device_param_HPP
#define device_param_HPP

#pragma once
#include "databaseOperation/databaseBasics.hpp"
#include "httpComm/httpClient.hpp"
#include "fileParser/INIParser.hpp"
#include "x2struct/x2struct.hpp"

typedef struct _camera_t
{
    int camera_id;
    int type;
    std::string stream_url;
    int sampling_interval;
    int sampling_num;
    string default_angle;
    int status;
    XTOSTRUCT(O(camera_id, type, stream_url, sampling_interval, sampling_num, default_angle, status));
} camera_t;

typedef struct _device_t
{
    std::string device_id;
    int type;
    int rotate;
    std::string master_ip;
    std::string calibration;
    // camera_t visible;
    // camera_t infrared;
    // camera_t lidar;
    std::vector<camera_t> cameras;
    // XTOSTRUCT(O(cam_id, type, master_ip, calibration, visible, infrared, lidar, cameras));
    XTOSTRUCT(O(device_id, type, rotate, master_ip, calibration, cameras));
} device_t;

typedef struct _device_datas_t
{
    std::vector<device_t> devices;
    XTOSTRUCT(O(devices));
} device_datas_t;

typedef struct _area_t
{
    int id;
    std::string name;
    int type;
    std::string range;
    XTOSTRUCT(O(id, name, type, range));
} area_t;

typedef struct _area_datas_t
{
    std::vector<area_t> areas;
    XTOSTRUCT(O(areas));
} area_datas_t;

class CDataParma
{
public:
    CDataParma();
    ~CDataParma();

private:
    std::string http_url;
    int req_header(std::string &str_header);

public:
    int loadRobotIps(vector<string> &ips, string &ips_str);

    device_datas_t get_device_parma();
    area_datas_t get_fence_area();
};

#endif
