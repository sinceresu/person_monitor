#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class watch_point
{
public:
    watch_point(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string name;
    int stop_point_id;
    float default_value;
    float x;
    float y;
    float z;
    unsigned char calc_on_off;
    float f_h_angle;
    float f_p_angle;
    float f_mult;
    float b_h_angle;
    float b_p_angle;
    float f_preset_no;
    float b_mult;
    float b_preset_no;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
