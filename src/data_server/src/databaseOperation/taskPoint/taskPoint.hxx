#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class task_point
{
public:
    task_point(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int task_id;
    int point_id;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
