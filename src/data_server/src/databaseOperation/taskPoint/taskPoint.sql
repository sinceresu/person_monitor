/* This file was generated by ODB, object-relational mapping (ORM)
 * compiler for C++.
 */

DROP TABLE IF EXISTS `task_point`;

CREATE TABLE `task_point` (
  `task_id` INT NOT NULL,
  `point_id` INT NOT NULL,
  `id` BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT)
 ENGINE=InnoDB;

