#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class robot_param
{
public:
    robot_param(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int robot_id;
    unsigned char type;
    string name;
    string display_name;
    string value;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
