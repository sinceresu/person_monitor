#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class route
{
public:
    route(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string entity;
    string layer;
    int color;
    string line_type;
    float elevation;
    float line_wt;
    string ref_name;
    float line_wt1;
    string standard;
    int start;
    int end;
    string camera_pose;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
