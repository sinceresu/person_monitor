#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class watch_pos
{
public:
    watch_pos(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    float x;
    float y;
    float z;
    unsigned char angle_pitch;
    unsigned char angle_plane;
    unsigned char zoom;
    int is_allow_turn;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
