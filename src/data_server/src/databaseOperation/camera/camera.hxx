#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class camera
{
public:
    camera(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    unsigned char type;
    int robot_id;
    string stream_url;
    int sampling_interval;
    int sampling_num;
    string default_angle;
    int status;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
