/* This file was generated by ODB, object-relational mapping (ORM)
 * compiler for C++.
 */

DROP TABLE IF EXISTS `camera`;

CREATE TABLE `camera` (
  `type` TINYINT UNSIGNED NOT NULL,
  `robot_id` INT NOT NULL,
  `stream_url` TEXT NOT NULL,
  `sampling_interval` INT NOT NULL,
  `sampling_num` INT NOT NULL,
  `default_angle` TEXT NOT NULL,
  `status` INT NOT NULL,
  `id` BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT)
 ENGINE=InnoDB;

