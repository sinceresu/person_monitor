#ifndef DB_MYSQL_H
#define DB_MYSQL_H

#pragma once
#include <iostream>
#include <string.h>
#include <iomanip>
#include <mysql/mysql.h>

using namespace std;

class CDBMySql
{
public:
    CDBMySql();
    ~CDBMySql();

private:
    MYSQL m_mySql;
    MYSQL_RES *m_res;
    MYSQL_ROW m_row;
    MYSQL_FIELD *m_field;

public:
    bool init();
    bool connect(const char *host,
                 const char *user,
                 const char *passwd,
                 const char *db,
                 unsigned int port,
                 const char *unix_socket,
                 unsigned long clientflag);
    void disconnect();
    int setCharacter(const char *csname);
    int selectQuery(const char *szSql);
    int insertQuery(const char *szSql);
};

#endif