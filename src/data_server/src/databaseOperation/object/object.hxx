#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class object
{
public:
    object(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string name;
    string display_name;
    float x;
    float y;
    float z;
    int device_id;
    int object_type_id;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
