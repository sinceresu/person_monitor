#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class robot
{
public:
    robot(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string name;
    int type;
    int rotate;
    int device_id;
    string ip_core;
    string calibration_data;
    string position;
    string angle;
    string create_time;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
