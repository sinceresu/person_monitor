#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class task_history
{
public:
    task_history(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int robot_id;
    int task_id;
    string task_start_time;
    string task_end_time;
    unsigned char task_status;
    int temp;
    int hum;
    int wind;
    int weather;
    int check_status;
    string check_desc;
    string check_time;
    int operater_id;
    int total_run_time;
    int cumulative_run_time;
    string restart_start_time;
    string create_time;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
