/* This file was generated by ODB, object-relational mapping (ORM)
 * compiler for C++.
 */

DROP TABLE IF EXISTS `point_visible_light`;

CREATE TABLE `point_visible_light` (
  `point_id` INT NOT NULL,
  `height` FLOAT NOT NULL,
  `width` FLOAT NOT NULL,
  `id` BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT)
 ENGINE=InnoDB;

