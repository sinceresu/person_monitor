#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class point_visible_light
{
public:
    point_visible_light(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int point_id;
    float height;
    float width;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
