#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class point
{
public:
    point(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string name;
    string display_name;
    int object_id;
    int recon_type_id;
    int meter_type_id;
    int face_type_id;
    int hot_type_id;
    int save_type_id;
    unsigned char nis;
    unsigned char is_alarm;
    string unit;
    int coef;
    unsigned char is_use;
    unsigned char is_show;
    unsigned char is_save;
    float default_value;
    float x;
    float y;
    float z;
    unsigned char is_delete;
    unsigned char max_alarm_level;
    int recon_model_sub_id;
    unsigned char is_abnormal;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
