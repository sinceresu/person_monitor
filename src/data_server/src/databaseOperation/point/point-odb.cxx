// This file was generated by ODB, object-relational mapping (ORM)
// compiler for C++.
//

#include <odb/pre.hxx>

#include "point-odb.hxx"

#include <cassert>
#include <cstring>  // std::memcpy


#include <odb/mysql/traits.hxx>
#include <odb/mysql/database.hxx>
#include <odb/mysql/transaction.hxx>
#include <odb/mysql/connection.hxx>
#include <odb/mysql/statement.hxx>
#include <odb/mysql/statement-cache.hxx>
#include <odb/mysql/simple-object-statements.hxx>
#include <odb/mysql/container-statements.hxx>
#include <odb/mysql/exceptions.hxx>
#include <odb/mysql/simple-object-result.hxx>
#include <odb/mysql/enum.hxx>

namespace odb
{
  // point
  //

  struct access::object_traits_impl< ::point, id_mysql >::extra_statement_cache_type
  {
    extra_statement_cache_type (
      mysql::connection&,
      image_type&,
      id_image_type&,
      mysql::binding&,
      mysql::binding&)
    {
    }
  };

  access::object_traits_impl< ::point, id_mysql >::id_type
  access::object_traits_impl< ::point, id_mysql >::
  id (const id_image_type& i)
  {
    mysql::database* db (0);
    ODB_POTENTIALLY_UNUSED (db);

    id_type id;
    {
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_value (
        id,
        i.id_value,
        i.id_null);
    }

    return id;
  }

  access::object_traits_impl< ::point, id_mysql >::id_type
  access::object_traits_impl< ::point, id_mysql >::
  id (const image_type& i)
  {
    mysql::database* db (0);
    ODB_POTENTIALLY_UNUSED (db);

    id_type id;
    {
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_value (
        id,
        i._id_value,
        i._id_null);
    }

    return id;
  }

  bool access::object_traits_impl< ::point, id_mysql >::
  grow (image_type& i,
        my_bool* t)
  {
    ODB_POTENTIALLY_UNUSED (i);
    ODB_POTENTIALLY_UNUSED (t);

    bool grew (false);

    // name
    //
    if (t[0UL])
    {
      i.name_value.capacity (i.name_size);
      grew = true;
    }

    // display_name
    //
    if (t[1UL])
    {
      i.display_name_value.capacity (i.display_name_size);
      grew = true;
    }

    // object_id
    //
    t[2UL] = 0;

    // recon_type_id
    //
    t[3UL] = 0;

    // meter_type_id
    //
    t[4UL] = 0;

    // face_type_id
    //
    t[5UL] = 0;

    // hot_type_id
    //
    t[6UL] = 0;

    // save_type_id
    //
    t[7UL] = 0;

    // nis
    //
    t[8UL] = 0;

    // is_alarm
    //
    t[9UL] = 0;

    // unit
    //
    if (t[10UL])
    {
      i.unit_value.capacity (i.unit_size);
      grew = true;
    }

    // coef
    //
    t[11UL] = 0;

    // is_use
    //
    t[12UL] = 0;

    // is_show
    //
    t[13UL] = 0;

    // is_save
    //
    t[14UL] = 0;

    // default_value
    //
    t[15UL] = 0;

    // x
    //
    t[16UL] = 0;

    // y
    //
    t[17UL] = 0;

    // z
    //
    t[18UL] = 0;

    // is_delete
    //
    t[19UL] = 0;

    // max_alarm_level
    //
    t[20UL] = 0;

    // recon_model_sub_id
    //
    t[21UL] = 0;

    // is_abnormal
    //
    t[22UL] = 0;

    // _id
    //
    t[23UL] = 0;

    return grew;
  }

  void access::object_traits_impl< ::point, id_mysql >::
  bind (MYSQL_BIND* b,
        image_type& i,
        mysql::statement_kind sk)
  {
    ODB_POTENTIALLY_UNUSED (sk);

    using namespace mysql;

    std::size_t n (0);

    // name
    //
    b[n].buffer_type = MYSQL_TYPE_STRING;
    b[n].buffer = i.name_value.data ();
    b[n].buffer_length = static_cast<unsigned long> (
      i.name_value.capacity ());
    b[n].length = &i.name_size;
    b[n].is_null = &i.name_null;
    n++;

    // display_name
    //
    b[n].buffer_type = MYSQL_TYPE_STRING;
    b[n].buffer = i.display_name_value.data ();
    b[n].buffer_length = static_cast<unsigned long> (
      i.display_name_value.capacity ());
    b[n].length = &i.display_name_size;
    b[n].is_null = &i.display_name_null;
    n++;

    // object_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.object_id_value;
    b[n].is_null = &i.object_id_null;
    n++;

    // recon_type_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.recon_type_id_value;
    b[n].is_null = &i.recon_type_id_null;
    n++;

    // meter_type_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.meter_type_id_value;
    b[n].is_null = &i.meter_type_id_null;
    n++;

    // face_type_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.face_type_id_value;
    b[n].is_null = &i.face_type_id_null;
    n++;

    // hot_type_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.hot_type_id_value;
    b[n].is_null = &i.hot_type_id_null;
    n++;

    // save_type_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.save_type_id_value;
    b[n].is_null = &i.save_type_id_null;
    n++;

    // nis
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.nis_value;
    b[n].is_null = &i.nis_null;
    n++;

    // is_alarm
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.is_alarm_value;
    b[n].is_null = &i.is_alarm_null;
    n++;

    // unit
    //
    b[n].buffer_type = MYSQL_TYPE_STRING;
    b[n].buffer = i.unit_value.data ();
    b[n].buffer_length = static_cast<unsigned long> (
      i.unit_value.capacity ());
    b[n].length = &i.unit_size;
    b[n].is_null = &i.unit_null;
    n++;

    // coef
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.coef_value;
    b[n].is_null = &i.coef_null;
    n++;

    // is_use
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.is_use_value;
    b[n].is_null = &i.is_use_null;
    n++;

    // is_show
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.is_show_value;
    b[n].is_null = &i.is_show_null;
    n++;

    // is_save
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.is_save_value;
    b[n].is_null = &i.is_save_null;
    n++;

    // default_value
    //
    b[n].buffer_type = MYSQL_TYPE_FLOAT;
    b[n].buffer = &i.default_value_value;
    b[n].is_null = &i.default_value_null;
    n++;

    // x
    //
    b[n].buffer_type = MYSQL_TYPE_FLOAT;
    b[n].buffer = &i.x_value;
    b[n].is_null = &i.x_null;
    n++;

    // y
    //
    b[n].buffer_type = MYSQL_TYPE_FLOAT;
    b[n].buffer = &i.y_value;
    b[n].is_null = &i.y_null;
    n++;

    // z
    //
    b[n].buffer_type = MYSQL_TYPE_FLOAT;
    b[n].buffer = &i.z_value;
    b[n].is_null = &i.z_null;
    n++;

    // is_delete
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.is_delete_value;
    b[n].is_null = &i.is_delete_null;
    n++;

    // max_alarm_level
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.max_alarm_level_value;
    b[n].is_null = &i.max_alarm_level_null;
    n++;

    // recon_model_sub_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.recon_model_sub_id_value;
    b[n].is_null = &i.recon_model_sub_id_null;
    n++;

    // is_abnormal
    //
    b[n].buffer_type = MYSQL_TYPE_TINY;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.is_abnormal_value;
    b[n].is_null = &i.is_abnormal_null;
    n++;

    // _id
    //
    if (sk != statement_update)
    {
      b[n].buffer_type = MYSQL_TYPE_LONGLONG;
      b[n].is_unsigned = 1;
      b[n].buffer = &i._id_value;
      b[n].is_null = &i._id_null;
      n++;
    }
  }

  void access::object_traits_impl< ::point, id_mysql >::
  bind (MYSQL_BIND* b, id_image_type& i)
  {
    std::size_t n (0);
    b[n].buffer_type = MYSQL_TYPE_LONGLONG;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.id_value;
    b[n].is_null = &i.id_null;
  }

  bool access::object_traits_impl< ::point, id_mysql >::
  init (image_type& i,
        const object_type& o,
        mysql::statement_kind sk)
  {
    ODB_POTENTIALLY_UNUSED (i);
    ODB_POTENTIALLY_UNUSED (o);
    ODB_POTENTIALLY_UNUSED (sk);

    using namespace mysql;

    bool grew (false);

    // name
    //
    {
      ::std::string const& v =
        o.name;

      bool is_null (false);
      std::size_t size (0);
      std::size_t cap (i.name_value.capacity ());
      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_image (
        i.name_value,
        size,
        is_null,
        v);
      i.name_null = is_null;
      i.name_size = static_cast<unsigned long> (size);
      grew = grew || (cap != i.name_value.capacity ());
    }

    // display_name
    //
    {
      ::std::string const& v =
        o.display_name;

      bool is_null (false);
      std::size_t size (0);
      std::size_t cap (i.display_name_value.capacity ());
      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_image (
        i.display_name_value,
        size,
        is_null,
        v);
      i.display_name_null = is_null;
      i.display_name_size = static_cast<unsigned long> (size);
      grew = grew || (cap != i.display_name_value.capacity ());
    }

    // object_id
    //
    {
      int const& v =
        o.object_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.object_id_value, is_null, v);
      i.object_id_null = is_null;
    }

    // recon_type_id
    //
    {
      int const& v =
        o.recon_type_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.recon_type_id_value, is_null, v);
      i.recon_type_id_null = is_null;
    }

    // meter_type_id
    //
    {
      int const& v =
        o.meter_type_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.meter_type_id_value, is_null, v);
      i.meter_type_id_null = is_null;
    }

    // face_type_id
    //
    {
      int const& v =
        o.face_type_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.face_type_id_value, is_null, v);
      i.face_type_id_null = is_null;
    }

    // hot_type_id
    //
    {
      int const& v =
        o.hot_type_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.hot_type_id_value, is_null, v);
      i.hot_type_id_null = is_null;
    }

    // save_type_id
    //
    {
      int const& v =
        o.save_type_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.save_type_id_value, is_null, v);
      i.save_type_id_null = is_null;
    }

    // nis
    //
    {
      unsigned char const& v =
        o.nis;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.nis_value, is_null, v);
      i.nis_null = is_null;
    }

    // is_alarm
    //
    {
      unsigned char const& v =
        o.is_alarm;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.is_alarm_value, is_null, v);
      i.is_alarm_null = is_null;
    }

    // unit
    //
    {
      ::std::string const& v =
        o.unit;

      bool is_null (false);
      std::size_t size (0);
      std::size_t cap (i.unit_value.capacity ());
      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_image (
        i.unit_value,
        size,
        is_null,
        v);
      i.unit_null = is_null;
      i.unit_size = static_cast<unsigned long> (size);
      grew = grew || (cap != i.unit_value.capacity ());
    }

    // coef
    //
    {
      int const& v =
        o.coef;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.coef_value, is_null, v);
      i.coef_null = is_null;
    }

    // is_use
    //
    {
      unsigned char const& v =
        o.is_use;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.is_use_value, is_null, v);
      i.is_use_null = is_null;
    }

    // is_show
    //
    {
      unsigned char const& v =
        o.is_show;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.is_show_value, is_null, v);
      i.is_show_null = is_null;
    }

    // is_save
    //
    {
      unsigned char const& v =
        o.is_save;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.is_save_value, is_null, v);
      i.is_save_null = is_null;
    }

    // default_value
    //
    {
      float const& v =
        o.default_value;

      bool is_null (false);
      mysql::value_traits<
          float,
          mysql::id_float >::set_image (
        i.default_value_value, is_null, v);
      i.default_value_null = is_null;
    }

    // x
    //
    {
      float const& v =
        o.x;

      bool is_null (false);
      mysql::value_traits<
          float,
          mysql::id_float >::set_image (
        i.x_value, is_null, v);
      i.x_null = is_null;
    }

    // y
    //
    {
      float const& v =
        o.y;

      bool is_null (false);
      mysql::value_traits<
          float,
          mysql::id_float >::set_image (
        i.y_value, is_null, v);
      i.y_null = is_null;
    }

    // z
    //
    {
      float const& v =
        o.z;

      bool is_null (false);
      mysql::value_traits<
          float,
          mysql::id_float >::set_image (
        i.z_value, is_null, v);
      i.z_null = is_null;
    }

    // is_delete
    //
    {
      unsigned char const& v =
        o.is_delete;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.is_delete_value, is_null, v);
      i.is_delete_null = is_null;
    }

    // max_alarm_level
    //
    {
      unsigned char const& v =
        o.max_alarm_level;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.max_alarm_level_value, is_null, v);
      i.max_alarm_level_null = is_null;
    }

    // recon_model_sub_id
    //
    {
      int const& v =
        o.recon_model_sub_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.recon_model_sub_id_value, is_null, v);
      i.recon_model_sub_id_null = is_null;
    }

    // is_abnormal
    //
    {
      unsigned char const& v =
        o.is_abnormal;

      bool is_null (false);
      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_image (
        i.is_abnormal_value, is_null, v);
      i.is_abnormal_null = is_null;
    }

    // _id
    //
    if (sk == statement_insert)
    {
      long unsigned int const& v =
        o._id;

      bool is_null (false);
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_image (
        i._id_value, is_null, v);
      i._id_null = is_null;
    }

    return grew;
  }

  void access::object_traits_impl< ::point, id_mysql >::
  init (object_type& o,
        const image_type& i,
        database* db)
  {
    ODB_POTENTIALLY_UNUSED (o);
    ODB_POTENTIALLY_UNUSED (i);
    ODB_POTENTIALLY_UNUSED (db);

    // name
    //
    {
      ::std::string& v =
        o.name;

      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_value (
        v,
        i.name_value,
        i.name_size,
        i.name_null);
    }

    // display_name
    //
    {
      ::std::string& v =
        o.display_name;

      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_value (
        v,
        i.display_name_value,
        i.display_name_size,
        i.display_name_null);
    }

    // object_id
    //
    {
      int& v =
        o.object_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.object_id_value,
        i.object_id_null);
    }

    // recon_type_id
    //
    {
      int& v =
        o.recon_type_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.recon_type_id_value,
        i.recon_type_id_null);
    }

    // meter_type_id
    //
    {
      int& v =
        o.meter_type_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.meter_type_id_value,
        i.meter_type_id_null);
    }

    // face_type_id
    //
    {
      int& v =
        o.face_type_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.face_type_id_value,
        i.face_type_id_null);
    }

    // hot_type_id
    //
    {
      int& v =
        o.hot_type_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.hot_type_id_value,
        i.hot_type_id_null);
    }

    // save_type_id
    //
    {
      int& v =
        o.save_type_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.save_type_id_value,
        i.save_type_id_null);
    }

    // nis
    //
    {
      unsigned char& v =
        o.nis;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.nis_value,
        i.nis_null);
    }

    // is_alarm
    //
    {
      unsigned char& v =
        o.is_alarm;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.is_alarm_value,
        i.is_alarm_null);
    }

    // unit
    //
    {
      ::std::string& v =
        o.unit;

      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_value (
        v,
        i.unit_value,
        i.unit_size,
        i.unit_null);
    }

    // coef
    //
    {
      int& v =
        o.coef;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.coef_value,
        i.coef_null);
    }

    // is_use
    //
    {
      unsigned char& v =
        o.is_use;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.is_use_value,
        i.is_use_null);
    }

    // is_show
    //
    {
      unsigned char& v =
        o.is_show;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.is_show_value,
        i.is_show_null);
    }

    // is_save
    //
    {
      unsigned char& v =
        o.is_save;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.is_save_value,
        i.is_save_null);
    }

    // default_value
    //
    {
      float& v =
        o.default_value;

      mysql::value_traits<
          float,
          mysql::id_float >::set_value (
        v,
        i.default_value_value,
        i.default_value_null);
    }

    // x
    //
    {
      float& v =
        o.x;

      mysql::value_traits<
          float,
          mysql::id_float >::set_value (
        v,
        i.x_value,
        i.x_null);
    }

    // y
    //
    {
      float& v =
        o.y;

      mysql::value_traits<
          float,
          mysql::id_float >::set_value (
        v,
        i.y_value,
        i.y_null);
    }

    // z
    //
    {
      float& v =
        o.z;

      mysql::value_traits<
          float,
          mysql::id_float >::set_value (
        v,
        i.z_value,
        i.z_null);
    }

    // is_delete
    //
    {
      unsigned char& v =
        o.is_delete;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.is_delete_value,
        i.is_delete_null);
    }

    // max_alarm_level
    //
    {
      unsigned char& v =
        o.max_alarm_level;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.max_alarm_level_value,
        i.max_alarm_level_null);
    }

    // recon_model_sub_id
    //
    {
      int& v =
        o.recon_model_sub_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.recon_model_sub_id_value,
        i.recon_model_sub_id_null);
    }

    // is_abnormal
    //
    {
      unsigned char& v =
        o.is_abnormal;

      mysql::value_traits<
          unsigned char,
          mysql::id_utiny >::set_value (
        v,
        i.is_abnormal_value,
        i.is_abnormal_null);
    }

    // _id
    //
    {
      long unsigned int& v =
        o._id;

      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_value (
        v,
        i._id_value,
        i._id_null);
    }
  }

  void access::object_traits_impl< ::point, id_mysql >::
  init (id_image_type& i, const id_type& id)
  {
    {
      bool is_null (false);
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_image (
        i.id_value, is_null, id);
      i.id_null = is_null;
    }
  }

  const char access::object_traits_impl< ::point, id_mysql >::persist_statement[] =
  "INSERT INTO `point` "
  "(`name`, "
  "`display_name`, "
  "`object_id`, "
  "`recon_type_id`, "
  "`meter_type_id`, "
  "`face_type_id`, "
  "`hot_type_id`, "
  "`save_type_id`, "
  "`nis`, "
  "`is_alarm`, "
  "`unit`, "
  "`coef`, "
  "`is_use`, "
  "`is_show`, "
  "`is_save`, "
  "`default_value`, "
  "`x`, "
  "`y`, "
  "`z`, "
  "`is_delete`, "
  "`max_alarm_level`, "
  "`recon_model_sub_id`, "
  "`is_abnormal`, "
  "`id`) "
  "VALUES "
  "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

  const char access::object_traits_impl< ::point, id_mysql >::find_statement[] =
  "SELECT "
  "`point`.`name`, "
  "`point`.`display_name`, "
  "`point`.`object_id`, "
  "`point`.`recon_type_id`, "
  "`point`.`meter_type_id`, "
  "`point`.`face_type_id`, "
  "`point`.`hot_type_id`, "
  "`point`.`save_type_id`, "
  "`point`.`nis`, "
  "`point`.`is_alarm`, "
  "`point`.`unit`, "
  "`point`.`coef`, "
  "`point`.`is_use`, "
  "`point`.`is_show`, "
  "`point`.`is_save`, "
  "`point`.`default_value`, "
  "`point`.`x`, "
  "`point`.`y`, "
  "`point`.`z`, "
  "`point`.`is_delete`, "
  "`point`.`max_alarm_level`, "
  "`point`.`recon_model_sub_id`, "
  "`point`.`is_abnormal`, "
  "`point`.`id` "
  "FROM `point` "
  "WHERE `point`.`id`=?";

  const char access::object_traits_impl< ::point, id_mysql >::update_statement[] =
  "UPDATE `point` "
  "SET "
  "`name`=?, "
  "`display_name`=?, "
  "`object_id`=?, "
  "`recon_type_id`=?, "
  "`meter_type_id`=?, "
  "`face_type_id`=?, "
  "`hot_type_id`=?, "
  "`save_type_id`=?, "
  "`nis`=?, "
  "`is_alarm`=?, "
  "`unit`=?, "
  "`coef`=?, "
  "`is_use`=?, "
  "`is_show`=?, "
  "`is_save`=?, "
  "`default_value`=?, "
  "`x`=?, "
  "`y`=?, "
  "`z`=?, "
  "`is_delete`=?, "
  "`max_alarm_level`=?, "
  "`recon_model_sub_id`=?, "
  "`is_abnormal`=? "
  "WHERE `id`=?";

  const char access::object_traits_impl< ::point, id_mysql >::erase_statement[] =
  "DELETE FROM `point` "
  "WHERE `id`=?";

  const char access::object_traits_impl< ::point, id_mysql >::query_statement[] =
  "SELECT "
  "`point`.`name`, "
  "`point`.`display_name`, "
  "`point`.`object_id`, "
  "`point`.`recon_type_id`, "
  "`point`.`meter_type_id`, "
  "`point`.`face_type_id`, "
  "`point`.`hot_type_id`, "
  "`point`.`save_type_id`, "
  "`point`.`nis`, "
  "`point`.`is_alarm`, "
  "`point`.`unit`, "
  "`point`.`coef`, "
  "`point`.`is_use`, "
  "`point`.`is_show`, "
  "`point`.`is_save`, "
  "`point`.`default_value`, "
  "`point`.`x`, "
  "`point`.`y`, "
  "`point`.`z`, "
  "`point`.`is_delete`, "
  "`point`.`max_alarm_level`, "
  "`point`.`recon_model_sub_id`, "
  "`point`.`is_abnormal`, "
  "`point`.`id` "
  "FROM `point`";

  const char access::object_traits_impl< ::point, id_mysql >::erase_query_statement[] =
  "DELETE FROM `point`";

  const char access::object_traits_impl< ::point, id_mysql >::table_name[] =
  "`point`";

  void access::object_traits_impl< ::point, id_mysql >::
  persist (database& db, object_type& obj)
  {
    ODB_POTENTIALLY_UNUSED (db);

    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    callback (db,
              static_cast<const object_type&> (obj),
              callback_event::pre_persist);

    image_type& im (sts.image ());
    binding& imb (sts.insert_image_binding ());

    if (init (im, obj, statement_insert))
      im.version++;

    im._id_value = 0;

    if (im.version != sts.insert_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_insert);
      sts.insert_image_version (im.version);
      imb.version++;
    }

    {
      id_image_type& i (sts.id_image ());
      binding& b (sts.id_image_binding ());
      if (i.version != sts.id_image_version () || b.version == 0)
      {
        bind (b.bind, i);
        sts.id_image_version (i.version);
        b.version++;
      }
    }

    insert_statement& st (sts.persist_statement ());
    if (!st.execute ())
      throw object_already_persistent ();

    obj._id = id (sts.id_image ());

    callback (db,
              static_cast<const object_type&> (obj),
              callback_event::post_persist);
  }

  void access::object_traits_impl< ::point, id_mysql >::
  update (database& db, const object_type& obj)
  {
    ODB_POTENTIALLY_UNUSED (db);

    using namespace mysql;
    using mysql::update_statement;

    callback (db, obj, callback_event::pre_update);

    mysql::transaction& tr (mysql::transaction::current ());
    mysql::connection& conn (tr.connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    const id_type& id (
      obj._id);
    id_image_type& idi (sts.id_image ());
    init (idi, id);

    image_type& im (sts.image ());
    if (init (im, obj, statement_update))
      im.version++;

    bool u (false);
    binding& imb (sts.update_image_binding ());
    if (im.version != sts.update_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_update);
      sts.update_image_version (im.version);
      imb.version++;
      u = true;
    }

    binding& idb (sts.id_image_binding ());
    if (idi.version != sts.update_id_image_version () ||
        idb.version == 0)
    {
      if (idi.version != sts.id_image_version () ||
          idb.version == 0)
      {
        bind (idb.bind, idi);
        sts.id_image_version (idi.version);
        idb.version++;
      }

      sts.update_id_image_version (idi.version);

      if (!u)
        imb.version++;
    }

    update_statement& st (sts.update_statement ());
    if (st.execute () == 0)
      throw object_not_persistent ();

    callback (db, obj, callback_event::post_update);
    pointer_cache_traits::update (db, obj);
  }

  void access::object_traits_impl< ::point, id_mysql >::
  erase (database& db, const id_type& id)
  {
    using namespace mysql;

    ODB_POTENTIALLY_UNUSED (db);

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    id_image_type& i (sts.id_image ());
    init (i, id);

    binding& idb (sts.id_image_binding ());
    if (i.version != sts.id_image_version () || idb.version == 0)
    {
      bind (idb.bind, i);
      sts.id_image_version (i.version);
      idb.version++;
    }

    if (sts.erase_statement ().execute () != 1)
      throw object_not_persistent ();

    pointer_cache_traits::erase (db, id);
  }

  access::object_traits_impl< ::point, id_mysql >::pointer_type
  access::object_traits_impl< ::point, id_mysql >::
  find (database& db, const id_type& id)
  {
    using namespace mysql;

    {
      pointer_type p (pointer_cache_traits::find (db, id));

      if (!pointer_traits::null_ptr (p))
        return p;
    }

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    statements_type::auto_lock l (sts);

    if (l.locked ())
    {
      if (!find_ (sts, &id))
        return pointer_type ();
    }

    pointer_type p (
      access::object_factory<object_type, pointer_type>::create ());
    pointer_traits::guard pg (p);

    pointer_cache_traits::insert_guard ig (
      pointer_cache_traits::insert (db, id, p));

    object_type& obj (pointer_traits::get_ref (p));

    if (l.locked ())
    {
      select_statement& st (sts.find_statement ());
      ODB_POTENTIALLY_UNUSED (st);

      callback (db, obj, callback_event::pre_load);
      init (obj, sts.image (), &db);
      load_ (sts, obj, false);
      sts.load_delayed (0);
      l.unlock ();
      callback (db, obj, callback_event::post_load);
      pointer_cache_traits::load (ig.position ());
    }
    else
      sts.delay_load (id, obj, ig.position ());

    ig.release ();
    pg.release ();
    return p;
  }

  bool access::object_traits_impl< ::point, id_mysql >::
  find (database& db, const id_type& id, object_type& obj)
  {
    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    statements_type::auto_lock l (sts);

    if (!find_ (sts, &id))
      return false;

    select_statement& st (sts.find_statement ());
    ODB_POTENTIALLY_UNUSED (st);

    reference_cache_traits::position_type pos (
      reference_cache_traits::insert (db, id, obj));
    reference_cache_traits::insert_guard ig (pos);

    callback (db, obj, callback_event::pre_load);
    init (obj, sts.image (), &db);
    load_ (sts, obj, false);
    sts.load_delayed (0);
    l.unlock ();
    callback (db, obj, callback_event::post_load);
    reference_cache_traits::load (pos);
    ig.release ();
    return true;
  }

  bool access::object_traits_impl< ::point, id_mysql >::
  reload (database& db, object_type& obj)
  {
    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    statements_type::auto_lock l (sts);

    const id_type& id  (
      obj._id);

    if (!find_ (sts, &id))
      return false;

    select_statement& st (sts.find_statement ());
    ODB_POTENTIALLY_UNUSED (st);

    callback (db, obj, callback_event::pre_load);
    init (obj, sts.image (), &db);
    load_ (sts, obj, true);
    sts.load_delayed (0);
    l.unlock ();
    callback (db, obj, callback_event::post_load);
    return true;
  }

  bool access::object_traits_impl< ::point, id_mysql >::
  find_ (statements_type& sts,
         const id_type* id)
  {
    using namespace mysql;

    id_image_type& i (sts.id_image ());
    init (i, *id);

    binding& idb (sts.id_image_binding ());
    if (i.version != sts.id_image_version () || idb.version == 0)
    {
      bind (idb.bind, i);
      sts.id_image_version (i.version);
      idb.version++;
    }

    image_type& im (sts.image ());
    binding& imb (sts.select_image_binding ());

    if (im.version != sts.select_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_select);
      sts.select_image_version (im.version);
      imb.version++;
    }

    select_statement& st (sts.find_statement ());

    st.execute ();
    auto_result ar (st);
    select_statement::result r (st.fetch ());

    if (r == select_statement::truncated)
    {
      if (grow (im, sts.select_image_truncated ()))
        im.version++;

      if (im.version != sts.select_image_version ())
      {
        bind (imb.bind, im, statement_select);
        sts.select_image_version (im.version);
        imb.version++;
        st.refetch ();
      }
    }

    return r != select_statement::no_data;
  }

  result< access::object_traits_impl< ::point, id_mysql >::object_type >
  access::object_traits_impl< ::point, id_mysql >::
  query (database&, const query_base_type& q)
  {
    using namespace mysql;
    using odb::details::shared;
    using odb::details::shared_ptr;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());

    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    image_type& im (sts.image ());
    binding& imb (sts.select_image_binding ());

    if (im.version != sts.select_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_select);
      sts.select_image_version (im.version);
      imb.version++;
    }

    std::string text (query_statement);
    if (!q.empty ())
    {
      text += " ";
      text += q.clause ();
    }

    q.init_parameters ();
    shared_ptr<select_statement> st (
      new (shared) select_statement (
        conn,
        text,
        false,
        true,
        q.parameters_binding (),
        imb));

    st->execute ();

    shared_ptr< odb::object_result_impl<object_type> > r (
      new (shared) mysql::object_result_impl<object_type> (
        q, st, sts, 0));

    return result<object_type> (r);
  }

  unsigned long long access::object_traits_impl< ::point, id_mysql >::
  erase_query (database&, const query_base_type& q)
  {
    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());

    std::string text (erase_query_statement);
    if (!q.empty ())
    {
      text += ' ';
      text += q.clause ();
    }

    q.init_parameters ();
    delete_statement st (
      conn,
      text,
      q.parameters_binding ());

    return st.execute ();
  }
}

#include <odb/post.hxx>
