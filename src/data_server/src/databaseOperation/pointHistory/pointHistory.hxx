#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class point_history
{
public:
    point_history(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int task_history_id;
    int robot_id;
    int point_id;
    string camera_pic;
    string flir_pic;
    string sound;
    float value;
    string recon_time;
    int watch_point_id;
    unsigned char check_status;
    unsigned char recon_status;
    float modify_value;
    string create_time;
    int operater_id;
    unsigned char entity_status;
    string max_temp_position;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
