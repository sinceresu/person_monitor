#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class area
{
public:
    area(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string name;
    int type;
    string area_range;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
