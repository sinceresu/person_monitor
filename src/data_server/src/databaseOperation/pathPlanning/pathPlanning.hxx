#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class path_planning
{
public:
    path_planning(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int task_id;
    int task_history_id;
    string path;
    int flag;
    int robot_id;
    string record;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
