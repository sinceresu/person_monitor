// This file was generated by ODB, object-relational mapping (ORM)
// compiler for C++.
//

#include <odb/pre.hxx>

#include "pathPlanning-odb.hxx"

#include <cassert>
#include <cstring>  // std::memcpy


#include <odb/mysql/traits.hxx>
#include <odb/mysql/database.hxx>
#include <odb/mysql/transaction.hxx>
#include <odb/mysql/connection.hxx>
#include <odb/mysql/statement.hxx>
#include <odb/mysql/statement-cache.hxx>
#include <odb/mysql/simple-object-statements.hxx>
#include <odb/mysql/container-statements.hxx>
#include <odb/mysql/exceptions.hxx>
#include <odb/mysql/simple-object-result.hxx>
#include <odb/mysql/enum.hxx>

namespace odb
{
  // path_planning
  //

  struct access::object_traits_impl< ::path_planning, id_mysql >::extra_statement_cache_type
  {
    extra_statement_cache_type (
      mysql::connection&,
      image_type&,
      id_image_type&,
      mysql::binding&,
      mysql::binding&)
    {
    }
  };

  access::object_traits_impl< ::path_planning, id_mysql >::id_type
  access::object_traits_impl< ::path_planning, id_mysql >::
  id (const id_image_type& i)
  {
    mysql::database* db (0);
    ODB_POTENTIALLY_UNUSED (db);

    id_type id;
    {
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_value (
        id,
        i.id_value,
        i.id_null);
    }

    return id;
  }

  access::object_traits_impl< ::path_planning, id_mysql >::id_type
  access::object_traits_impl< ::path_planning, id_mysql >::
  id (const image_type& i)
  {
    mysql::database* db (0);
    ODB_POTENTIALLY_UNUSED (db);

    id_type id;
    {
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_value (
        id,
        i._id_value,
        i._id_null);
    }

    return id;
  }

  bool access::object_traits_impl< ::path_planning, id_mysql >::
  grow (image_type& i,
        my_bool* t)
  {
    ODB_POTENTIALLY_UNUSED (i);
    ODB_POTENTIALLY_UNUSED (t);

    bool grew (false);

    // task_id
    //
    t[0UL] = 0;

    // task_history_id
    //
    t[1UL] = 0;

    // path
    //
    if (t[2UL])
    {
      i.path_value.capacity (i.path_size);
      grew = true;
    }

    // flag
    //
    t[3UL] = 0;

    // robot_id
    //
    t[4UL] = 0;

    // record
    //
    if (t[5UL])
    {
      i.record_value.capacity (i.record_size);
      grew = true;
    }

    // _id
    //
    t[6UL] = 0;

    return grew;
  }

  void access::object_traits_impl< ::path_planning, id_mysql >::
  bind (MYSQL_BIND* b,
        image_type& i,
        mysql::statement_kind sk)
  {
    ODB_POTENTIALLY_UNUSED (sk);

    using namespace mysql;

    std::size_t n (0);

    // task_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.task_id_value;
    b[n].is_null = &i.task_id_null;
    n++;

    // task_history_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.task_history_id_value;
    b[n].is_null = &i.task_history_id_null;
    n++;

    // path
    //
    b[n].buffer_type = MYSQL_TYPE_STRING;
    b[n].buffer = i.path_value.data ();
    b[n].buffer_length = static_cast<unsigned long> (
      i.path_value.capacity ());
    b[n].length = &i.path_size;
    b[n].is_null = &i.path_null;
    n++;

    // flag
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.flag_value;
    b[n].is_null = &i.flag_null;
    n++;

    // robot_id
    //
    b[n].buffer_type = MYSQL_TYPE_LONG;
    b[n].is_unsigned = 0;
    b[n].buffer = &i.robot_id_value;
    b[n].is_null = &i.robot_id_null;
    n++;

    // record
    //
    b[n].buffer_type = MYSQL_TYPE_STRING;
    b[n].buffer = i.record_value.data ();
    b[n].buffer_length = static_cast<unsigned long> (
      i.record_value.capacity ());
    b[n].length = &i.record_size;
    b[n].is_null = &i.record_null;
    n++;

    // _id
    //
    if (sk != statement_update)
    {
      b[n].buffer_type = MYSQL_TYPE_LONGLONG;
      b[n].is_unsigned = 1;
      b[n].buffer = &i._id_value;
      b[n].is_null = &i._id_null;
      n++;
    }
  }

  void access::object_traits_impl< ::path_planning, id_mysql >::
  bind (MYSQL_BIND* b, id_image_type& i)
  {
    std::size_t n (0);
    b[n].buffer_type = MYSQL_TYPE_LONGLONG;
    b[n].is_unsigned = 1;
    b[n].buffer = &i.id_value;
    b[n].is_null = &i.id_null;
  }

  bool access::object_traits_impl< ::path_planning, id_mysql >::
  init (image_type& i,
        const object_type& o,
        mysql::statement_kind sk)
  {
    ODB_POTENTIALLY_UNUSED (i);
    ODB_POTENTIALLY_UNUSED (o);
    ODB_POTENTIALLY_UNUSED (sk);

    using namespace mysql;

    bool grew (false);

    // task_id
    //
    {
      int const& v =
        o.task_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.task_id_value, is_null, v);
      i.task_id_null = is_null;
    }

    // task_history_id
    //
    {
      int const& v =
        o.task_history_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.task_history_id_value, is_null, v);
      i.task_history_id_null = is_null;
    }

    // path
    //
    {
      ::std::string const& v =
        o.path;

      bool is_null (false);
      std::size_t size (0);
      std::size_t cap (i.path_value.capacity ());
      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_image (
        i.path_value,
        size,
        is_null,
        v);
      i.path_null = is_null;
      i.path_size = static_cast<unsigned long> (size);
      grew = grew || (cap != i.path_value.capacity ());
    }

    // flag
    //
    {
      int const& v =
        o.flag;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.flag_value, is_null, v);
      i.flag_null = is_null;
    }

    // robot_id
    //
    {
      int const& v =
        o.robot_id;

      bool is_null (false);
      mysql::value_traits<
          int,
          mysql::id_long >::set_image (
        i.robot_id_value, is_null, v);
      i.robot_id_null = is_null;
    }

    // record
    //
    {
      ::std::string const& v =
        o.record;

      bool is_null (false);
      std::size_t size (0);
      std::size_t cap (i.record_value.capacity ());
      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_image (
        i.record_value,
        size,
        is_null,
        v);
      i.record_null = is_null;
      i.record_size = static_cast<unsigned long> (size);
      grew = grew || (cap != i.record_value.capacity ());
    }

    // _id
    //
    if (sk == statement_insert)
    {
      long unsigned int const& v =
        o._id;

      bool is_null (false);
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_image (
        i._id_value, is_null, v);
      i._id_null = is_null;
    }

    return grew;
  }

  void access::object_traits_impl< ::path_planning, id_mysql >::
  init (object_type& o,
        const image_type& i,
        database* db)
  {
    ODB_POTENTIALLY_UNUSED (o);
    ODB_POTENTIALLY_UNUSED (i);
    ODB_POTENTIALLY_UNUSED (db);

    // task_id
    //
    {
      int& v =
        o.task_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.task_id_value,
        i.task_id_null);
    }

    // task_history_id
    //
    {
      int& v =
        o.task_history_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.task_history_id_value,
        i.task_history_id_null);
    }

    // path
    //
    {
      ::std::string& v =
        o.path;

      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_value (
        v,
        i.path_value,
        i.path_size,
        i.path_null);
    }

    // flag
    //
    {
      int& v =
        o.flag;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.flag_value,
        i.flag_null);
    }

    // robot_id
    //
    {
      int& v =
        o.robot_id;

      mysql::value_traits<
          int,
          mysql::id_long >::set_value (
        v,
        i.robot_id_value,
        i.robot_id_null);
    }

    // record
    //
    {
      ::std::string& v =
        o.record;

      mysql::value_traits<
          ::std::string,
          mysql::id_string >::set_value (
        v,
        i.record_value,
        i.record_size,
        i.record_null);
    }

    // _id
    //
    {
      long unsigned int& v =
        o._id;

      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_value (
        v,
        i._id_value,
        i._id_null);
    }
  }

  void access::object_traits_impl< ::path_planning, id_mysql >::
  init (id_image_type& i, const id_type& id)
  {
    {
      bool is_null (false);
      mysql::value_traits<
          long unsigned int,
          mysql::id_ulonglong >::set_image (
        i.id_value, is_null, id);
      i.id_null = is_null;
    }
  }

  const char access::object_traits_impl< ::path_planning, id_mysql >::persist_statement[] =
  "INSERT INTO `path_planning` "
  "(`task_id`, "
  "`task_history_id`, "
  "`path`, "
  "`flag`, "
  "`robot_id`, "
  "`record`, "
  "`id`) "
  "VALUES "
  "(?, ?, ?, ?, ?, ?, ?)";

  const char access::object_traits_impl< ::path_planning, id_mysql >::find_statement[] =
  "SELECT "
  "`path_planning`.`task_id`, "
  "`path_planning`.`task_history_id`, "
  "`path_planning`.`path`, "
  "`path_planning`.`flag`, "
  "`path_planning`.`robot_id`, "
  "`path_planning`.`record`, "
  "`path_planning`.`id` "
  "FROM `path_planning` "
  "WHERE `path_planning`.`id`=?";

  const char access::object_traits_impl< ::path_planning, id_mysql >::update_statement[] =
  "UPDATE `path_planning` "
  "SET "
  "`task_id`=?, "
  "`task_history_id`=?, "
  "`path`=?, "
  "`flag`=?, "
  "`robot_id`=?, "
  "`record`=? "
  "WHERE `id`=?";

  const char access::object_traits_impl< ::path_planning, id_mysql >::erase_statement[] =
  "DELETE FROM `path_planning` "
  "WHERE `id`=?";

  const char access::object_traits_impl< ::path_planning, id_mysql >::query_statement[] =
  "SELECT "
  "`path_planning`.`task_id`, "
  "`path_planning`.`task_history_id`, "
  "`path_planning`.`path`, "
  "`path_planning`.`flag`, "
  "`path_planning`.`robot_id`, "
  "`path_planning`.`record`, "
  "`path_planning`.`id` "
  "FROM `path_planning`";

  const char access::object_traits_impl< ::path_planning, id_mysql >::erase_query_statement[] =
  "DELETE FROM `path_planning`";

  const char access::object_traits_impl< ::path_planning, id_mysql >::table_name[] =
  "`path_planning`";

  void access::object_traits_impl< ::path_planning, id_mysql >::
  persist (database& db, object_type& obj)
  {
    ODB_POTENTIALLY_UNUSED (db);

    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    callback (db,
              static_cast<const object_type&> (obj),
              callback_event::pre_persist);

    image_type& im (sts.image ());
    binding& imb (sts.insert_image_binding ());

    if (init (im, obj, statement_insert))
      im.version++;

    im._id_value = 0;

    if (im.version != sts.insert_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_insert);
      sts.insert_image_version (im.version);
      imb.version++;
    }

    {
      id_image_type& i (sts.id_image ());
      binding& b (sts.id_image_binding ());
      if (i.version != sts.id_image_version () || b.version == 0)
      {
        bind (b.bind, i);
        sts.id_image_version (i.version);
        b.version++;
      }
    }

    insert_statement& st (sts.persist_statement ());
    if (!st.execute ())
      throw object_already_persistent ();

    obj._id = id (sts.id_image ());

    callback (db,
              static_cast<const object_type&> (obj),
              callback_event::post_persist);
  }

  void access::object_traits_impl< ::path_planning, id_mysql >::
  update (database& db, const object_type& obj)
  {
    ODB_POTENTIALLY_UNUSED (db);

    using namespace mysql;
    using mysql::update_statement;

    callback (db, obj, callback_event::pre_update);

    mysql::transaction& tr (mysql::transaction::current ());
    mysql::connection& conn (tr.connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    const id_type& id (
      obj._id);
    id_image_type& idi (sts.id_image ());
    init (idi, id);

    image_type& im (sts.image ());
    if (init (im, obj, statement_update))
      im.version++;

    bool u (false);
    binding& imb (sts.update_image_binding ());
    if (im.version != sts.update_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_update);
      sts.update_image_version (im.version);
      imb.version++;
      u = true;
    }

    binding& idb (sts.id_image_binding ());
    if (idi.version != sts.update_id_image_version () ||
        idb.version == 0)
    {
      if (idi.version != sts.id_image_version () ||
          idb.version == 0)
      {
        bind (idb.bind, idi);
        sts.id_image_version (idi.version);
        idb.version++;
      }

      sts.update_id_image_version (idi.version);

      if (!u)
        imb.version++;
    }

    update_statement& st (sts.update_statement ());
    if (st.execute () == 0)
      throw object_not_persistent ();

    callback (db, obj, callback_event::post_update);
    pointer_cache_traits::update (db, obj);
  }

  void access::object_traits_impl< ::path_planning, id_mysql >::
  erase (database& db, const id_type& id)
  {
    using namespace mysql;

    ODB_POTENTIALLY_UNUSED (db);

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    id_image_type& i (sts.id_image ());
    init (i, id);

    binding& idb (sts.id_image_binding ());
    if (i.version != sts.id_image_version () || idb.version == 0)
    {
      bind (idb.bind, i);
      sts.id_image_version (i.version);
      idb.version++;
    }

    if (sts.erase_statement ().execute () != 1)
      throw object_not_persistent ();

    pointer_cache_traits::erase (db, id);
  }

  access::object_traits_impl< ::path_planning, id_mysql >::pointer_type
  access::object_traits_impl< ::path_planning, id_mysql >::
  find (database& db, const id_type& id)
  {
    using namespace mysql;

    {
      pointer_type p (pointer_cache_traits::find (db, id));

      if (!pointer_traits::null_ptr (p))
        return p;
    }

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    statements_type::auto_lock l (sts);

    if (l.locked ())
    {
      if (!find_ (sts, &id))
        return pointer_type ();
    }

    pointer_type p (
      access::object_factory<object_type, pointer_type>::create ());
    pointer_traits::guard pg (p);

    pointer_cache_traits::insert_guard ig (
      pointer_cache_traits::insert (db, id, p));

    object_type& obj (pointer_traits::get_ref (p));

    if (l.locked ())
    {
      select_statement& st (sts.find_statement ());
      ODB_POTENTIALLY_UNUSED (st);

      callback (db, obj, callback_event::pre_load);
      init (obj, sts.image (), &db);
      load_ (sts, obj, false);
      sts.load_delayed (0);
      l.unlock ();
      callback (db, obj, callback_event::post_load);
      pointer_cache_traits::load (ig.position ());
    }
    else
      sts.delay_load (id, obj, ig.position ());

    ig.release ();
    pg.release ();
    return p;
  }

  bool access::object_traits_impl< ::path_planning, id_mysql >::
  find (database& db, const id_type& id, object_type& obj)
  {
    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    statements_type::auto_lock l (sts);

    if (!find_ (sts, &id))
      return false;

    select_statement& st (sts.find_statement ());
    ODB_POTENTIALLY_UNUSED (st);

    reference_cache_traits::position_type pos (
      reference_cache_traits::insert (db, id, obj));
    reference_cache_traits::insert_guard ig (pos);

    callback (db, obj, callback_event::pre_load);
    init (obj, sts.image (), &db);
    load_ (sts, obj, false);
    sts.load_delayed (0);
    l.unlock ();
    callback (db, obj, callback_event::post_load);
    reference_cache_traits::load (pos);
    ig.release ();
    return true;
  }

  bool access::object_traits_impl< ::path_planning, id_mysql >::
  reload (database& db, object_type& obj)
  {
    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());
    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    statements_type::auto_lock l (sts);

    const id_type& id  (
      obj._id);

    if (!find_ (sts, &id))
      return false;

    select_statement& st (sts.find_statement ());
    ODB_POTENTIALLY_UNUSED (st);

    callback (db, obj, callback_event::pre_load);
    init (obj, sts.image (), &db);
    load_ (sts, obj, true);
    sts.load_delayed (0);
    l.unlock ();
    callback (db, obj, callback_event::post_load);
    return true;
  }

  bool access::object_traits_impl< ::path_planning, id_mysql >::
  find_ (statements_type& sts,
         const id_type* id)
  {
    using namespace mysql;

    id_image_type& i (sts.id_image ());
    init (i, *id);

    binding& idb (sts.id_image_binding ());
    if (i.version != sts.id_image_version () || idb.version == 0)
    {
      bind (idb.bind, i);
      sts.id_image_version (i.version);
      idb.version++;
    }

    image_type& im (sts.image ());
    binding& imb (sts.select_image_binding ());

    if (im.version != sts.select_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_select);
      sts.select_image_version (im.version);
      imb.version++;
    }

    select_statement& st (sts.find_statement ());

    st.execute ();
    auto_result ar (st);
    select_statement::result r (st.fetch ());

    if (r == select_statement::truncated)
    {
      if (grow (im, sts.select_image_truncated ()))
        im.version++;

      if (im.version != sts.select_image_version ())
      {
        bind (imb.bind, im, statement_select);
        sts.select_image_version (im.version);
        imb.version++;
        st.refetch ();
      }
    }

    return r != select_statement::no_data;
  }

  result< access::object_traits_impl< ::path_planning, id_mysql >::object_type >
  access::object_traits_impl< ::path_planning, id_mysql >::
  query (database&, const query_base_type& q)
  {
    using namespace mysql;
    using odb::details::shared;
    using odb::details::shared_ptr;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());

    statements_type& sts (
      conn.statement_cache ().find_object<object_type> ());

    image_type& im (sts.image ());
    binding& imb (sts.select_image_binding ());

    if (im.version != sts.select_image_version () ||
        imb.version == 0)
    {
      bind (imb.bind, im, statement_select);
      sts.select_image_version (im.version);
      imb.version++;
    }

    std::string text (query_statement);
    if (!q.empty ())
    {
      text += " ";
      text += q.clause ();
    }

    q.init_parameters ();
    shared_ptr<select_statement> st (
      new (shared) select_statement (
        conn,
        text,
        false,
        true,
        q.parameters_binding (),
        imb));

    st->execute ();

    shared_ptr< odb::object_result_impl<object_type> > r (
      new (shared) mysql::object_result_impl<object_type> (
        q, st, sts, 0));

    return result<object_type> (r);
  }

  unsigned long long access::object_traits_impl< ::path_planning, id_mysql >::
  erase_query (database&, const query_base_type& q)
  {
    using namespace mysql;

    mysql::connection& conn (
      mysql::transaction::current ().connection ());

    std::string text (erase_query_statement);
    if (!q.empty ())
    {
      text += ' ';
      text += q.clause ();
    }

    q.init_parameters ();
    delete_statement st (
      conn,
      text,
      q.parameters_binding ());

    return st.execute ();
  }
}

#include <odb/post.hxx>
