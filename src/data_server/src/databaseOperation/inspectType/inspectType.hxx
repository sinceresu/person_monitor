#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class inspect_type
{
public:
    inspect_type(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    string name;
    int level;
    int parent_id;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
