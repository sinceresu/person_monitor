#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class recon_model_sub
{
public:
    recon_model_sub(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int recon_model_id;
    string name;
    string display_name;
    string description;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
