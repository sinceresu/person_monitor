#ifndef DB_DATABASE_BASICS_H
#define DB_DATABASE_BASICS_H

#pragma once
#include <mutex>
#include <pthread.h>
#include "odb/database.hxx"
#include "odb/transaction.hxx"
#include "odb/mysql/database.hxx"

#include "inspectType/inspectType.hxx"
#include "inspectType/inspectType-odb.hxx"
#include "task/task.hxx"
#include "task/task-odb.hxx"
#include "taskPoint/taskPoint.hxx"
#include "taskPoint/taskPoint-odb.hxx"
#include "taskHistory/taskHistory.hxx"
#include "taskHistory/taskHistory-odb.hxx"
#include "point/point.hxx"
#include "point/point-odb.hxx"
#include "pointHistory/pointHistory.hxx"
#include "pointHistory/pointHistory-odb.hxx"
#include "pointVisibleLight/pointVisibleLight.hxx"
#include "pointVisibleLight/pointVisibleLight-odb.hxx"
#include "reconModelSub/reconModelSub.hxx"
#include "reconModelSub/reconModelSub-odb.hxx"
#include "meterPointInfo/meterPointInfo.hxx"
#include "meterPointInfo/meterPointInfo-odb.hxx"
#include "pathPlanning/pathPlanning.hxx"
#include "pathPlanning/pathPlanning-odb.hxx"
#include "watchPos/watchPos.hxx"
#include "watchPos/watchPos-odb.hxx"
#include "stopPoint/stopPoint.hxx"
#include "stopPoint/stopPoint-odb.hxx"
#include "routePos/routePos.hxx"
#include "routePos/routePos-odb.hxx"
#include "route/route.hxx"
#include "route/route-odb.hxx"
#include "watchPoint/watchPoint.hxx"
#include "watchPoint/watchPoint-odb.hxx"
#include "pointWatchPoint/pointWatchPoint.hxx"
#include "pointWatchPoint/pointWatchPoint-odb.hxx"
#include "object/object.hxx"
#include "object/object-odb.hxx"
#include "robotParam/robotParam.hxx"
#include "robotParam/robotParam-odb.hxx"
#include "fullInspectStopPoint/fullInspectStopPoint.hxx"
#include "fullInspectStopPoint/fullInspectStopPoint-odb.hxx"
#include "robot/robot.hxx"
#include "robot/robot-odb.hxx"
#include "camera/camera.hxx"
#include "camera/camera-odb.hxx"
#include "area/area.hxx"
#include "area/area-odb.hxx"

using namespace odb::core;
using namespace std;

typedef odb::query<inspect_type> inspectType_query;
typedef odb::result<inspect_type> inspectType_result;

typedef odb::query<task> task_query;
typedef odb::result<task> task_result;

typedef odb::query<task_point> taskPoint_query;
typedef odb::result<task_point> taskPoint_result;

typedef odb::query<task_history> taskHistory_query;
typedef odb::result<task_history> taskHistory_result;

typedef odb::query<point> point_query;
typedef odb::result<point> point_result;

typedef odb::query<point_history> pointHistory_query;
typedef odb::result<point_history> pointHistory_result;

typedef odb::query<point_visible_light> pointVisibleLight_query;
typedef odb::result<point_visible_light> pointVisibleLight_result;

typedef odb::query<recon_model_sub> reconModeSub_query;
typedef odb::result<recon_model_sub> reconModeSub_result;

typedef odb::query<meter_point_info> meterPointInfo_query;
typedef odb::result<meter_point_info> meterPointInfo_result;

typedef odb::query<path_planning> pathPlanning_query;
typedef odb::result<path_planning> pathPlanning_result;

typedef odb::query<watch_pos> watchPos_query;
typedef odb::result<watch_pos> watchPos_result;

typedef odb::query<stop_point> stopPoint_query;
typedef odb::result<stop_point> stopPoint_result;

typedef odb::query<route_pos> routePos_query;
typedef odb::result<route_pos> routePos_result;

typedef odb::query<route> route_query;
typedef odb::result<route> route_result;

typedef odb::query<watch_point> watchPoint_query;
typedef odb::result<watch_point> watchPoint_result;

typedef odb::query<point_watch_point> pointWatchPoint_query;
typedef odb::result<point_watch_point> pointWatchPoint_result;

typedef odb::query<object> object_query;
typedef odb::result<object> object_result;

typedef odb::query<full_inspect_stop_point> fullInspectStopPoint_query;
typedef odb::result<full_inspect_stop_point> fullInspectStopPoint_result;

typedef odb::query<robot_param> robotParam_query;
typedef odb::result<robot_param> robotParam_result;

typedef odb::query<robot> robot_query;
typedef odb::result<robot> robot_result;

typedef odb::query<camera> camera_query;
typedef odb::result<camera> camera_result;

typedef odb::query<area> area_query;
typedef odb::result<area> area_result;

class CDatabaseBasics
{
public:
    CDatabaseBasics();
    CDatabaseBasics(const char *_user,
                    const char *_passwd,
                    const char *_db,
                    const char *_host = 0,
                    unsigned int _port = 0,
                    const char *_socket = 0,
                    const char *_charset = 0);
    ~CDatabaseBasics();

public:
    static odb::mysql::database *db;

public:
    static std::mutex mtx;
    static pthread_mutex_t pt_mutex_t;
};

#endif

// odb -d mysql --generate-query --generate-schema person.hxx