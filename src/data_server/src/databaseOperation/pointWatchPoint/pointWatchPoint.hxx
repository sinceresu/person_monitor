#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class point_watch_point
{
public:
    point_watch_point(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int point_id;
    int watch_point_id;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
