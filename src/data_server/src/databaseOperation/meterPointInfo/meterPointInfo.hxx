#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class meter_point_info
{
public:
    meter_point_info(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int point_id;
    float range_min;
    float range_max;
    float angle_min;
    float angle_max;
    int meter_type_id;
    int offset_value;
    int direction;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
