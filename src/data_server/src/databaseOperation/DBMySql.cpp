#include "DBMySql.hpp"

/**
 * Example
 * 
 * CDBMySql db;
 * db.connect("192.168.0.39", "root", "root", "ydrobot", 0, NULL, 0);
 * db.setCharacter("utf8");
 * db.insertQuery("INSERT INTO device_type (id, name, display_name, description) VALUES('78', '6000', '飞飞', 'abc')");
 * db.selectQuery("SELECT * FROM device");
*/

/**
  * 函数名称:       DBMySql
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CDBMySql::CDBMySql()
{
    mysql_library_init(0, NULL, NULL);
    init();
}

/**
  * 函数名称:       ~DBMySql
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CDBMySql::~CDBMySql()
{
    disconnect();
    mysql_library_end();
}

/**
  * 函数名称:       init
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       初始化MySQL
  * 函数参数:
  * 返回值:         bool
*/
bool CDBMySql::init()
{
    if (mysql_init(&m_mySql) == NULL)
    {
        cout << __FILE__ << __LINE__ << __FUNCTION__ << __DATE__ << __TIME__ << "mysql_init error" << endl;
        return false;
    }

    return true;
}

/**
  * 函数名称:       connect
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       连接MySQL
  * 函数参数:       const char *host        连接地址
  * 函数参数:       const char *user        用户名
  * 函数参数:       const char *passwd      密码
  * 函数参数:       const char *db          数据库名称
  * 函数参数:       const char *port        端口
  * 函数参数:       const char *unix_socket NULL
  * 函数参数:       const char *clientflag  以允许特定功能
  * 返回值:         bool
*/
bool CDBMySql::connect(const char *host,
                       const char *user,
                       const char *passwd,
                       const char *db,
                       unsigned int port,
                       const char *unix_socket,
                       unsigned long clientflag)
{
    // 连接数据库
    if (mysql_real_connect(&m_mySql, host, user, passwd, db, port, unix_socket, clientflag) == NULL)
    {
        cout << __FILE__ << __LINE__ << __FUNCTION__ << __DATE__ << __TIME__ << "mysql_real_connect error" << endl;
        cout << mysql_error(&m_mySql) << endl;
        return false;
    }

    return true;
}

/**
  * 函数名称:       closeConnect
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       关闭连接MySQL
  * 函数参数:
  * 返回值:         void
*/
void CDBMySql::disconnect()
{
    mysql_close(&m_mySql);
}

/**
  * 函数名称:       setCharacter
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       设置MySQL字符编码
  * 函数参数:       char *csname        字符集名称
  * 返回值:         int
*/
int CDBMySql::setCharacter(const char *csname)
{
    int nRet = mysql_set_character_set(&m_mySql, csname);
    if (nRet != 0)
    {
    }

    return nRet;
}

/**
  * 函数名称:       selectQuery
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       查询MySql
  * 函数参数:       const char *szSql       Sql语句
  * 返回值:         int
*/
int CDBMySql::selectQuery(const char *szSql)
{
    try
    {
        int nRet = 0;
        // 查询
        nRet = mysql_real_query(&m_mySql, szSql, strlen(szSql));
        if (nRet != 0)
        {
            return nRet;
        }

        // 取结果集
        m_res = mysql_store_result(&m_mySql);
        if (m_res == NULL)
        {
            return -1;
        }

        while ((m_field = mysql_fetch_field(m_res)))
        {
            cout << setiosflags(ios::left) << setw(15) << m_field->name;
        }
        cout << endl;
        while (m_row = mysql_fetch_row(m_res)) // 获取具体的数据
        {
            for (int i = 0; i < mysql_num_fields(m_res); i++)
            {
                if (m_row[i])
                    cout << setiosflags(ios::left) << setw(15) << m_row[i];
            }
            cout << endl;
        }
    }
    catch (...)
    {
        return -1;
    }

    return 0;
}

/**
  * 函数名称:       insertQuery
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       插入MySql
  * 函数参数:       const char *szSql       Sql语句
  * 返回值:         int
*/
int CDBMySql::insertQuery(const char *szSql)
{
    try
    {
        int nRet = 0;
        // 查询
        nRet = mysql_real_query(&m_mySql, szSql, strlen(szSql));
        if (nRet != 0)
        {
            return nRet;
        }
    }
    catch (...)
    {
        return -1;
    }

    return 0;
}