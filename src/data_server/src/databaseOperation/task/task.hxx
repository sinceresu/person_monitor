#pragma once
#include <string>
#include "odb/core.hxx"

using namespace std;

#pragma db object
class task
{
public:
    task(){};

    unsigned long id() { return _id; }
    void id(unsigned long i) { _id = i; }

public:
    int robot_id;
    string name;
    int inspect_type_id;
    string description;
    unsigned char is_update;
    unsigned char is_custom;
    int operater_id;
    string create_time;
    string update_time;
    int is_delete;

private:
    friend class odb::access;

#pragma db id auto
    unsigned long _id;
};
