#include "databaseBasics.hpp"

// transaction m_t(CDatabaseBasics::db->begin());
// point *point_Ret(CDatabaseBasics::db->query_one<point>(point_query::id == 10));
// odb::result<point> all_point_Ret(CDatabaseBasics::db->query<point>(point_query::id == 10));
// for (odb::result<point>::iterator it = all_point_Ret.begin(); it != all_point_Ret.end(); it++)
// {
//   cout << setiosflags(ios::left) << setw(10) << it->id();
//   cout << setiosflags(ios::left) << setw(10) << it->name;
//   cout << endl;
// }
// m_t.commit();

odb::mysql::database *CDatabaseBasics::db = nullptr;

std::mutex CDatabaseBasics::mtx;
pthread_mutex_t CDatabaseBasics::pt_mutex_t = PTHREAD_MUTEX_INITIALIZER;

/**
  * 函数名称:       CDatabaseBasics
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CDatabaseBasics::CDatabaseBasics()
{
    pthread_mutex_init(&pt_mutex_t, NULL);

    try
    {
        db = new odb::mysql::database("root", "root", "mysqlDataBase", "localhost", 3306, 0, "utf8");
    }
    catch (const std::exception &e)
    {
        printf("database exception ... \r\n");
    }
}

/**
  * 函数名称:       CDatabaseBasics
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:       const char *user
  * 函数参数:       const char *passwd
  * 函数参数:       const char *db
  * 函数参数:       const char *host
  * 函数参数:       unsigned int port,
  * 函数参数:       const char *socket
  * 函数参数:       const char *charset
  * 返回值:
*/
CDatabaseBasics::CDatabaseBasics(const char *_user,
                                 const char *_passwd,
                                 const char *_db,
                                 const char *_host,
                                 unsigned int _port,
                                 const char *_socket,
                                 const char *_charset)
{
    pthread_mutex_init(&pt_mutex_t, NULL);

    try
    {
        db = new odb::mysql::database(_user, _passwd, _db, _host, _port, _socket, _charset);
    }
    catch (const std::exception &e)
    {
        printf("database exception ... \r\n");
    }
}

/**
  * 函数名称:       ~CDatabaseBasics
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CDatabaseBasics::~CDatabaseBasics()
{
    pthread_mutex_destroy(&pt_mutex_t);
}