#include <stdio.h>
#include <math.h>
#include <sstream>
#include <csignal>
#include <mcheck.h>
#include <dirent.h>
#include "common/Common.hpp"
#include "fileParser/INIParser.hpp"
#include "databaseOperation/databaseBasics.hpp"
#include "rosWebBasics.hpp"
#include "util_datas.hpp"

using namespace std;

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        log_warn("Terminated by Ctrl+C signal.");
        exit(0);
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    CUtilDatas::pack_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        CUtilDatas::pack_path = ".";
    }
    else
    {
        CUtilDatas::pack_path = buffer;
        free(buffer);
    }

    CINIParser m_ini;
    m_ini.LoadFIle(CUtilDatas::pack_path + "/config/log.ini");
    string mode = m_ini.GetData("log", "mode", "0");
    string style = m_ini.GetData("log", "style", "0");
    string level = m_ini.GetData("log", "level", "4");
    string path = m_ini.GetData("log", "path", "");
    log_init(LogMode(atoi(mode.c_str())), LogStyle(atoi(style.c_str())), LogLevel(atoi(level.c_str())), path);

    log_debug("main log info Mode:%s, Style:%s, Level:%s, Path:%s ...", mode.c_str(), style.c_str(), level.c_str(), path.c_str());

    m_ini.LoadFIle(CUtilDatas::pack_path + "/config/config.ini");
    // 数据库初始化
    string user = m_ini.GetData("mysqlDataBase", "user", "root");
    string passwd = m_ini.GetData("mysqlDataBase", "passwd", "root");
    string db = m_ini.GetData("mysqlDataBase", "db", "ydrobot");
    string host = m_ini.GetData("mysqlDataBase", "host", "127.0.0.1");
    string port = m_ini.GetData("mysqlDataBase", "port", "3306");
    string socket = m_ini.GetData("mysqlDataBase", "socket", "0");
    string charset = m_ini.GetData("mysqlDataBase", "charset", "utf8");

    CDatabaseBasics mysql_db(user.c_str(), passwd.c_str(), db.c_str(), host.c_str(), atoi(port.c_str()), socket.c_str(), charset.c_str());

    log_debug("main mysql database User:%s, Passwd:%s, DB:%s, Host:%s, Port:%s, Socket:%s, Charset:%s ...",
              user.c_str(), passwd.c_str(), db.c_str(), host.c_str(), port.c_str(), socket.c_str(), charset.c_str());

    // ros websocket连接初始化
    string websocket_url = m_ini.GetData("websocket", "url", "ws://127.0.0.1:9090");

    CRosWebBasics m_CRosWebBasics(websocket_url, nullptr);
    m_CRosWebBasics.ros_init();

    log_debug("main ros websocket url:%s ...", websocket_url.c_str());

    //
    bool done = false;
    string input;
    while (!done)
    {
        std::cout << "Enter Command:\r\n";
        std::getline(std::cin, input);

        if (input == "help")
        {
            std::cout << "\nCommand List:\n"
                      << "help: Display this help text\n"
                      << "quit: Exit the program\n"
                      //   << "show: Display WebSocket information\n"
                      << "con: Display Connect information\n"
                      << "msg: Display Message information\n"
                      << "server: Display Server information\n"
                      << "client: Display Client information\n"
                      << "pub: Display Publisher information\n"
                      << "sub: Display Subscriber information\n"
                      << std::endl;
        }
        else if (input == "quit")
        {
            done = true;
        }
        // else if (input.substr(0, 4) == "show")
        // {
        // }
        else if (input.substr(0, 3) == "con")
        {
            m_CRosWebBasics.ros_node.printf_metadata();
        }
        else if (input.substr(0, 3) == "msg")
        {
            m_CRosWebBasics.ros_node.printf_message();
        }
        else if (input.substr(0, 6) == "server")
        {
            m_CRosWebBasics.ros_node.printf_server();
        }
        else if (input.substr(0, 6) == "client")
        {
            m_CRosWebBasics.ros_node.printf_client();
        }
        else if (input.substr(0, 3) == "pub")
        {
            m_CRosWebBasics.ros_node.printf_publisher();
        }
        else if (input.substr(0, 3) == "sub")
        {
            m_CRosWebBasics.ros_node.printf_subscriber();
        }
        else
        {
            std::cout << "> Unrecognized Command" << std::endl;
        }
    }

    return 0;
}