#include "rosWebBasics.hpp"

/**
  * 函数名称:       CRosWebBasics
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosWebBasics::CRosWebBasics()
    : url("127.0.0.1:9090")
{
}

CRosWebBasics::CRosWebBasics(string _url, void *p)
    : url(_url)
{
}

CRosWebBasics::~CRosWebBasics()
{
}

void *CRosWebBasics::test_fun(void *arg)
{
  return nullptr;
}

void CRosWebBasics::ros_init(bool bFirst)
{
  std::vector<std::string> ips;
  std::string ips_str;
  dataParma.loadRobotIps(ips, ips_str);
  if (ips_str == "")
  {
    ips_str = "*";
  }
  if (ips_str[ips_str.size() - 1] == ',')
  {
    ips_str.pop_back();
  }
  //
  if (bFirst)
  {
    ros_node.init_connect(url, "data_server");
  }
  else
  {
    ros_node.reconnection();
  }

  // ros server
  device_param_server = ros_node.advertiseService(ips_str, "/data_server/device_param", "person_monitor_msgs/DeviceParam",
                                                  boost::bind(&CRosWebBasics::device_param_Callback, this, _1, _2, _3));
  // device_param_client = ros_node.serviceClient(ips_str, "/data_server/device_param", "person_monitor_msgs/DeviceParam");
  // publisher = ros_node.advertise(ips_str, "/rostopic_name", "std_msgs/String");
  // subscriber = ros_node.subscribe(ips_str, "/rostopic_name", "std_msgs/String", boost::bind(&CRosWebBasics::subscriber_Callback, this, _1, _2), 1);

  if (bFirst)
  {
    timing_monitoring();
  }
}

void CRosWebBasics::timing_monitoring()
{
  thread connect_status(boost::bind(&CRosWebBasics::connect_check_fun, this, 3000.0));
  connect_status.detach();

  // pthread_t testThread;
  // pthread_create(&testThread, NULL, CRosWebBasics::test_fun, NULL);
}

void CRosWebBasics::connect_check_fun(double msec)
{
  usleep(3000000);
  // std::time_t start_time = 0;
  std::time_t start_time = GetTimeStamp();
  while (1)
  {
    std::time_t now_time = GetTimeStamp();
    if ((now_time - start_time) >= msec)
    {
      start_time = GetTimeStamp();

      try
      {
        std::string status = ros_node.get_status();
        if (status != "Open")
        {
          ros_init(false);
        }
        else
        {
          ros_node.retry_register();
        }
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }
    }
    usleep(100000);
  }
}

bool CRosWebBasics::device_param_Callback(string &ip, string &req, string &res)
{
  printf("device_param_Callback data:%s \r\n", req.c_str());

  RosDeviceParamReq mReq;
  x2struct::X::loadjson(req, mReq, false);
  // 查询参数
  string res_msg = "";
  if (mReq.param == "device")
  {
    device_datas_t devices = dataParma.get_device_parma();
    res_msg = x2struct::X::tojson(devices);
  }
  if (mReq.param == "area")
  {
    area_datas_t areas = dataParma.get_fence_area();
    res_msg = x2struct::X::tojson(areas);
  }
  printf("device_param_Callback res_msg:%s \r\n", res_msg.c_str());

  RosDeviceParamRes mRes;
  mRes.result = 0;
  mRes.data = res_msg;
  res = x2struct::X::tojson(mRes);

  return true;
}

// bool CRosWebBasics::call_server(string ip, string data)
// {
//   bool bRet = false;

//   SRosDeviceParamServer device_param;
//   device_param.request.param = data;
//   string msg = x2struct::X::tojson(device_param);
//   // string msg = x2struct::X::tojson(task, "", 4, ' ');
//   if (device_param_client.call(ip, msg))
//   {
//     x2struct::X::loadjson(msg, device_param, false);
//     if (device_param.response.result == 0)
//     {
//       bRet = true;
//     }
//   }
//   else
//   {
//     bRet = false;
//   }

//   return bRet;
// }

// void CRosWebBasics::publisher_function(string ip, string data)
// {
//   SRosString ros_msg;
//   ros_msg.data = data;

//   string msg = x2struct::X::tojson(ros_msg);
//   // string msg = x2struct::X::tojson(plan_status, "", 4, ' ');

//   publisher.publish(ip, msg);
// }

// void CRosWebBasics::subscriber_Callback(string &ip, string &msg)
// {
//   SRosString ros_msg;
//   x2struct::X::loadjson(msg, ros_msg, false);
//   printf("msg data:%s", ros_msg.data.c_str());
// }
