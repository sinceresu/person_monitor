#include "rosTopic.hpp"
#include "rosNode.hpp"

/**
  * 函数名称:       CRosTopic
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosTopic::CRosTopic()
{
  p = this;
}

/**
  * 函数名称:       ~CRosTopic
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosTopic::~CRosTopic()
{
}

CRosTopic *CRosTopic::p = nullptr;

CRosTopic *CRosTopic::get_topic()
{
  return p;
}

std::vector<CRosPublisher> CRosTopic::get_pubs()
{
  return pubs;
}

void CRosTopic::add_publisher(CRosPublisher _pub)
{
  pubs.push_back(_pub);
}

void CRosTopic::remove_publisher(CRosPublisher _pub)
{
  std::vector<CRosPublisher>::const_iterator it;
  for (it = pubs.begin(); it != pubs.end(); ++it)
  {
    if (*it == _pub)
    {
      pubs.erase(it);
    }
  }
}

void CRosTopic::clear_publisher()
{
  pubs.clear();
}

std::vector<CRosSubscriber> CRosTopic::get_subs()
{
  return subs;
}

void CRosTopic::add_subscriber(CRosSubscriber _sub)
{
  subs.push_back(_sub);
}

void CRosTopic::remove_subscriber(CRosSubscriber _sub)
{
  std::vector<CRosSubscriber>::const_iterator it;
  for (it = subs.begin(); it != subs.end(); ++it)
  {
    if (*it == _sub)
    {
      subs.erase(it);
    }
  }
}

void CRosTopic::clear_subscriber()
{
  subs.clear();
}