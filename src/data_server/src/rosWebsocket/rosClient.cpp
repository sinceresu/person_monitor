#include "rosClient.hpp"
#include "rosService.hpp"

/**
  * 函数名称:       CRosClient
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosClient::CRosClient()
{
}

/**
  * 函数名称:       CRosClient
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosClient::CRosClient(const std::string &_ip, const std::string &_name, const std::string &_type,
                       const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint)
    : ip(_ip),
      id(to_string(boost::uuids::random_generator()())),
      name(_name),
      type(_type),
      fragment_size(0),
      compression("none"),
      state("call_service"),
      node_id(_nodeId),
      node_name(_nodeName),
      endpoint(_endpoint)
{
}

/**
  * 函数名称:       ~CRosClient
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosClient::~CRosClient()
{
}

std::map<std::string, SresMsg> CRosClient::res_msg;

bool CRosClient::call(std::string &_ip, std::string &_msg)
{
  map<std::string, SresMsg>::iterator it;
  it = CRosClient::res_msg.find(id + "/" + name);
  if (it == CRosClient::res_msg.end())
  {
    return false;
  }
  SresMsg &res = it->second;
  res.exit_flag = 0;
  res.res_ip = "";
  res.res_msg = "";

  if (state == "uncall_service" || endpoint == nullptr || node_id == 0)
  {
    return false;
  }

  Json::Reader reader;
  Json::Value Value;
  bool b = reader.parse(_msg, Value);
  if (b == false)
  {
    return false;
  }

  std::string callData;
  Json::Value root;
  root["ip"] = _ip;
  root["op"] = "call_service";
  root["id"] = id;
  root["service"] = name;
  root["args"] = Value["request"];
  // root["fragment_size"] = fragment_size;
  // root["compression"] = compression;
  callData = root.toStyledString();

  std::string message = callData;

  int ret = endpoint->send(node_id, message);
  if (ret != 0)
  {
    return false;
  }

  int timeout = 0;
  while (res.exit_flag == 0)
  {
    if (res.exit_flag == 0 && timeout >= 3000)
    {
      res.exit_flag == 1;
      return false;
    }
    timeout++;
    usleep(10000);
  }

  Json::Reader res_reader;
  Json::Value res_Value;
  bool res_b = reader.parse(res.res_msg, res_Value);
  if (res_b == false)
  {
    return false;
  }
  Value["response"] = res_Value;

  _ip = res.res_ip;
  _msg = Value.toStyledString();

  return true;
}

void CRosClient::unserviceClient()
{
  if (state == "" || state == "uncall_service" || endpoint == nullptr || node_id == 0)
  {
    return;
  }

  map<std::string, SresMsg>::iterator it;
  it = CRosClient::res_msg.find(id + "/" + name);
  if (it == CRosClient::res_msg.end())
  {
    //
  }
  else
  {
    CRosClient::res_msg.erase(id + "/" + name);
  }

  CRosService::get_service()->remove_client(*this);

  ip = "";
  id = "";
  name = "";
  type = "";
  fragment_size = 0;
  compression = "";
  state = "";
  node_id = 0,
  node_name = "",
  endpoint = nullptr;
}

bool CRosClient::operator==(const CRosClient m) const
{
  if (ip == m.ip && id == m.id && name == m.name && type == m.type && state == m.state &&
      fragment_size == m.fragment_size && compression == m.compression &&
      node_id == m.node_id && node_name == m.node_name && endpoint == m.endpoint)
  {
    return true;
  }

  return false;
}

bool CRosClient::operator!=(const CRosClient m) const
{
  if (ip != m.ip || id != m.id || name != m.name || type != m.type || state != m.state ||
      fragment_size != m.fragment_size || compression != m.compression ||
      node_id != m.node_id || node_name != m.node_name || endpoint != m.endpoint)
  {
    return true;
  }

  return false;
}