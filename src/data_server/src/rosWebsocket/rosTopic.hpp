#ifndef rosTopic_HPP
#define rosTopic_HPP

#pragma once
#include "rosPublisher.hpp"
#include "rosSubscriber.hpp"

using namespace std;

class CRosTopic
{
public:
    CRosTopic();
    ~CRosTopic();

public:
    static CRosTopic *get_topic();
    //
    std::vector<CRosPublisher> get_pubs();
    void add_publisher(CRosPublisher _pub);
    void remove_publisher(CRosPublisher _pub);
    void clear_publisher();
    std::vector<CRosSubscriber> get_subs();
    void add_subscriber(CRosSubscriber _sub);
    void remove_subscriber(CRosSubscriber _sub);
    void clear_subscriber();

private:
    static CRosTopic *p;
    //
    std::vector<CRosPublisher> pubs;
    std::vector<CRosSubscriber> subs;
};

#endif