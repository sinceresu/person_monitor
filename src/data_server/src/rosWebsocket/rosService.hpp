#ifndef rosService_HPP
#define rosService_HPP

#pragma once
#include "rosServer.hpp"
#include "rosClient.hpp"

using namespace std;

class CRosService
{
public:
    CRosService();
    ~CRosService();

public:
    static CRosService *get_service();
    //
    std::vector<CRosServer> get_servers();
    void add_server(CRosServer _server);
    void remove_server(CRosServer _server);
    void clear_server();
    std::vector<CRosClient> get_clients();
    void add_client(CRosClient _client);
    void remove_client(CRosClient _client);
    void clear_client();

private:
    static CRosService *p;
    //
    std::vector<CRosServer> servers;
    std::vector<CRosClient> clients;
};

#endif