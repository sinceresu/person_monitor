/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2011, Willow Garage, Inc.
*  Copyright (c) 2015, Tal Regev.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

// #include "boost/endian/conversion.hpp"
#include <boost/endian/conversion.hpp>
#include <map>
#include <boost/make_shared.hpp>
#include <boost/regex.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "rosImage_encodings.h"
#include "rosCv_bridge.h"

namespace enc = ros_sensor_msgs::ros_image_encodings;

namespace ros_cv_bridge
{
  static int depthStrToInt(const std::string depth)
  {
    if (depth == "8U")
    {
      return 0;
    }
    else if (depth == "8S")
    {
      return 1;
    }
    else if (depth == "16U")
    {
      return 2;
    }
    else if (depth == "16S")
    {
      return 3;
    }
    else if (depth == "32S")
    {
      return 4;
    }
    else if (depth == "32F")
    {
      return 5;
    }
    return 6;
  }

  int getCvType(const std::string &encoding)
  {
    // Check for the most common encodings first
    if (encoding == enc::BGR8)
      return CV_8UC3;
    if (encoding == enc::MONO8)
      return CV_8UC1;
    if (encoding == enc::RGB8)
      return CV_8UC3;
    if (encoding == enc::MONO16)
      return CV_16UC1;
    if (encoding == enc::BGR16)
      return CV_16UC3;
    if (encoding == enc::RGB16)
      return CV_16UC3;
    if (encoding == enc::BGRA8)
      return CV_8UC4;
    if (encoding == enc::RGBA8)
      return CV_8UC4;
    if (encoding == enc::BGRA16)
      return CV_16UC4;
    if (encoding == enc::RGBA16)
      return CV_16UC4;

    // For bayer, return one-channel
    if (encoding == enc::BAYER_RGGB8)
      return CV_8UC1;
    if (encoding == enc::BAYER_BGGR8)
      return CV_8UC1;
    if (encoding == enc::BAYER_GBRG8)
      return CV_8UC1;
    if (encoding == enc::BAYER_GRBG8)
      return CV_8UC1;
    if (encoding == enc::BAYER_RGGB16)
      return CV_16UC1;
    if (encoding == enc::BAYER_BGGR16)
      return CV_16UC1;
    if (encoding == enc::BAYER_GBRG16)
      return CV_16UC1;
    if (encoding == enc::BAYER_GRBG16)
      return CV_16UC1;

    // Miscellaneous
    if (encoding == enc::YUV422)
      return CV_8UC2;

    // Check all the generic content encodings
    boost::cmatch m;

    if (boost::regex_match(encoding.c_str(), m,
                           boost::regex("(8U|8S|16U|16S|32S|32F|64F)C([0-9]+)")))
    {
      return CV_MAKETYPE(depthStrToInt(m[1].str()), atoi(m[2].str().c_str()));
    }

    if (boost::regex_match(encoding.c_str(), m,
                           boost::regex("(8U|8S|16U|16S|32S|32F|64F)")))
    {
      return CV_MAKETYPE(depthStrToInt(m[1].str()), 1);
    }

    throw("Unrecognized image encoding [" + encoding + "]" + "\r\n");
  }

  /////////////////////////////////////// Image ///////////////////////////////////////////

  // Converts a ROS Image to a cv::Mat by sharing the data or changing its endianness if needed
  cv::Mat matFromImage(const SRosImage &source)
  {
    int source_type = getCvType(source.encoding);
    int byte_depth = enc::bitDepth(source.encoding) / 8;
    int num_channels = enc::numChannels(source.encoding);

    if (source.step < source.width * byte_depth * num_channels)
    {
      std::stringstream ss;
      ss << "Image is wrongly formed: step < width * byte_depth * num_channels  or  " << source.step << " != " << source.width << " * " << byte_depth << " * " << num_channels;
      throw(ss.str());
    }

    if (source.height * source.step != source.data.size())
    {
      std::stringstream ss;
      ss << "Image is wrongly formed: height * step != size  or  " << source.height << " * " << source.step << " != " << source.data.size();
      throw(ss.str());
    }

    // If the endianness is the same as locally, share the data
    // cv::Mat mat(source.height, source.width, source_type, const_cast<uchar *>(&source.data[0]), source.step);
    string Output;
    ros_type::Base64Decode(source.data, &Output);
    vector<unsigned char> ucImage(Output.begin(), Output.end());
    cv::Mat mat(source.height, source.width, source_type, const_cast<uchar *>(&ucImage[0]), source.step);
    if ((boost::endian::order::native == boost::endian::order::big && source.is_bigendian) ||
        (boost::endian::order::native == boost::endian::order::little && !source.is_bigendian) ||
        byte_depth == 1)
      return mat;

    // Otherwise, reinterpret the data as bytes and switch the channels accordingly
    // mat = cv::Mat(source.height, source.width, CV_MAKETYPE(CV_8U, num_channels * byte_depth),
    //               const_cast<uchar *>(&source.data[0]), source.step);
    mat = cv::Mat(source.height, source.width, CV_MAKETYPE(CV_8U, num_channels * byte_depth),
                  const_cast<uchar *>(&ucImage[0]), source.step);
    cv::Mat mat_swap(source.height, source.width, mat.type());

    std::vector<int> fromTo;
    fromTo.reserve(num_channels * byte_depth);
    for (int i = 0; i < num_channels; ++i)
      for (int j = 0; j < byte_depth; ++j)
      {
        fromTo.push_back(byte_depth * i + j);
        fromTo.push_back(byte_depth * i + byte_depth - 1 - j);
      }
    cv::mixChannels(std::vector<cv::Mat>(1, mat), std::vector<cv::Mat>(1, mat_swap), fromTo);

    // Interpret mat_swap back as the proper type
    mat_swap.reshape(num_channels);

    return mat_swap;
  }

} // namespace ros_cv_bridge
