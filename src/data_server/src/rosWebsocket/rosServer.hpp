#ifndef rosServer_HPP
#define rosServer_HPP

#pragma once
#include "rosType.hpp"

using namespace std;

typedef boost::function<bool(std::string &_ip, std::string &req, std::string &res)> service_callback_fun;

class CRosServer
{
public:
    CRosServer();
    CRosServer(const std::string &_ip, const std::string &_name, const std::string &_type, const service_callback_fun &_callback,
               const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint);
    ~CRosServer();

private:
    websocket_endpoint *endpoint;
    int node_id;
    std::string node_name;

public:
    std::string ip;
    std::string id;
    std::string name;
    std::string type;
    //
    service_callback_fun callback;
    //
    std::string state;

public:
    void response(std::string _call_ip, bool _bResult, std::string _call_id, std::string _msg);
    void unadvertiseService();

public:
    bool operator==(const CRosServer m) const;
    bool operator!=(const CRosServer m) const;
};

#endif