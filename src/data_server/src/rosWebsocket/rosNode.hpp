#ifndef rosNode_HPP
#define rosNode_HPP

#pragma once
#include <thread>
#include <boost/bind.hpp>
#include "rosService.hpp"
#include "rosTopic.hpp"

using namespace std;

class CRosNode
{
public:
    CRosNode();
    CRosNode(const std::string &_url, const std::string &_name = "");
    ~CRosNode();

private:
    static CRosNode *p;
    std::string url;
    int id;
    std::string name;
    websocket_endpoint *endpoint;
    //
    CRosService services;
    CRosTopic topics;
    //
    std::list<std::string> register_fail_ips;

private:
    void on_message(std::string _msg);
    void connect_check_fun(double msec);

public:
    void init_connect(const std::string &_url, const std::string &_name = "");
    void reconnection();
    std::string get_status();
    static CRosNode *get_node();
    void printf_info();
    void printf_metadata();
    void printf_message();
    void printf_server();
    void printf_client();
    void printf_publisher();
    void printf_subscriber();

public:
    CRosServer advertiseService(std::string _ip, std::string _name, std::string _type, service_callback_fun _callback);
    void unadvertiseService(CRosServer &server);
    CRosClient serviceClient(std::string _ip, std::string _name, std::string _type);
    void unserviceClient(CRosClient &_client);
    CRosPublisher advertise(std::string _ip, std::string _name, std::string _type);
    void unadvertise(CRosPublisher &_pub);
    CRosSubscriber subscribe(std::string _ip, std::string _name, std::string _type, topic_callback_fun _callback, unsigned int _queue_length = 0);
    void unsubscribe(CRosSubscriber &_sub);

public:
    void retry_register();
};

#endif
