#include "rosServer.hpp"
#include "rosService.hpp"

/**
  * 函数名称:       CRosServer
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosServer::CRosServer()
{
}

/**
  * 函数名称:       CRosServer
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CRosServer::CRosServer(const std::string &_ip, const std::string &_name, const std::string &_type, const service_callback_fun &_callback,
                       const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint)
    : ip(_ip),
      id(to_string(boost::uuids::random_generator()())),
      name(_name),
      type(_type),
      callback(_callback),
      state("advertise_service"),
      node_id(_nodeId),
      node_name(_nodeName),
      endpoint(_endpoint)
{
}

/**
  * 函数名称:       ~CRosServer
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CRosServer::~CRosServer()
{
}

void CRosServer::response(std::string _call_ip, bool _bResult, std::string _call_id, std::string _msg)
{
  if (state == "unadvertise_service" || endpoint == nullptr || node_id == 0)
  {
    return;
  }

  Json::Reader reader;
  Json::Value Value;
  bool b = reader.parse(_msg, Value);
  if (b == false)
  {
    return;
  }

  std::string responseData;
  Json::Value root;
  root["ip"] = _call_ip;
  root["op"] = "service_response";
  root["id"] = _call_id;
  root["service"] = name;
  root["values"] = Value;
  root["result"] = _bResult;
  responseData = root.toStyledString();

  std::string message = responseData;

  int ret = endpoint->send(node_id, message);
  if (ret != 0)
  {
  }
}

void CRosServer::unadvertiseService()
{
  if (state == "" || state == "unadvertise_service" || endpoint == nullptr || node_id == 0)
  {
    return;
  }

  std::string unadvertiseServiceData;
  Json::Value root;
  root["ip"] = ip;
  root["op"] = "unadvertise_service";
  root["service"] = name;
  unadvertiseServiceData = root.toStyledString();

  std::string message = unadvertiseServiceData;

  int ret = endpoint->send(node_id, message);
  if (ret != 0)
  {
    return;
  }

  CRosService::get_service()->remove_server(*this);

  ip = "";
  id = "";
  name = "";
  type = "";
  callback = 0;
  state = "";
  node_id = 0,
  node_name = "",
  endpoint = nullptr;
}

bool CRosServer::operator==(const CRosServer m) const
{
  if (ip == m.ip && id == m.id && name == m.name && type == m.type && state == m.state &&
      node_id == m.node_id && node_name == m.node_name && endpoint == m.endpoint)
  {
    return true;
  }

  return false;
}

bool CRosServer::operator!=(const CRosServer m) const
{
  if (ip != m.ip || id != m.id || name != m.name || type != m.type || state != m.state ||
      node_id != m.node_id || node_name != m.node_name || endpoint != m.endpoint)
  {
    return true;
  }

  return false;
}