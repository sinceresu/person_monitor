#ifndef rosSubscriber_HPP
#define rosSubscriber_HPP

#pragma once
#include "rosType.hpp"

using namespace std;

typedef boost::function<void(std::string &ip, std::string &msg)> topic_callback_fun;

class CRosSubscriber
{
public:
    CRosSubscriber();
    CRosSubscriber(const std::string &_ip, const std::string &_name, const std::string &_type, const topic_callback_fun &_callback, const unsigned int &_queue_length,
                   const int &_nodeId, const std::string &_nodeName, websocket_endpoint *&_endpoint);
    ~CRosSubscriber();

private:
    int node_id;
    std::string node_name;
    websocket_endpoint *endpoint;

public:
    void unsubscriber();

public:
    std::string ip;
    std::string id;
    std::string name;
    std::string type;
    unsigned int throttle_rate;
    unsigned int queue_length;
    unsigned int fragment_size;
    std::string compression;
    //
    topic_callback_fun callback;
    //
    std::string state;

public:
    bool operator==(const CRosSubscriber m) const;
    bool operator!=(const CRosSubscriber m) const;
};

#endif