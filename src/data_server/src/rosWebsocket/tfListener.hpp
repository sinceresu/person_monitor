#ifndef tfListener_HPP
#define tfListener_HPP

#pragma once
#include <list>
#include <map>
#include <mutex>
#include "rosType.hpp"

using namespace std;

class CTfListener
{
public:
    CTfListener();
    ~CTfListener();

private:
    int node_id;
    std::string node_name;
    websocket_endpoint *endpoint;

    std::list<SRosTFMessage> tfMsgs;
    std::map<std::string, std::list<SRosTFMessage>> tfMsgs_map;

    std::mutex mtx;

public:
    void tf_Callback(std::string &ip, std::string &msg);
    SRosTransformStamped lookupTransform(std::string ip, std::string target_frame, std::string source_frame,
                                         std::time_t time, std::time_t timeout);
};

#endif