#ifndef rosWebsocket_HPP
#define rosWebsocket_HPP

#pragma once
#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

#include <websocketpp/common/thread.hpp>
#include <websocketpp/common/memory.hpp>

#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <list>
#include <mutex>
#include <pthread.h>

#include "jsoncpp/json/json.h"
#include "jsoncpp/json/value.h"
#include "jsoncpp/json/writer.h"

#include "../logger/Logger.hpp"
#include "../common/Common.hpp"

using namespace std;

typedef struct Msg_t
{
    std::string time;
    std::string type;
    std::string op;
    std::string topic;
    std::string service;
    std::string msg;
} SMsg, *PMsg;

typedef boost::function<void(std::string msg)> msg_callback_fun;

typedef websocketpp::client<websocketpp::config::asio_client> client;

class connection_metadata
{
public:
    typedef websocketpp::lib::shared_ptr<connection_metadata> ptr;

    connection_metadata(int id, websocketpp::connection_hdl hdl, std::string uri);

    void on_open(client *c, websocketpp::connection_hdl hdl);

    void on_fail(client *c, websocketpp::connection_hdl hdl);

    void on_close(client *c, websocketpp::connection_hdl hdl);

    void on_message(websocketpp::connection_hdl, client::message_ptr msg);

    websocketpp::connection_hdl get_hdl() const;

    int get_id() const;

    std::string get_status() const;

    void record_sent_message(std::string message);

    void record_message(std::string message, std::string type);

    void record_sent_callback(msg_callback_fun _callback);

    friend std::ostream &operator<<(std::ostream &out, connection_metadata const &data);

    void printf_metadata();

    void printf_message();

private:
    int m_id;
    websocketpp::connection_hdl m_hdl;
    std::string m_status;
    std::string m_uri;
    std::string m_server;
    std::string m_error_reason;
    std::vector<std::string> m_messages;
    std::map<std::string, Msg_t> send_msgs;
    std::map<std::string, Msg_t> receive_msgs;
    std::vector<msg_callback_fun> m_callbacks;
    //
    std::mutex msg_mtx;
    pthread_mutex_t msg_pt_mutex_t;

private:
    std::vector<std::string> topic_arr;
    std::vector<std::string> service_arr;
};

/**
  * websocket_endpoint
*/
class websocket_endpoint
{
public:
    websocket_endpoint();
    ~websocket_endpoint();

    int connect(std::string const &uri);
    int connect(int const &id, std::string const &uri);

    void close(int id, websocketpp::close::status::value code, std::string reason);

    int send(int id, std::string message);

    int register_msg_callback(int id, msg_callback_fun _callback);

    std::string get_status(int id);

    connection_metadata::ptr get_metadata(int id) const;

    void printf_metadata(int id);

    void printf_message(int id);

private:
    typedef std::map<int, connection_metadata::ptr> con_list;

    client m_endpoint;
    websocketpp::lib::shared_ptr<websocketpp::lib::thread> m_thread;

    con_list m_connection_list;
    int m_next_id;
};

#endif

// https://github.com/RobotWebTools/rosbridge_suite/blob/groovy-devel/ROSBRIDGE_PROTOCOL.md
