#ifndef Logger_HPP
#define Logger_HPP

#pragma once
// #include <stdio.h>
#include <stdarg.h>
#include <iostream>
// #include <iomanip>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#ifdef __linux__
// #include <unistd.h>
#elif _WIN32
#include <Windows.h>
#elif __ANDROID__
#else
#endif

#ifdef __linux__
//the following are Ubuntu/Linux only terminal color codes.
#define _NONE "\033[0m"
#define _BLACK "\033[30m"              /* Black */
#define _RED "\033[31m"                /* Red */
#define _GREEN "\033[32m"              /* Green */
#define _YELLOW "\033[33m"             /* Yellow */
#define _BLUE "\033[34m"               /* Blue */
#define _MAGENTA "\033[35m"            /* Magenta */
#define _CYAN "\033[36m"               /* Cyan */
#define _WHITE "\033[37m"              /* White */
#define _BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define _BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define _BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define _BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define _BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define _BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define _BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define _BOLDWHITE "\033[1m\033[37m"   /* Bold White */
#elif _WIN32
//the following are Windows ONLY terminal color codes.
#define _NONE FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE
#define _BLACK 0                                                                              /* Black */
#define _RED FOREGROUND_RED                                                                   /* Red */
#define _GREEN FOREGROUND_GREEN                                                               /* Green */
#define _YELLOW FOREGROUND_RED | FOREGROUND_GREEN                                             /* Yellow */
#define _BLUE FOREGROUND_BLUE                                                                 /* Blue */
#define _MAGENTA FOREGROUND_RED | FOREGROUND_BLUE                                             /* Magenta */
#define _CYAN FOREGROUND_GREEN | FOREGROUND_BLUE                                              /* Cyan */
#define _WHITE FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE                            /* White */
#define _BOLDBLACK FOREGROUND_INTENSITY | 0                                                   /* Bold Black */
#define _BOLDRED FOREGROUND_INTENSITY | FOREGROUND_RED                                        /* Bold Red */
#define _BOLDGREEN FOREGROUND_INTENSITY | FOREGROUND_GREEN                                    /* Bold Green */
#define _BOLDYELLOW FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN                  /* Bold Yellow */
#define _BOLDBLUE FOREGROUND_INTENSITY | FOREGROUND_BLUE                                      /* Bold Blue */
#define _BOLDMAGENTA FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE                  /* Bold Magenta */
#define _BOLDCYAN FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE                   /* Bold Cyan */
#define _BOLDWHITE FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE /* Bold White */
#elif __ANDROID__
#else
#endif

#define log_init(_mode, _style, _level, _path) CLogger::Log_Init(_mode, _style, _level, _path)
#define log_output(type, format, ...) CLogger::Log_Output(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, type, format, ##__VA_ARGS__)
#define log_trace(format, ...) CLogger::Log_Trace(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__)
#define log_debug(format, ...) CLogger::Log_Debug(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__)
#define log_info(format, ...) CLogger::Log_Info(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__)
#define log_warn(format, ...) CLogger::Log_Warn(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__)
#define log_error(format, ...) CLogger::Log_Error(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__)
#define log_fatal(format, ...) CLogger::Log_Fatal(__DATE__, __TIME__, __FILE__, __LINE__, __FUNCTION__, format, ##__VA_ARGS__)

enum struct LogMode
{
    CONSOLE = 0,
    FILE,
    ALL
};

enum struct LogStyle
{
    CONCISE = 0,
    DETAIL
};

enum struct LogLevel
{
    OFF = 0,
    FATAL,
    ERROR,
    WARN,
    INFO,
    DEBUG,
    TRACE,
    ALL
};

enum struct LogType
{
    FATAL = 0,
    ERROR,
    WARN,
    INFO,
    DEBUG,
    TRACE
};

class CLogger
{
public:
    CLogger();
    ~CLogger();

private:
    static LogMode Mode;
    static LogStyle Style;
    static LogLevel Level;
    static std::string Path;

private:
    static int log_vscprintf(const char *format, va_list pargs);
    //
    static std::string log_time(const std::string &_format = "%Y-%m-%d %H:%M:%S");
    //
    static std::string log_header(LogType type, const char *date, const char *time, const char *file, const int line, const char *function);
    static std::string log_body(LogType type, const char *format, va_list pargs);
    //
    static void log_printf(LogType type, const char *header, const char *body);
    static void console_printf(LogType type, const char *header, const char *body);
    static void file_printf(LogType type, const char *header, const char *body);

public:
    static void Log_Init(LogMode _mode = LogMode::CONSOLE, LogStyle _style = LogStyle::CONCISE, LogLevel _level = LogLevel::OFF, std::string _path = "./log");
    //
    static void Log_Output(const char *date, const char *time, const char *file, const int line, const char *function, LogType type, const char *format, ...);
    static void Log_Trace(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...);
    static void Log_Debug(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...);
    static void Log_Info(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...);
    static void Log_Warn(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...);
    static void Log_Error(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...);
    static void Log_Fatal(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...);
};

#endif