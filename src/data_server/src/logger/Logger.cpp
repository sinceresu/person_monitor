#include "Logger.hpp"

CLogger logger;

CLogger::CLogger()
{
}

CLogger::~CLogger()
{
}

/**
  * 输出模式
*/
LogMode CLogger::Mode = LogMode::CONSOLE;

/**
  * 输出样式
*/
LogStyle CLogger::Style = LogStyle::CONCISE;

/**
  * 输出等级
*/
LogLevel CLogger::Level = LogLevel::OFF;

/**
  * 输出路径
*/
std::string CLogger::Path = "./log";

/**
 * Description:      计算可变参数长度
 * Function:         log_vscprintf
 * Parameters:       format
 * Parameters:       pargs
 * Return:           int
 * Author:
 * Date:
*/
int CLogger::log_vscprintf(const char *format, va_list pargs)
{
    int retval;
    va_list argcopy;
    va_copy(argcopy, pargs);
    retval = vsnprintf(NULL, 0, format, argcopy);
    va_end(argcopy);

    return retval;
}

/**
 * Description:      log时间
 * Function:         log_time
 * Parameters:       format
 * Return:           std::string
 * Author:
 * Date:
*/
std::string CLogger::log_time(const std::string &_format)
{
    time_t nowtime;
    nowtime = time(NULL);
    char tmp[64];
#ifdef __linux__
    strftime(tmp, sizeof(tmp), _format.c_str(), localtime(&nowtime));
#elif _WIN32
    struct tm t;
    errno_t err = localtime_s(&t, &nowtime);
    if (err != 0)
    {
        return "";
    }
    strftime(tmp, sizeof(tmp), _format.c_str(), &t);
#elif __ANDROID__
#else
#endif

    return tmp;
}

/**
 * Description:      log公共头数据
 * Function:         log_header
 * Parameters:       type
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Return:           std::string
 * Author:
 * Date:
*/
std::string CLogger::log_header(LogType type, const char *date, const char *time, const char *file, const int line, const char *function)
{
    // std::string _data = date;
    std::string _data = __DATE__;
    // std::string _time = time;
    std::string _time = __TIME__;
    std::string _file = file;
    std::string _line = std::to_string(line);
    std::string _function = function;

    std::string header = "";
    if (Style == LogStyle::CONCISE)
    {
        header += "[ ";
        header += _data + " ";
        header += _time + " ";
        header += "] ";
    }
    //
    if (Style == LogStyle::DETAIL)
    {
        header += "[\r\n";
        header += " __DATE__     : \"" + _data + "\"\r\n";
        header += " __TIME__     : \"" + _time + "\"\r\n";
        header += " __FILE__     : \"" + _file + "\"\r\n";
        header += " __LINE__     : \"" + _line + "\"\r\n";
        header += " __FUNCTION__ : \"" + _function + "\"\r\n";
        header += "]\r\n";
    }

    return header;
}

/**
 * Description:      log内容数据
 * Function:         log_body
 * Parameters:       type
 * Parameters:       format
 * Parameters:       pargs
 * Return:           std::string
 * Author:
 * Date:
*/
std::string CLogger::log_body(LogType type, const char *format, va_list pargs)
{
    va_list argcopy;
    va_copy(argcopy, pargs);
    int len = log_vscprintf(format, argcopy) + 1;
    char buffer[len];
    vsnprintf(buffer, len, format, argcopy);
    va_end(argcopy);

    std::string _type = "";
    std::string _buffer = buffer;
    switch (type)
    {
    case LogType::TRACE:
        _type = " __TRACK__    : ";
        break;
    case LogType::DEBUG:
        _type = " __DEBUG__    : ";
        break;
    case LogType::INFO:
        _type = " __INFO__     : ";
        break;
    case LogType::WARN:
        _type = " __WARN__     : ";
        break;
    case LogType::ERROR:
        _type = " __ERROR__    : ";
        break;
    case LogType::FATAL:
        _type = " __FATAL__    : ";
        break;

    default:
        _type = " __DEFAULT__  : ";
        break;
    }

    std::string body = "";
    if (Style == LogStyle::CONCISE)
    {
        body += _type + "\"" + _buffer + "\"\r\n";
    }
    //
    if (Style == LogStyle::DETAIL)
    {
        body += _type + "\"" + _buffer + "\"\r\n";
    }

    return body;
}

/**
 * Description:      log打印
 * Function:         log_printf
 * Parameters:       type
 * Parameters:       header
 * Parameters:       body
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::log_printf(LogType type, const char *header, const char *body)
{
    if (Mode == LogMode::ALL || Mode == LogMode::CONSOLE)
    {
        console_printf(type, header, body);
    }
    //
    if (Mode == LogMode::ALL || Mode == LogMode::FILE)
    {
        file_printf(type, header, body);
    }
}

/**
 * Description:      log控制台打印
 * Function:         console_printf
 * Parameters:       type
 * Parameters:       header
 * Parameters:       body
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::console_printf(LogType type, const char *header, const char *body)
{
#ifdef __linux__
    std::string _Color = _NONE;
#elif _WIN32
    int _Color = _NONE;
#elif __ANDROID__
#else
#endif
    switch (type)
    {
    case LogType::TRACE:
        _Color = _BLUE;
        break;
    case LogType::DEBUG:
        _Color = _CYAN;
        break;
    case LogType::INFO:
        _Color = _GREEN;
        break;
    case LogType::WARN:
        _Color = _YELLOW;
        break;
    case LogType::ERROR:
        _Color = _RED;
        break;
    case LogType::FATAL:
        _Color = _MAGENTA;
        break;

    default:
        _Color = _NONE;
        break;
    }

#ifdef __linux__
    // printf("%s%s%s%s%s%s", _NONE, header, _Color, body, _NONE, "\r\n");
    if (Style == LogStyle::CONCISE)
    {
        std::cout << _NONE << header << _Color << body << _NONE << std::endl;
    }
    //
    if (Style == LogStyle::DETAIL)
    {
        std::cout << _NONE << header << _Color << body << _NONE << std::endl;
    }
#elif _WIN32
    HANDLE handle;
    handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, _NONE);
    std::cout << header;
    SetConsoleTextAttribute(handle, _Color);
    std::cout << body;
    SetConsoleTextAttribute(handle, _NONE);
    std::cout << std::endl;
#elif __ANDROID__
#else
#endif
}

/**
 * Description:      log文件打印
 * Function:         file_printf
 * Parameters:       type
 * Parameters:       header
 * Parameters:       body
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::file_printf(LogType type, const char *header, const char *body)
{
    try
    {
        std::string log_name = log_time("%Y-%m-%d") + ".log";
        std::string log_file = Path + log_name;
#ifdef __linux__
        int fd = open(log_file.c_str(), O_RDWR | O_CREAT | O_APPEND, 0666);
        if (fd != -1)
        {
            // string text = header + body;
            // write(fd, text.c_str(), text.length());
            write(fd, header, strlen(header));
            write(fd, body, strlen(body));
            write(fd, "\r\n", 2);

            close(fd);
        }
#elif _WIN32
        FILE *fp = fopen(log_file.c_str(), "ab+");
        if (fp)
        {
            // string text = header + body;
            // size_t r_size = fwrite(text, strlen(text), 1, fp);
            size_t r_size = fwrite(header, strlen(header), 1, fp);
            r_size = fwrite(body, strlen(body), 1, fp);
            r_size = fwrite("\r\n", 2, 1, fp);

            fclose(fp);
        }
#elif __ANDROID__
#else
#endif
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}

/**
 * Description:      log信息初始化
 * Function:         Log_Init
 * Parameters:       _mode
 * Parameters:       _style
 * Parameters:       _level
 * Parameters:       _path
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Init(LogMode _mode, LogStyle _style, LogLevel _level, std::string _path)
{
    Mode = _mode;
    Style = _style;
    Level = _level;
    Path = _path;
    if (Path.size() > 0)
    {
#ifdef __linux__
        if (Path[Path.size() - 1] != '/')
        {
            Path += "/";
        }
#elif _WIN32
        if (Path[Path.size() - 1] != '\\')
        {
            Path += "\\";
        }
#elif __ANDROID__
#else
#endif
    }
}

/**
 * Description:      log输出
 * Function:         Log_Output
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       type
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Output(const char *date, const char *time, const char *file, const int line, const char *function, LogType type, const char *format, ...)
{
    va_list args;
    va_start(args, format);

    switch (type)
    {
    case LogType::TRACE:
        Log_Trace(date, time, file, line, function, format, args);
        break;
    case LogType::DEBUG:
        Log_Debug(date, time, file, line, function, format, args);
        break;
    case LogType::INFO:
        Log_Info(date, time, file, line, function, format, args);
        break;
    case LogType::WARN:
        Log_Warn(date, time, file, line, function, format, args);
        break;
    case LogType::ERROR:
        Log_Error(date, time, file, line, function, format, args);
        break;
    case LogType::FATAL:
        Log_Fatal(date, time, file, line, function, format, args);
        break;
    default:
        break;
    }

    va_end(args);
}

/**
 * Description:      log跟踪输出
 * Function:         Log_Trace
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Trace(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...)
{
    if (Level >= LogLevel::TRACE)
    {
        va_list args;
        va_start(args, format);

        std::string header = log_header(LogType::TRACE, date, time, file, line, function);
        std::string body = log_body(LogType::TRACE, format, args);

        log_printf(LogType::TRACE, header.c_str(), body.c_str());

        va_end(args);
    }
}

/**
 * Description:      log调试输出
 * Function:         Log_Debug
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Debug(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...)
{
    if (Level >= LogLevel::DEBUG)
    {
        va_list args;
        va_start(args, format);

        std::string header = log_header(LogType::DEBUG, date, time, file, line, function);
        std::string body = log_body(LogType::DEBUG, format, args);

        log_printf(LogType::DEBUG, header.c_str(), body.c_str());

        va_end(args);
    }
}

/**
 * Description:      log信息输出
 * Function:         Log_Info
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Info(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...)
{
    if (Level >= LogLevel::INFO)
    {
        va_list args;
        va_start(args, format);

        std::string header = log_header(LogType::INFO, date, time, file, line, function);
        std::string body = log_body(LogType::INFO, format, args);

        log_printf(LogType::INFO, header.c_str(), body.c_str());

        va_end(args);
    }
}

/**
 * Description:      log警告输出
 * Function:         Log_Warn
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Warn(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...)
{
    if (Level >= LogLevel::WARN)
    {
        va_list args;
        va_start(args, format);

        std::string header = log_header(LogType::WARN, date, time, file, line, function);
        std::string body = log_body(LogType::WARN, format, args);

        log_printf(LogType::WARN, header.c_str(), body.c_str());

        va_end(args);
    }
}

/**
 * Description:      log错误输出
 * Function:         Log_Error
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Error(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...)
{
    if (Level >= LogLevel::ERROR)
    {
        va_list args;
        va_start(args, format);

        std::string header = log_header(LogType::ERROR, date, time, file, line, function);
        std::string body = log_body(LogType::ERROR, format, args);

        log_printf(LogType::ERROR, header.c_str(), body.c_str());

        va_end(args);
    }
}

/**
 * Description:      log致命输出
 * Function:         Log_Fatal
 * Parameters:       date
 * Parameters:       time
 * Parameters:       file
 * Parameters:       line
 * Parameters:       function
 * Parameters:       format
 * Parameters:       ...
 * Return:           void
 * Author:
 * Date:
*/
void CLogger::Log_Fatal(const char *date, const char *time, const char *file, const int line, const char *function, const char *format, ...)
{
    if (Level >= LogLevel::FATAL)
    {
        va_list args;
        va_start(args, format);

        std::string header = log_header(LogType::FATAL, date, time, file, line, function);
        std::string body = log_body(LogType::FATAL, format, args);

        log_printf(LogType::FATAL, header.c_str(), body.c_str());

        va_end(args);
    }
}