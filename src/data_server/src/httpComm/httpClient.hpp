#ifndef httpClient_HPP
#define httpClient_HPP

#pragma once
#include <string>
#include <openssl/md5.h>
#include "mongoose.h"

using namespace std;

typedef void (*Func_Ptr)(int code, const char *response);

class CHttpClient
{
public:
    CHttpClient();
    ~CHttpClient();

public:
    static Func_Ptr resultCallback;
    static int exit_flag;

public:
    static void ev_handler(struct mg_connection *c, int ev, void *p);
    //
    static void get(const char *url, const char *headers, const char *getData, Func_Ptr callback = nullptr);
    static void post(const char *url, const char *headers, const char *postData, Func_Ptr callback = nullptr);
    static void put(const char *url, const char *headers, const char *putData, Func_Ptr callback = nullptr);

public:
    static int md5_encrypt(const char *input, char *output);
    static char *md5_hex2c(unsigned char *in_md5_c, char *out_md5_c);
};

#endif