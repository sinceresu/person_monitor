#include "httpClient.hpp"

/**
  * 函数名称:       CHttpClient
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       构造方法
  * 函数参数:
  * 返回值:
*/
CHttpClient::CHttpClient()
{
}

/**
  * 函数名称:       ~CHttpClient
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       析构方法
  * 函数参数:
  * 返回值:
*/
CHttpClient::~CHttpClient()
{
}

Func_Ptr CHttpClient::resultCallback = nullptr;

int CHttpClient::exit_flag = 0;

/**
  * 函数名称:       ev_handler
  * 访问权限:       public
  * 创建日期:
  * 函数说明:
  * 函数参数:
  * 返回值:
*/
void CHttpClient::ev_handler(struct mg_connection *c, int ev, void *p)
{
  // struct http_message *hm = (struct http_message *)p;

  if (ev == MG_EV_CONNECT)
  {
    if (*(int *)p != 0)
    {
      fprintf(stderr, "connect() failed: %s\n", strerror(*(int *)p));
      if (resultCallback != nullptr)
      {
        resultCallback(-1, "");
      }
      exit_flag = 1;
    }
  }
  else if (ev == MG_EV_HTTP_REPLY)
  {
    struct http_message *hm = (struct http_message *)p;
    c->flags |= MG_F_CLOSE_IMMEDIATELY;

    // fwrite(hm->message.p, 1, hm->message.len, stdout);
    // putchar('\n');
    // fwrite(hm->body.p, 1, hm->body.len, stdout);
    // putchar('\n');
    // fprintf(stderr, "\r\n");

    if (resultCallback != nullptr)
    {
      resultCallback(0, hm->body.p);
    }
    exit_flag = 1;
  }
  else if (ev == MG_EV_CLOSE)
  {
    if (exit_flag == 0)
    {
      printf("Server closed connection\n");
      if (resultCallback != nullptr)
      {
        resultCallback(-1, "");
      }
      exit_flag = 1;
    }
  }
}

/**
  * 函数名称:       get
  * 访问权限:       public
  * 创建日期:
  * 函数说明:
  * 函数参数:
  * 返回值:
*/
void CHttpClient::get(const char *url, const char *headers, const char *getData, Func_Ptr callback)
{
  // printf("http client get request. url:%s, header:%s, data:%s... \r\n", url, headers, getData);

  resultCallback = callback;

  struct mg_mgr mgr;

  mg_mgr_init(&mgr, NULL);
  mg_connect_http(&mgr, ev_handler, url, NULL, NULL, "GET");

  while (exit_flag == 0)
  {
    mg_mgr_poll(&mgr, 1000);
  }
  mg_mgr_free(&mgr);

  exit_flag = 0;
}

/**
  * 函数名称:       post
  * 访问权限:       public
  * 创建日期:
  * 函数说明:
  * 函数参数:
  * 返回值:
*/
void CHttpClient::post(const char *url, const char *headers, const char *postData, Func_Ptr callback)
{
  // printf("http client post request. url:%s, header:%s, data:%s... \r\n", url, headers, postData);

  resultCallback = callback;

  struct mg_mgr mgr;

  mg_mgr_init(&mgr, NULL);
  mg_connect_http(&mgr, ev_handler, url, headers, postData, "POST");

  while (exit_flag == 0)
  {
    mg_mgr_poll(&mgr, 1000);
  }
  mg_mgr_free(&mgr);

  exit_flag = 0;
}

/**
  * 函数名称:       put
  * 访问权限:       public
  * 创建日期:
  * 函数说明:
  * 函数参数:
  * 返回值:
*/
void CHttpClient::put(const char *url, const char *headers, const char *putData, Func_Ptr callback)
{
  // printf("http client put request. url:%s, header:%s, data:%s... \r\n", url, headers, putData);

  resultCallback = callback;

  struct mg_mgr mgr;

  mg_mgr_init(&mgr, NULL);
  mg_connect_http(&mgr, ev_handler, url, headers, putData, "PUT");

  while (exit_flag == 0)
  {
    mg_mgr_poll(&mgr, 1000);
  }
  mg_mgr_free(&mgr);

  exit_flag = 0;
}

/**
  * 函数名称:       md5_encrypt
  * 访问权限:       public
  * 创建日期:
  * 函数说明:
  * 函数参数:
  * 返回值:         int
*/
int CHttpClient::md5_encrypt(const char *input, char *output)
{
  int nRet = 0;
  //
  unsigned char md[MD5_DIGEST_LENGTH];
  memset(md, 0, MD5_DIGEST_LENGTH);
  char buf[MD5_DIGEST_LENGTH * 2 + 1];
  memset(buf, 0, MD5_DIGEST_LENGTH * 2 + 1);
  //
  char tmp[3] = {'\0'};

  MD5_CTX ctx;
  // 1. 初始化
  if (1 != MD5_Init(&ctx))
  {
    nRet = -1;
    printf("MD5_Init failed ... \r\n");
  }
  // 2. 添加数据
  if (1 != MD5_Update(&ctx, (const void *)input, strlen((char *)input)))
  {
    nRet = -1;
    printf("MD5_Update failed ... \r\n");
  }
  // 3. 计算结果
  if (1 != MD5_Final(md, &ctx))
  {
    nRet = -1;
    printf("MD5_Final failed ... \r\n");
  }
  // 4. 输出结果
  for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
  {
    sprintf(&buf[i * 2], "%02x", md[i]);
    // sprintf(&buf[i * 2], "%02X", md[i]);
  }
  strcpy(output, buf);
  //
  // MD5((unsigned char *)input, strlen((char *)input), md);
  // for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
  // {
  //   sprintf(&buf[i * 2], "%02x", md[i]);
  //   // sprintf(&buf[i * 2], "%02X", md[i]);
  // }
  // strcpy(output, buf);

  return nRet;
}

/**
  * 函数名称:       md5_hex2c
  * 访问权限:       public
  * 创建日期:
  * 函数说明:       将hex编码的MD5转换成字符串
  * 函数参数:
  * 返回值:         char *
*/
char *CHttpClient::md5_hex2c(unsigned char *in_md5_c, char *out_md5_c)
{
  for (int i = 0; i < MD5_DIGEST_LENGTH; ++i)
  {
    // 小写
    sprintf(out_md5_c + i * 2, "%02x", in_md5_c[i]);
    // 大写
    // sprintf(out_md5_c + i * 2, "%02X", in_md5_c[i]);
  }
  out_md5_c[MD5_DIGEST_LENGTH * 2] = '\0';

  return out_md5_c;
}