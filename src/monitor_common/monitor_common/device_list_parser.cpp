#include "device_list_parser.h"


#include <fstream>

#include <jsoncpp/json/json.h>


namespace person_monitor {
namespace  monitor_common{


DeviceListParser::DeviceListParser(std::string json_str) :
json_str_(json_str)
{
}

bool DeviceListParser:: ReadDeviceList(DeviceList & device_list){
  device_list.devices.clear();
  Json::Reader reader;
  Json::Value root;

  reader.parse(&json_str_[0],&json_str_[json_str_.size() - 1],  root);
  if (root.empty()) 
    return false;

  // Json::Value devices_value = root["devices"];

  // for (const auto& device_value : devices_value ) {
  //   Device device;
  //   device.device_id = device_value["device_id"].asString();
  //   device.master_ip = device_value["master_ip"].asString();
  //   device.type = device_value["type"].asInt();
  //   device.rotate = device_value["rotate"].asInt();
  //   device.calibration = device_value["calibration"].asString();

  //   Json::Value camera_value = device_value["cameras"];
  //   if (camera_value.empty()) {
  //     // return false;
  //     continue;
  //   }
  //   for (const auto& camera_value : camera_value ) {
  //     Camera camera;
  //     camera.type = camera_value["type"].asString();
  //     camera.stream_url = camera_value["stream_url"].asString();
  //     //note : should obtain from configuration json, temporarily set to device id.
  //     camera.id = device.device_id;
  //     device.cameras.push_back(camera);
  //   }

  //   device_list.devices.push_back(std::move(device));
  // }

  Json::Value devices_value = root["list"];

  for (const auto& device_value : devices_value ) {
    Device device;
    device.id = device_value["id"].asInt();
    device.type = device_value["type"].asInt();
    device.rotate = device_value["rotate"].asInt();
    device.status = device_value["status"].asInt();
    device.ipCore = device_value["ipCore"].asString();
    device.calibrationData = device_value["calibrationData"].asString();

    Json::Value camera_value = device_value["cameras"];
    if (camera_value.empty()) {
      continue;
    }
    for (const auto& camera_value : camera_value ) {
      Camera camera;
      camera.id = camera_value["id"].asInt();
      camera.type = camera_value["type"].asInt();
      camera.status = camera_value["status"].asInt();
      camera.rtspUrl = camera_value["rtspUrl"].asString();
      //note : should obtain from configuration json, temporarily set to device id.
      camera.id = device.id;
      device.cameras.push_back(camera);
    }

    device_list.devices.push_back(std::move(device));
  }

  return true;

}
}
}
