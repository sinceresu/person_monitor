#pragma once
#include <string>
#include <vector>

namespace person_monitor {
namespace  monitor_common{
      class DeviceListParser {
        public:
        // struct Camera {
        //   std::string id;
        //   std::string type;
        //   std::string stream_url;
        // };
        // struct Device {
        //   std::string device_id;
        //   std::string master_ip;
        //   int type;
        //   int rotate;
        //   std::string calibration;
        //    std::vector<Camera> cameras;
        // };
        // struct DeviceList {
        //     std::vector<Device> devices;
        // };
        
        struct Camera {
          int id;
          int status;
          int type;
          std::string rtspUrl;
        };
        struct Device {
          int id;
          int type;
          int rotate;
          int status;
          std::string ipCore;
          std::string calibrationData;
           std::vector<Camera> cameras;
        };
        struct DeviceList {
            std::vector<Device> devices;
        };

        public:
          // Constructor & Destructor
          DeviceListParser(std::string json_str);
          ~DeviceListParser(){};
          bool ReadDeviceList(DeviceList & device_list) ;
      private:
        std::string json_str_;

      };

}
}
